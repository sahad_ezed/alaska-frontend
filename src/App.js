import React, {Fragment} from 'react';

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import './Components/Assets/Stylesheet.css'
// import './Components/Assets/fonts/icofont.min.css'

import { Switch, Route } from 'react-router-dom'

//user screens
import Home from './Components/Scenes/Home'
import Test from './Components/Scenes/Test'
import CourseList from './Components/Scenes/CourseList'
import CourseDetails from './Components/Scenes/CourseDetails'
import Cart from './Components/Scenes/Cart'
import Login from './Components/Scenes/Login'
import MyAccounts from './Components/Scenes/MyAccount'
import ChapterList from './Components/Scenes/ChapterList'
import ChapterDetail from './Components/Scenes/ChapterDetail'
import SubchapterDetail from './Components/Scenes/SubchapterDetail'
import ExamScreen from './Components/Scenes/ExamScreen'
import EmailVerify from './Components/Scenes/EmailVerify'
import PartnerInstitutions from './Components/Scenes/PartnerInstitutions'
import AboutUs from './Components/Scenes/AboutUs'
import Faq from './Components/Scenes/Faq'
import PageNotFound from './Components/Scenes/PageNotFound'

//admin screens
import AdminLogin from './Components/Scenes/Admin/Login'
import AdminHome from './Components/Scenes/Admin/Home'
import Category from './Components/Scenes/Admin/Category'
import Users from './Components/Scenes/Admin/Users'

  //course
  import AdminCourses from './Components/Scenes/Admin/Courses/Courses'
  import AddCourse from './Components/Scenes/Admin/Courses/AddCourse'
  //import EditCourse from './Components/Scenes/Admin/Courses/EditCourse'
  import Chapterlist from './Components/Scenes/Admin/chapter/Chapterlist'
  import Add_chapter from './Components/Scenes/Admin/chapter/Add_chapter'
  import AdminChapterDetail from './Components/Scenes/Admin/chapter/AdminChapterDetail'
  import Edit_Chapter from './Components/Scenes/Admin/chapter/Edit_Chapter'
  import AddSubChapter from './Components/Scenes/Admin/chapter/AddSubChapter'
  import AdminSubChapterDetail from './Components/Scenes/Admin/chapter/AdminSubChapterDetail'
  import EditSubChapter from './Components/Scenes/Admin/chapter/EditSubChapter'
  import AddExam from './Components/Scenes/Admin/chapter/AddExam'
  import AddPractical from './Components/Scenes/Admin/Practical/AddPractical'
  import TestEdit from './Components/Scenes/Admin/Courses/MainEditCourse'



    

import TopNav from './Components/Elements/Top-Nav'
import NavbarContainer from './Components/Elements/Navbar'
import Footer from './Components/Elements/Footer'

function App() {
  return (
    <Fragment>
        <TopNav />
        <NavbarContainer />

          <Switch>
            {/* user */}
            
            <Route exact path="/" component={Home} />
            <Route path="/test" component={Test} />
            <Route path="/courses" component={CourseList} />
            <Route path="/course" component={CourseDetails} />
            <Route path="/cart" component={Cart} />
            <Route path="/login" component={Login} />
            <Route path="/my-course" component={MyAccounts} />
            <Route path='/chapter' component={ChapterList} />
            <Route path='/chapter-detailed' component={ChapterDetail} />
            <Route path='/subchapter-detailed' component={SubchapterDetail} /> ExamScreen
            <Route path='/exam' component={ExamScreen} />
            <Route path='/verify-email/:id' component={EmailVerify} /> 
            <Route path='/partner-institutions' component={PartnerInstitutions} /> 
            <Route path='/about-us' component={AboutUs} />
            <Route path='/faq' component={Faq} />
           
            

            {/* admin */}
            <Route path="/admin" component={AdminHome} />
            <Route path="/adminLogin" component={AdminLogin} />
            <Route path="/admin-category" component={Category} />
            <Route path="/admin-courses" component={AdminCourses} />
            <Route path="/admin-add-courses" component={AddCourse} /> 
            <Route path="/admin-edit-courses" component={TestEdit} />
            <Route path="/admin-chapter" component={Chapterlist} />
            <Route path="/admin-edit-chapter" component={Edit_Chapter} />
            <Route path="/admin-add-chapter" component={Add_chapter} /> 
            <Route path="/admin-detailed-chapter" component={AdminChapterDetail} />
            <Route path="/admin-add-subchapter" component={AddSubChapter} />  
            <Route path="/admin-detailed-subchapter" component={AdminSubChapterDetail} /> 
            <Route path="/admin-edit-subchapter" component={EditSubChapter} />
            <Route path="/admin-exam" component={AddExam} />
            <Route path="/admin-users" component={Users} />
            <Route path="/admin-practical" component={AddPractical} />

            <Route component={PageNotFound} />

          </Switch>

          
        <Footer />
    </Fragment>
    
  );
}

export default App;
