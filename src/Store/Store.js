import React, { createContext, useReducer, useEffect } from 'react'
import axios from 'axios'
import swal from 'sweetalert';
import Cookies from "js-cookie"
import { BASE_URL } from './Config'
import { ShopReducer, CATEGORY_LIST, COURSE_LIST, 
        TEMP_PRODUCT, CART_LIST, LOGIN_ERR, LOGIN, 
        LOGOUT, CART_ALERT, LOGGIN_STATUS, LOGIN_TO_PAGE,
        CART_TOTAL, ADMIN_LOGIN, ORDER_LIST, SET_CHAPTER, MY_COURSE, 
        TEMP_CHAPTER, TEMP_SUBCHAPTER, SET_CURRENT_CHAPTER_ID, SET_CURRENT_SUBCHAPTER_ID,
        SET_CURRENT_CHAPTER_INDEX, READ_CHAPTER_STATUS, READ_SUBCHAPTER_STATUS, TEMP_SUBCHAPTER_2,
        SET_COURSE_PROGRESS, REGISTER_ERR, PROFILE, MODIFY_TEMP_DATA, TEMP_ADMIN_COURSE } from './Reducer'


const StoreContext = createContext()

const StoreProvider = (props) => {

    const store = {
        isUserLogged: false,
        is_admin_access: false,
        login_to_page: '',
        category_list: [],
        course_list: [],
        temp_product: {},
        login_alert: '',
        register_alert: '',
        register_btn_state: '',
        order_list: [],
        my_course: [],
        chapter: {},
        temp_chapter: {},
        temp_subchapter: {},
        temp_subchapter_2: {},
        course_progress: 0,
        profile: {},
        modal_chapter: false,
        modal_password: false,
        otp_valid: false,
        temp_chapter_content: {},
        exam_data: [],
        temp_exam: {},
        user_datas: [],
        temp_user_data: {},
        otp_err: '',
        temp_email: '',
        practical_data: {},

        //chapter nav start
        current_chapter_id: 0,
        current_subchapter_id: 0,
        current_chapter_index: 0,
        //chapter nav end

        //admin
        live_chapter_data: [],
        temp_chapter_data: [],
        temp_admin_course: {},
        add_chapter_redirect: false,
        edit_chapter_redirect: false,
        add_subchapter_redirect: false,
        edit_subchapter_redirect: false,
        delete_chapter_status: false,
        delete_subchapter_status: false,
        emailVerifyRedirect: false,
        //admin

        cart: [],
        temp_cart: [],
        cart_alert: false,
        cart_alert_content: '',
        cart_total: 0
    }

    const [useStore, dispatch] = useReducer(ShopReducer, store)

    useEffect(() => {
        // console.log('Use Effect')
        // console.log('Sahad test.....!')
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        // console.log(token)

        if(token) {
            dispatch({ type: LOGGIN_STATUS })
        }
        
        category_list() //update category list reference
        orders_list()
        my_courses()
        profile()
        userDetails()
        //cart_list() //update cartlist reference
    },[])

    const login = (email, password) => {
        axios.post(BASE_URL + 'user/login', {email, password}, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(async response => {
            await dispatch({ type: LOGIN, payload: response.data })
            default_functions()
        })
        .catch(error => {
            const { data } = error.response
            dispatch({ type: LOGIN_ERR, payload: data })
            
        })
    }

    const default_functions = () => {
        cart_list() //update cartlist reference
        orders_list()
        my_courses()
        profile()
    }

    const profile_update = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        fetch(BASE_URL + 'user/profile', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).then(res => res.text())
        .then(resp => {
            if(resp === 'Updated successful') {
                profile()
                swal("Profile updated!", 'Updated profile', "success");
            } else {
                swal("Error!", resp, "error");
            }
        })
                
    }

    const register = (name, email, password) => {
        axios.post(BASE_URL + 'user/register', {name, email, password}, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(async response => {
            register_btn_state('')
            swal("confirm your identity!", 'Check you email and verify your identity ', "info");
        })
        .catch(error => {
            const { data } = error.response
            dispatch({ type: REGISTER_ERR, payload: data })
        })
    }

    const register_btn_state = (data) => {
        dispatch({ type: 'REGISTER_BTN_STATE', payload: data })
    }

    const update_password = (current_password, new_password) => {
        var data = {
            old_password: current_password,
            new_password
        }
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'user/password', data, {
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'bearer ' + token
            }
        })
        .then(async response => {
            swal("Profile updated!", response.data, "success");
        })
        .catch(error => {
            if(error.response.data) {
                swal("Error!", error.response.data, "error");
            } 
        })
    }

    const profile = () => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'user/profile', { 
            headers: {
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + token
            }
        }).then(response => {
            dispatch({ type: PROFILE, payload: response.data })
        })
    }

    const admin_login = (email, password) => {
        axios.post(BASE_URL + 'admin/login', {email, password}, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(async response => {
            await dispatch({ type: ADMIN_LOGIN, payload: response.data })
            userDetails()
        })
        .catch(error => {
            dispatch({ type: LOGIN_ERR, payload: 'Invalid username or password' })
            
        })
    }

    const logout = () => {
        swal({
            title: "Are you sure?",
            text: "Do you want to logout!?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
                
              }, dispatch({ type: LOGOUT }));
            } 
          });

    }

    const category_list = () => {
        axios.get(BASE_URL + 'course/category_list')
            .then(response => {
            dispatch({ type: CATEGORY_LIST, payload: response.data })
        })
    }
    
    const set_temp_product = (product) => {
        dispatch({ type: TEMP_PRODUCT, payload: product })
    }

    const set_temp_chapter = (chapter) => {
        dispatch({ type: TEMP_CHAPTER, payload: chapter })
    }

    const set_temp_subchapter_2 = (chapter) => {
        dispatch({ type: TEMP_SUBCHAPTER_2, payload: chapter })
    }

    const set_course_progress = () => {
        dispatch({ type: SET_COURSE_PROGRESS })
    }

    const set_temp_subchapter = (subchapter) => {
        dispatch({ type: TEMP_SUBCHAPTER, payload: subchapter })
    }

    const set_chapter = (order_id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'mycourse/' + order_id, {
            headers: {
              "Content-Type": "application/json",
              "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: SET_CHAPTER, payload: response.data })
            set_course_progress()
        })
    }

    const set_admin_chapter = (course_id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'course/all_chapters/' + course_id, {
            headers: {
              "Content-Type": "application/json",
              "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: SET_CHAPTER, payload: response.data })
        }).catch(err => {
            dispatch({ type: SET_CHAPTER, payload: [] })
        })
    }

    const my_courses = () => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'mycourse/', {
            headers: {
              "Content-Type": "application/json",
              "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: MY_COURSE, payload: response.data })
        })
    }

    const orders_list = () => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'order', {
            headers: {
              "Content-Type": "application/json",
              "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: ORDER_LIST, payload: response.data })
        })
    }

    const add_to_cart = (id) => {

        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check product alredy added to cart

        const order = useStore.order_list.filter(order_item => order_item.course_id === id);
        if(order.length === 0) {
            const product = useStore.cart.filter(cart => cart.course_data.course_id === id);
            if(product.length === 0) {
                axios.post(BASE_URL + 'cart', {course_id: id}, {
                    headers: {
                    "Content-Type": "application/json",
                    "Authorization": "bearer " + token
                    }
                }).then(async response => {
                    await dispatch({ type: COURSE_LIST, payload: response.data })
                    await dispatch({ type: CART_ALERT, payload: 'Course added to cart' })
                    cart_list()

                })
            } else {
                dispatch({ type: CART_ALERT, payload: 'Course already added to cart' })
            }
        } else {
            dispatch({ type: CART_ALERT, payload: 'Course already purchased' })
        }
            
    }

    const resetCart_alert = () => {
        dispatch({ type: CART_ALERT, payload: '' })
    }

    const deleteCart = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')

        //check product alredy added to cart
        const product = useStore.cart.filter(cart => cart.course_data.course_id === id);
        if(product.length === 1) {
            axios.delete(BASE_URL + 'cart', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "bearer " + token
                },
                data: {
                    course_id: id
                }
            }).then(response => {
               cart_list()
            })
        }
    }

    const addCategory = (name) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check product alredy added to cart
        const category = useStore.category_list.filter(category_item => category_item.category_name.toLowerCase() === name.toLowerCase());
        if(category.length === 0) {
          axios.post(BASE_URL + 'admin/category', {category_name: name}, {
                headers: {
                  "Content-Type": "application/json",
                  "Authorization": "bearer " + token
                }
            }).then(response => {
                category_list()
            })
        }
        
    }

    const makeOrder = (course_id) => {

        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')

        
            axios.post(BASE_URL + 'order', {course_id: course_id }, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "bearer " + token
                }
            }).then(response => {
                my_courses()
                swal("Purchase Completed!", response.data.message, "success");
            }).catch(err => {
            if(err) {
                swal("Error", 'Error in purchasing', "error");
            }
            })
        
        
    }

    const addCourse = (data, image) => {

        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
       const { course_name, introduction, course_offered, skills, about, category_id, type, duration, award, language, level, price, offer_percentage } = data

        var FormBody = new FormData();
            FormBody.append('course_name', course_name);
            FormBody.append('category_id', category_id);
            FormBody.append('price', price);
            FormBody.append('offer_percentage', offer_percentage);
            FormBody.append('introduction', introduction);
            FormBody.append('skills', skills);
            FormBody.append('about', about);
            FormBody.append('type', type);
            FormBody.append('duration', duration);
            FormBody.append('award', award);
            FormBody.append('language', language);
            FormBody.append('level', level);            
            FormBody.append('image', image);
            FormBody.append('course_offered', course_offered);

            axios({
                method: 'post',
                url: BASE_URL + 'admin/product',
                data: FormBody,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    "Authorization": "bearer " + token
                }
            }).then(function (response) {
                    //handle success
                    category_list()
                    // alert(response.data.message)
                    swal("Course added!", response.data.message, "success");

            }).catch(function (response) {
                    //handle error
                   
                    // alert('Error while adding')
                    swal("Error!", 'Error while adding', "error");

            });

    }

    const updateCourse = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')

        fetch(BASE_URL + 'admin/product', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).then(res => {
            if(res.status === 200) {
                category_list()
                swal("Updated", "Updated Successful", "info");
            } else {
                swal("Error!", 'Error while adding', "error");
            }
        })

    }

    const update_course_image = (image, image_name) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        var FormBody = new FormData();          
            FormBody.append('image', image, image_name);
            axios({
                method: 'put',
                url: BASE_URL + 'admin/image/product',
                data: FormBody,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    "Authorization": "bearer " + token
                }
            }).then(function (response) {
                    //handle success
                   category_list()
                    // alert(response.data.message)
                    swal("Updated", response.data.message, "info")

            }).catch(err => {
            }).then(res => {
                category_list()
            })

    }

    const deleteCourse = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check category alredy present
        // const product = useStore.category_list.filter(category => category.category_id === id);
     
            axios.delete(BASE_URL + 'admin/product/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "bearer " + token
                }
            }).then(response => {
                //alert(response.data)
                swal("Deleted!", response.data, "info");
                category_list()
            }).catch(err => {
            })
    }

    const deleteCategory = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check category alredy present
        const product = useStore.category_list.filter(category => category.category_id === id);
        if(product.length === 1) {
            axios.delete(BASE_URL + 'admin/category/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "bearer " + token
                }
            }).then(response => {
                category_list()
            })
        }
    }

    const updateCategory = (id, name) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        var data = {
            category_id: id,
            category_name: name
        }
        fetch(BASE_URL + 'admin/category', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).then(res => {
            category_list()
        })
    }

    const cart_list = () => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'cart', {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
              }
        })
        .then(async response => {
            await dispatch({ type: CART_LIST, payload: response.data })
            await cart_total()
        });
    }

    const cart_total = () => {
        dispatch({ type: CART_TOTAL })
    }

    const setLogin_to_page = (page) => {
        dispatch({ type: LOGIN_TO_PAGE, payload: page })
    }

    //chapter nav
    const set_current_chapter_id = (chapter_id) => {
        dispatch({ type: SET_CURRENT_CHAPTER_ID, payload: chapter_id })
    }

    const set_current_subchapter_id = (subchapter_id) => {
        dispatch({ type: SET_CURRENT_SUBCHAPTER_ID, payload: subchapter_id })
    }

    const set_current_chapter_index = (index) => {
        dispatch({ type: SET_CURRENT_CHAPTER_INDEX, payload: index })
    }

    const set_read_chapter = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'mycourse/read_chapter', data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({type: READ_CHAPTER_STATUS, payload: data})
            set_course_progress()
            swal("Chapter!", 'Completed', "success");
            
        }).catch(err => {
        if(err) {
            swal("Error", 'chapter readed', "error");
        }
        })
    }

    const set_read_subchapter = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'mycourse/read_subchapter', data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({type: READ_SUBCHAPTER_STATUS, payload: data})
            set_course_progress()
            swal("Chapter!", 'Completed', "success");
            
        }).catch(err => {
        if(err) {
            swal("Error", 'chapter readed', "error");
        }
        })
    }

    const complete_course_status = (order_id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'mycourse/course_completed', {order_id}, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(response => {
            my_courses()
        }).catch(err => {
        })
    }

    const modify_temp_data = (data) => {
        dispatch({ type: MODIFY_TEMP_DATA, payload: data })
    }

    const temp_admin_course = (data) => {
        dispatch({ type: TEMP_ADMIN_COURSE, payload: data })
    }

    const delete_temp_data = (index) => {
        const { temp_chapter_data } = useStore
        let updation = temp_chapter_data.filter((item, indexx) => indexx !== index)
        dispatch({ type: MODIFY_TEMP_DATA, payload: updation })
    }

    const modify_individual_image = (new_image, ext, index, image_file) => {
        const { temp_chapter_data } = useStore
        let updation = temp_chapter_data
        updation[index].image = new_image
        updation[index].ext = ext
        updation[index].file = image_file
        dispatch({ type: MODIFY_TEMP_DATA, payload: updation })
    }

    const modify_individual_content = (content, index) => {
        const { temp_chapter_data } = useStore
        let updation = temp_chapter_data
        updation[index].content = content
        dispatch({ type: MODIFY_TEMP_DATA, payload: updation })
    }

    const add_chapter = async (chapter_data, image_data, title, chapterNo) => {
        var course_id = useStore.temp_product.course_id
        var update_data = {
            course_id: course_id,
            chapter_no: chapterNo,
            chapter_title: title,
            is_subchapters: 0,
            chapter_content: chapter_data
        }
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'admin/chapter', update_data, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            dispatch({ type: 'ADD_CHAPTER_REDIRECT', payload: true })
            set_admin_chapter(course_id)
            await image_data.forEach(async item => {
                const { image, name } = item

                const token =  Cookies.get("token") ? Cookies.get("token") : null;
                //const token = sessionStorage.getItem('token')
                var FormBody = new FormData();          
                    FormBody.append('image', image, name);
                    await axios({
                        method: 'put',
                        url: BASE_URL + 'admin/image/chapter',
                        data: FormBody,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            "Authorization": "bearer " + token
                        }
                    }).then(function (response) {
                            //handle success
                        category_list()
                        const course_id = useStore.temp_product.course_id
                        set_admin_chapter(course_id)

                        
                        
                        
                            // alert(response.data.message)
                            // swal("Updated", response.data.message, "info")
    
                    }).catch(err => {
                    }).then(res => {
                        category_list()
                    })
            });

            swal("Chapter!", 'Chapter adding successful', "success");
        }).catch(err => {
            swal("Error!", err.response.data, "error");
        })  
    }


    const edit_chapter = async (chapter_data, image_data, title, chapterNo) => {
        var chapter_id = useStore.temp_chapter.chapter_id
        var update_data = {
            chapter_id: chapter_id,
            chapter_no: parseInt(chapterNo),
            chapter_title: title,
            chapter_content: chapter_data
        }
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')


        fetch(BASE_URL + 'admin/chapter', {
            method: 'PUT',
            body: JSON.stringify(update_data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).then(async res => {
                if(res.status === 200) {
                    const course_id = useStore.temp_product.course_id
                    set_admin_chapter(course_id)
                    dispatch({ type: 'EDIT_CHAPTER_REDIRECT', payload: true })
                    await image_data.forEach(async item => {
                        const { image, name } = item
                        const token =  Cookies.get("token") ? Cookies.get("token") : null;
                        //const token = sessionStorage.getItem('token')
                        var FormBody = new FormData();          
                            FormBody.append('image', image, name);
                            await axios({
                                method: 'put',
                                url: BASE_URL + 'admin/image/chapter',
                                data: FormBody,
                                headers: {
                                    'Content-Type': 'multipart/form-data',
                                    "Authorization": "bearer " + token
                                }
                            }).then(function (response) {
                                    //handle success
                                category_list()
                                const course_id = useStore.temp_product.course_id
                                set_admin_chapter(course_id)
                                    // alert(response.data.message)
                                    // swal("Updated", response.data.message, "info")
            
                            }).catch(err => {
                            }).then(res => {
                                category_list()
                            })
                    });
                    swal("Updated", 'Chapter updated successful', "info");
                } else {
                    swal("Error!", 'Error while updationg', "error");
                }
            
        }).catch(err => {
            
        });

 
    }


    const add_subchapter = async (chapter_data, image_data, title) => {
        var course_id = useStore.temp_product.course_id
        const { chapter_id, is_subchapters } = useStore.temp_chapter
        var update_data = {
            chapter_id: chapter_id,
            course_id: course_id,
            sub_chapter_title: title,
            sub_chapter_contents: chapter_data
        }

        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')

        if(is_subchapters === 0) {
            var update_data_chaptr = {
                chapter_id: chapter_id,
                is_subchapters: 1
            }
            await fetch(BASE_URL + 'admin/chapter', {
                method: 'PUT',
                body: JSON.stringify(update_data_chaptr),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer ' + token
                }
            }).then(async res => {
                    
            }).catch(err => {
                
            });
        }

        axios.post(BASE_URL + 'admin/sub_chapter', update_data, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            dispatch({ type: 'ADD_SUBCHAPTER_REDIRECT', payload: true })
            const course_id = useStore.temp_product.course_id
            set_admin_chapter(course_id)
            await image_data.forEach(async item => {
                const { image, name } = item
                const token =  Cookies.get("token") ? Cookies.get("token") : null;
                //const token = sessionStorage.getItem('token')
                var FormBody = new FormData();          
                    FormBody.append('image', image, name);
                    await axios({
                        method: 'put',
                        url: BASE_URL + 'admin/image/sub_chapter',
                        data: FormBody,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            "Authorization": "bearer " + token
                        }
                    }).then(function (response) {
                            //handle success
                        category_list()
                        const course_id = useStore.temp_product.course_id
                        set_admin_chapter(course_id)
                        
                            // alert(response.data.message)
                            // swal("Updated", response.data.message, "info")
    
                    }).catch(err => {
                    }).then(res => {
                        category_list()
                    })
            });

            swal("Sub Chapter!", 'Chapter adding successful', "success");
        }).catch(err => {
            swal("Error!", err.response.data, "error");
        })  
    }

    const edit_subchapter = async (chapter_data, image_data, title) => {
        var sub_chapter_id = useStore.temp_subchapter.sub_chapter_id
        var update_data = {
            sub_chapter_id: sub_chapter_id,
            sub_chapter_title: title,
            sub_chapter_contents: chapter_data
        }
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')

        fetch(BASE_URL + 'admin/sub_chapter', {
            method: 'PUT',
            body: JSON.stringify(update_data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).then(async res => {
                if(res.status === 200) {
                    dispatch({ type: 'EDIT_SUBCHAPTER_REDIRECT', payload: true })
                    const course_id = useStore.temp_product.course_id
                    set_admin_chapter(course_id)
                    await image_data.forEach(async item => {
                        const { image, name } = item
                        const token =  Cookies.get("token") ? Cookies.get("token") : null;
                        //const token = sessionStorage.getItem('token')
                        var FormBody = new FormData();          
                            FormBody.append('image', image, name);
                            await axios({
                                method: 'put',
                                url: BASE_URL + 'admin/image/sub_chapter',
                                data: FormBody,
                                headers: {
                                    'Content-Type': 'multipart/form-data',
                                    "Authorization": "bearer " + token
                                }
                            }).then(function (response) {
                                    //handle success
                                category_list()
                                const course_id = useStore.temp_product.course_id
                                set_admin_chapter(course_id)
                                
                                    // alert(response.data.message)
                                    // swal("Updated", response.data.message, "info")
            
                            }).catch(err => {
                            }).then(res => {
                                category_list()
                            })
                    });
                    swal("Updated", 'Chapter updated successful', "info");
                } else {
                    swal("Error!", 'Error while updationg', "error");
                }
            
        }).catch(err => {
            
        });

    }

    const status_add_chapter = () => {
        dispatch({ type: 'ADD_CHAPTER_REDIRECT', payload: false })
    }

    const status_edit_chapter = () => {
        dispatch({ type: 'EDIT_CHAPTER_REDIRECT', payload: false })
    }

    const status_add_subchapter = () => {
        dispatch({ type: 'ADD_SUBCHAPTER_REDIRECT', payload: false })
    }

    const status_edit_subchapter = () => {
        dispatch({ type: 'EDIT_SUBCHAPTER_REDIRECT', payload: false })
    }

    const model_chapter_open = () => {
        dispatch({ type: 'CHAPTER_MODAL_OPEN' })
    }

    const modal_password_open = () => {
        dispatch({ type: 'PASSWORD_MODAL_OPEN' })
    }

    const modal_password_close = () => {
        dispatch({ type: 'PASSWORD_MODAL_CLOSE' })
    }
    

    const model_chapter_close = () => {
        dispatch({ type: 'CHAPTER_MODAL_CLOSE' })
    }

    const set_temp_chapter_content = (data) => {
        dispatch({ type: 'SET_TEMP_CHAPTER_CONTENT', payload: data })
    }

    const delete_chapter_status = (data) => {
        dispatch({ type: 'DELETE_CHAPTER_STATUS', payload: false })
    }

    const delete_subchapter_status = (data) => {
        dispatch({ type: 'DELETE_SUBCHAPTER_STATUS', payload: false })
    }
    

    const delete_chapter = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check category alredy present
        swal({
            title: "Are you sure?",
            text: "Do you want to delete subchapter!?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Subchapter has been deleted!", {
                icon: "success",
                
              }, 

              axios.delete(BASE_URL + 'admin/chapter/' + id, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "bearer " + token
                    }
                }).then(response => {
                    //alert(response.data)
                    dispatch({ type: 'DELETE_CHAPTER_STATUS', payload: true })
                    const course_id = useStore.temp_product.course_id
                    set_admin_chapter(course_id)
                    swal("Deleted!", response.data, "info");
                    category_list()
                }).catch(err => {
                    swal("Deleted!", 'Error while deleting', "info");
                })
              
              );
            } 
          });
        
    }


    const delete_subchapter = (id, chapter_id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check category alredy present
        swal({
            title: "Are you sure?",
            text: "Do you want to delete subchapter!?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Subchapter has been deleted!", {
                icon: "success",
                
              }, 

              axios.delete(BASE_URL + 'admin/sub_chapter/' + id + '/' + chapter_id, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "bearer " + token
                    }
                }).then(response => {
                    //alert(response.data)
                    dispatch({ type: 'DELETE_SUBCHAPTER_STATUS', payload: true })
                    const course_id = useStore.temp_product.course_id
                    set_admin_chapter(course_id)
                    swal("Deleted!", response.data, "info");
                    category_list()
                }).catch(err => {
                    swal("Delete error!", 'Error while deleting', "info");
                })
              
              );
            } 
          });

        
    }

    const temp_exam = (data) => {
        dispatch({ type: 'TEM_EXAM', payload: data })
    }

    const exam_data = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'admin/exam/' + id,{
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: 'EXAM_DATA', payload: response.data })
        })
    }

    const add_exam_question = (question, options) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        var course_id = useStore.temp_product.course_id
        var data = {
            question: {
                question_title: question,
                course_id: course_id
            },
            options: options
        }

        axios.post(BASE_URL + 'admin/exam', data, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            model_chapter_close()
            exam_data(course_id)
            swal("Chapter!", 'Chapter adding successful', "success");
        }).catch(err => {
            swal("Error!", err.response.data, "error");
        })  
    }

    const edit_exam_question = (question, options) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        var course_id = useStore.temp_product.course_id
        var data = {
            question: question,
            options: options
        }

        axios.post(BASE_URL + 'admin/exam/question', data.question, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            await axios.post(BASE_URL + 'admin/exam/options', data.options, {
                headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
                }
            }).then(response => {
                model_chapter_close()
                exam_data(course_id)
                swal("Updated!", 'Question', "success");
            }).catch(err => {
                swal("Error!", err.response.data, "error");
            })  

            
        }).catch(err => {
            // swal("Error!", err.response.data, "error");
        })  

        
    }

    const delete_exam = (id) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        //check category alredy present
        var course_id = useStore.temp_product.course_id
        swal({
            title: "Are you sure?",
            text: "Do you want to delete subchapter!?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Subchapter has been deleted!", {
                icon: "success",
                
              }, 

              axios.delete(BASE_URL + 'admin/exam/' + id, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "bearer " + token
                    }
                }).then(response => {
                    //alert(response.data)
                    exam_data(course_id)
                swal("Updated!", 'Question', "success");
                    swal("Deleted!", response.data, "info");
                    category_list()
                }).catch(err => {
                    swal("Deleted!", 'Error while deleting', "info");
                })
              
              );
            } 
          });
        
    }

    const verifyEmail = (token) => {
        axios.get(BASE_URL + 'user/verify-email',{
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(async response => {
            var data = {
                token: token
            }
            await dispatch({ type: LOGIN, payload: data })
            default_functions()
            swal("Verified!", 'Your Account Verified', "success");
            emailVerificationRedirect(true)
            
        }).catch(err => {
            swal("Error!", "Account verification error, Invalid token!", "error");
            emailVerificationRedirect(true)
        })
    }


    const emailVerificationRedirect = (data) => {
        dispatch({ type: 'EMAI_VERIFY_REDIRECT', payload: data })
    }

    

    const temp_user_data = (data) => {
        dispatch({ type: 'TEMP_USER_DATA', payload: data })
    }

    const institutionContactForm = (data) => {
        axios.post(BASE_URL + 'institution/contact-form', data, {
            headers: {
            "Content-Type": "application/json",
            }
        }).then(async response => {
           register_btn_state('')
           swal("Thanks!", 'Thank you for showing intrest to associate with Alaska', "success");
            
        }).catch(err => {
            register_btn_state('')
            swal("Error!", err.response.data, "error");
        })  
    }

    const userDetails = () => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.get(BASE_URL + 'admin/users',{
            headers: {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            }
        }).then(response => {
            dispatch({ type: 'USER_DATAS', payload: response.data })
            
        }).catch(err => {
            // swal("Error!", "Account verification error, Invalid token!", "error");
            // emailVerificationRedirect(true)
        })
    }

    const VerifyEmailRetry = (data) => {
        axios.post(BASE_URL + 'user/verify-email-retry', data, {
            headers: {
            "Content-Type": "application/json"
            }
        }).then(async response => {
            model_chapter_close()
            swal("Confirm your identity!", 'Check you email and verify your identity ', "info");
        }).catch(err => {
            swal("Error!", err.response.data, "error");
        }) 
    }

    const OtpValid = (data) => {
        dispatch({ type: 'OTP_VALID', payload: data })
    }

    const OtpError = (data) => {
        dispatch({ type: 'OTP_ERR', payload: data })
    }

    const temp_email_fx = (data) => {
        dispatch({ type: 'TEMP_EMAIL', payload: data })
    }
    
    const generate_otp = (data) => {
        axios.post(BASE_URL + 'user/send-otp', data, {
            headers: {
            "Content-Type": "application/json"
            }
        }).then(async response => {
            OtpError(response.data)
        }).catch(err => {
            OtpError(err.response.data)
        }) 
    }

    const verify_otp = (data) => {
        axios.post(BASE_URL + 'user/verify-otp', data, {
            headers: {
            "Content-Type": "application/json"
            }
        }).then(async response => {
            OtpError('')
            OtpValid(true)
        }).catch(err => {
            OtpError(err.response.data)
        }) 
    }

    const reset_password = (data) => {
        axios.post(BASE_URL + 'user/reset-password', data, {
            headers: {
            "Content-Type": "application/json"
            }
        }).then(async response => {
            modal_password_close()
            OtpError('')
            OtpValid(false)
            swal("Updated!", 'Password reset successfull, You can login with new password', "success");

        }).catch(err => {
            OtpError(err.response.data)
        }) 
    }

    const practical_course = (id) => {
            axios.get(BASE_URL + 'practicals/' + id)
            .then(response => {
                dispatch({ type: 'PRACTICAL_DATA', payload: response.data })
            }).catch(err => {
                dispatch({ type: 'PRACTICAL_DATA', payload: {} })
            })
    }
   
    const add_practical_course = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'admin/practical', data, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            const course_id = useStore.temp_product.course_id
            set_admin_chapter(course_id)
            practical_course(course_id)
            swal("Addedd!", 'Practical chapter added', "success");

        }).catch(err => {
            swal("Error!", err.response.data, "error");
        }) 
    }

    const update_practical_course = (data) => {
        const token =  Cookies.get("token") ? Cookies.get("token") : null;
        //const token = sessionStorage.getItem('token')
        axios.post(BASE_URL + 'admin/practical/update', data, {
            headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
            }
        }).then(async response => {
            const course_id = useStore.temp_product.course_id
            set_admin_chapter(course_id)
            practical_course(course_id)
            swal("Updated!", 'Practical chapter added', "success");

        }).catch(err => {
            swal("Error!", err.response.data, "error");
        }) 
    }


    return (
        <StoreContext.Provider value={{
            store: useStore,
            //functions
            set_temp_product,
            add_to_cart,
            deleteCart,
            cart_list,
            resetCart_alert,
            login,
            register,
            reg_btn_state: register_btn_state,
            setLogin_to_page,
            logout,
            admin_login,
            addCategory,
            deleteCategory,
            makeOrder,
            set_chapter,
            set_temp_chapter,
            set_temp_subchapter,
            set_temp_subchapter_2,
            set_course_progress,
            profile_update,
            update_password,
            verifyEmail,
            institutionContactForm,
            temp_user_data,
            VerifyEmailRetry,
            generate_otp,
            verify_otp,
            temp_email_fx,
            reset_password,
            OtpError,

            //chapter nav section
            set_current_chapter_id,
            set_current_subchapter_id,
            set_current_chapter_index,

            set_read_chapter,
            set_read_subchapter,
            complete_course_status,

            modify_temp_data,
            delete_temp_data,
            modify_individual_image,
            add_chapter,
            temp_admin_course,
            edit_chapter,
            add_subchapter,
            edit_subchapter,
            status_add_chapter,
            status_edit_chapter,
            status_add_subchapter,
            status_edit_subchapter,
            delete_chapter_status,
            delete_subchapter_status,

            updateCategory,
            addCourse,
            updateCourse,
            update_course_image,
            deleteCourse,
            set_admin_chapter,
            model_chapter_open,
            model_chapter_close,
            modal_password_open,
            modal_password_close,
            set_temp_chapter_content,
            modify_individual_content,
            delete_chapter,
            delete_subchapter,
            exam_data,
            temp_exam,
            add_exam_question,
            edit_exam_question,
            delete_exam,
            userDetails,
            practical_course,
            add_practical_course,
            update_practical_course

        }}>
            {props.children}
        </StoreContext.Provider>
    )
}

const StoreConsumer = StoreContext.Consumer;

export {StoreProvider, StoreConsumer}
