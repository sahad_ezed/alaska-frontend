import Cookies from "js-cookie"

export const CATEGORY_LIST = 'CATEGORY_LIST'
export const COURSE_LIST = 'COURSE_LIST'
export const CART_LIST = 'CART_LIST'
export const TEMP_PRODUCT = 'TEMP_PRODUCT'
export const ADD_TO_CART = 'ADD_TO_CART'
export const LOGIN = 'LOGIN'
export const LOGIN_TO_PAGE = 'LOGIN_TO_PAGE'
export const LOGIN_ERR = 'LOGIN_ERR'
export const REGISTER_ERR = 'REGISTER_ERR'
export const LOGOUT = 'LOGOUT'
export const CART_ALERT = 'CART_ALERT'
export const LOGGIN_STATUS = 'LOGGIN_STATUS'
export const CART_TOTAL = 'CART_TOTAL'
export const ORDER_LIST = 'ORDER_LIST'
export const SET_CHAPTER = 'SET_CHAPTER'
export const MY_COURSE = 'MY_COURSE'
export const TEMP_CHAPTER = 'TEMP_CHAPTER'
export const TEMP_SUBCHAPTER = 'TEMP_SUBCHAPTER'
export const SET_CURRENT_CHAPTER_ID = 'SET_CURRENT_CHAPTER_ID'
export const SET_CURRENT_SUBCHAPTER_ID = 'SET_CURRENT_SUBCHAPTER_ID'
export const SET_CURRENT_CHAPTER_INDEX = 'SET_CURRENT_CHAPTER_INDEX'
export const READ_CHAPTER_STATUS = 'READ_CHAPTER_STATUS'
export const READ_SUBCHAPTER_STATUS = 'READ_SUBCHAPTER_STATUS'
export const TEMP_SUBCHAPTER_2 = 'TEMP_SUBCHAPTER_2'
export const SET_COURSE_PROGRESS = 'SET_COURSE_PROGRESS'
export const PROFILE = 'PROFILE'
export const MODIFY_LIVE_DATA = 'MODIFY_LIVE_DATA'
export const MODIFY_TEMP_DATA = 'MODIFY_TEMP_DATA'
export const TEMP_ADMIN_COURSE = 'TEMP_ADMIN_COURSE' 
export const CHAPTER_MODAL_OPEN = 'CHAPTER_MODAL_OPEN' 
export const CHAPTER_MODAL_CLOSE = 'CHAPTER_MODAL_CLOSE' 
export const SET_TEMP_CHAPTER_CONTENT = 'SET_TEMP_CHAPTER_CONTENT'



export const ADMIN_LOGIN = 'ADMIN_LOGIN'


const api_category_list = (payload, state) => {
     let updateStore = {...state, category_list: payload}
     return updateStore
}

const api_order_list = (payload, state) => {
    let updateStore = {...state, order_list: payload}
    return updateStore
}

const api_course_list = (payload, state) => {
    let updateStore = {...state, course_list: payload}
    return updateStore
}

const temp_product = (payload, state) => {
    let updateStore = {...state, temp_product: payload}
    return updateStore
}

const temp_chapter = (payload, state) => {
    let updateStore = {...state, temp_chapter: payload}
    return updateStore
}

const temp_subchapter_2 = (payload, state) => {
    let updateStore = {...state, temp_subchapter_2: payload}
    return updateStore
}

const temp_subchapter = (payload, state) => {
    let updateStore = {...state, temp_subchapter: payload}
    return updateStore
}

const add_to_cart = (payload, state) => {
    return state
}

const cart_list = (payload, state) => {
    let updateStore = {...state, cart: payload}
    return updateStore
}

const login_error = (payload, state) => {
    let updateStore = {...state, login_alert: payload}
    return updateStore
}

const register_error = (payload, state) => {
    let updateStore = {...state, register_alert: payload}
    return updateStore
}

const login = (payload, state) => {
    let updateStore = {...state, isUserLogged: true}
    Cookies.set("token", payload.token);
    //sessionStorage.setItem('token', payload.token)
    return updateStore
}

const admin_login = (payload, state) => {
    let updateStore = {...state, isUserLogged: true, is_admin_access: true}
    Cookies.set("token", payload.token);
    Cookies.set("admin", true);
    //sessionStorage.setItem('token', payload.token)
    //sessionStorage.setItem('admin', true)
    return updateStore
}

const logout = (state) => {
    let updateStore = {...state, isUserLogged: false, is_admin_access: false}
    Cookies.remove('token')
    Cookies.remove('admin')
    //sessionStorage.clear()
    return updateStore
}


const cart_alert = (payload, state) => {
    let updateStore = {...state, cart_alert: !state.cart_alert, cart_alert_content: payload }
    return updateStore
}


const loggin_status = (state) => {

    // const admin = sessionStorage.getItem('admin')
    const admin =  Cookies.get("admin") ? Cookies.get("admin") : null
    let updateStore = {}
    if(admin) {
        updateStore = {...state, isUserLogged: true, is_admin_access: true }
    } else {
        updateStore = {...state, isUserLogged: true }
    }
    
    return updateStore
}

const login_to_page = (payload, state) => {
    let updateStore = {...state, login_to_page: payload }
    return updateStore
}

const cart_total = (state) => {
    let cart = state.cart;
    var price = 0
    

    cart.forEach(item => {
        var item_price = item.course_data.price
        var offer = (item.course_data.offer_percentage / 100) * item_price
        var real_price = item_price - offer
        // real_price = real_price.toFixed(2)
        price = price + real_price
    });
    let updateStore = {...state, cart_total: price.toFixed(2)}

    return updateStore
}

const setChapter = (payload, state) => {
    let updateStore = {...state, chapter: payload }
    return updateStore
}

const my_course = (payload, state) => {
    let updateStore = {...state, my_course: payload }
    return updateStore
}

//chapter nav
const set_current_chapter_id = (payload, state) => {
    let updateStore = {...state, current_chapter_id: payload}
    return updateStore
}

const set_current_subchapter_id = (payload, state) => {
    let updateStore = {...state, current_subchapter_id: payload}
    return updateStore
}

const set_current_chapter_index = (payload, state) => {
    let updateStore = {...state, current_chapter_index: payload}
    return updateStore
}

const read_chapter_status = (payload, state) => {
    const {chapter} = state
    let update_data = chapter.chapter
    let update_chapter = update_data.find(item => item.chapter_id === payload.chapter_id)
        update_chapter.status = true
    let updateStore = {...state, temp_chapter: update_chapter}
    return updateStore
}

const read_subchapter_status = (payload, state) => {
    const {chapter, temp_subchapter_2 } = state
    let update_data = chapter.chapter

    let update_chapter = update_data.find(item => item.chapter_id === temp_subchapter_2.chapter_id)
        let index = update_chapter.subchapters.indexOf(temp_subchapter_2)
        if(index !== -1) {
            update_chapter.subchapters[index].status = true
            let updateStore = {...state, temp_chapter: update_chapter}
            return updateStore
        } else {
            return state
        }
    // return state
     
}

const profile = (payload, state) => {
    let updateStore = {...state, profile: payload}
    return updateStore  
} 

const set_course_progress = (state) => {
    const { chapter } = state.chapter
    var completed_chapter = []
    var incomplete_chapter = []
    chapter.forEach(chapter_items => {
        const { status, is_subchapters, subchapters } = chapter_items
        if(is_subchapters) {
            subchapters.forEach(subchapter_items => {
                const subStauts = subchapter_items.status
                if(subStauts) {
                    completed_chapter.push('items')
                } else {
                    incomplete_chapter.push('items')
                }
            })
        } else {
            if(status) {
                completed_chapter.push('items')
            } else {
                incomplete_chapter.push('items')
            }
        }
    });

    var complete_count = completed_chapter.length
    var pending_count = incomplete_chapter.length
    var total_lessons = complete_count + pending_count
    var completed_percentage = parseInt((100/total_lessons) * complete_count) 

    let updateStore = {...state, course_progress: completed_percentage}
    return updateStore
}

const modify_live_data = (payload, state) => {
    return state
}

const modify_temp_data = (payload, state) => {
    let updateStore = {...state, temp_chapter_data: payload}
    return updateStore
}

const temp_admin_course = (payload, state) => {
    let updateStore = {...state, temp_admin_course: payload}
    return updateStore
}

const chapter_modal_open = (state) => {
    let updateStore = {...state, modal_chapter: true}
    return updateStore
}

const password_modal_open = (state) => {
    let updateStore = {...state, modal_password: true}
    return updateStore
}

const otp_valid = (payload, state) => {
    let updateStore = {...state, otp_valid: payload}
    return updateStore
}

const otp_error = (payload, state) => {
    let updateStore = {...state, otp_err: payload}
    return updateStore
}

const temp_email = (payload, state) => {
    let updateStore = {...state, temp_email: payload}
    return updateStore
}


const password_modal_close = (state) => {
    let updateStore = {...state, modal_password: false}
    return updateStore
}


const chapter_modal_close = (state) => {
    let updateStore = {...state, modal_chapter: false}
    return updateStore
}

const set_temo_chapter_content = (payload, state) => {
    let updateStore = {...state, temp_chapter_content: payload}
    return updateStore
}

const add_chapter_redirect = (payload, state) => {
    let updateStore = {...state, add_chapter_redirect: payload}
    return updateStore
}

const add_subchapter_redirect = (payload, state) => {
    let updateStore = {...state, add_subchapter_redirect: payload}
    return updateStore
}

const edit_chapter_redirect = (payload, state) => {
    let updateStore = {...state, edit_chapter_redirect: payload}
    return updateStore
}

const edit_subchapter_redirect = (payload, state) => {
    let updateStore = {...state, edit_subchapter_redirect: payload}
    return updateStore
}

const delete_chapter_status = (payload, state) => {
    let updateStore = {...state, delete_chapter_status: payload}
    return updateStore
}

const delete_subchapter_status = (payload, state) => {
    let updateStore = {...state, delete_subchapter_status: payload}
    return updateStore
}

const exam_data = (payload, state) => {
    let updateStore = {...state, exam_data: payload}
    return updateStore
}

const temp_exam = (payload, state) => {
    let updateStore = {...state, temp_exam: payload}
    return updateStore
}

const email_verify_redirect = (payload, state) => {
    let updateStore = {...state, emailVerifyRedirect: payload}
    return updateStore
}

const register_btn_state = (payload, state) => {
    let updateStore = {...state, register_btn_state: payload}
    return updateStore
}

const user_datas = (payload, state) => {
    let updateStore = {...state, user_datas: payload}
    return updateStore
}

const tem_user_data = (payload, state) => {
    let updateStore = {...state, temp_user_data: payload}
    return updateStore
}

const practical_data = (payload, state) => {
    let updateStore = {...state, practical_data: payload}
    return updateStore
}



export const ShopReducer = (state, action) => {
    switch (action.type) {
        case CATEGORY_LIST:
            return  api_category_list(action.payload, state)
        
        case CART_LIST:
            return  cart_list(action.payload, state)
        
        case COURSE_LIST:
            return  api_course_list(action.payload, state)

        case TEMP_PRODUCT:
            return  temp_product(action.payload, state)

        case ADD_TO_CART:
            return  add_to_cart(action.payload, state)

        case LOGIN:
            return  login(action.payload, state)

        case LOGIN_TO_PAGE:
            return  login_to_page(action.payload, state) 

        case LOGOUT:
            return  logout(state)

        case LOGIN_ERR:
            return  login_error(action.payload, state)

        case REGISTER_ERR:
            return register_error(action.payload, state)

        case CART_ALERT:
            return  cart_alert(action.payload, state)

        case LOGGIN_STATUS:
            return  loggin_status(state)

        case CART_TOTAL:
            return  cart_total(state)

        case ADMIN_LOGIN:
            return  admin_login(action.payload, state)

        case ORDER_LIST:
            return  api_order_list(action.payload, state)

        case SET_CHAPTER:
            return  setChapter(action.payload, state)

        case SET_COURSE_PROGRESS:
            return  set_course_progress(state)


        case MY_COURSE:
            return  my_course(action.payload, state)

        case TEMP_CHAPTER:
            return  temp_chapter(action.payload, state)

        case TEMP_SUBCHAPTER:
            return  temp_subchapter(action.payload, state)

        case TEMP_SUBCHAPTER_2:
            return  temp_subchapter_2(action.payload, state)

        case SET_CURRENT_CHAPTER_ID:
            return  set_current_chapter_id(action.payload, state)

        case SET_CURRENT_SUBCHAPTER_ID:
            return  set_current_subchapter_id(action.payload, state)

        case SET_CURRENT_CHAPTER_INDEX:
            return  set_current_chapter_index(action.payload, state)

        case READ_CHAPTER_STATUS:
            return  read_chapter_status(action.payload, state)

        case READ_SUBCHAPTER_STATUS:
            return  read_subchapter_status(action.payload, state)

        case PROFILE:
            return  profile(action.payload, state)

        case MODIFY_LIVE_DATA:
            return modify_live_data(action.payload, state)

        case MODIFY_TEMP_DATA:
            return modify_temp_data(action.payload, state)

        case TEMP_ADMIN_COURSE:
            return temp_admin_course(action.payload, state)

        case CHAPTER_MODAL_OPEN:
            return chapter_modal_open(state)
        
        case 'PASSWORD_MODAL_OPEN':
            return password_modal_open(state)

        case 'PASSWORD_MODAL_CLOSE':
            return password_modal_close(state)

        case CHAPTER_MODAL_CLOSE:
            return chapter_modal_close(state)

        case SET_TEMP_CHAPTER_CONTENT:
            return set_temo_chapter_content(action.payload, state)

        case 'ADD_CHAPTER_REDIRECT':
            return add_chapter_redirect(action.payload, state)

        case 'ADD_SUBCHAPTER_REDIRECT':
            return add_subchapter_redirect(action.payload, state)

        case 'EDIT_CHAPTER_REDIRECT':
            return edit_chapter_redirect(action.payload, state)

        case 'EDIT_SUBCHAPTER_REDIRECT':
            return edit_subchapter_redirect(action.payload, state)

        case 'DELETE_CHAPTER_STATUS':
            return delete_chapter_status(action.payload, state)

        case 'DELETE_SUBCHAPTER_STATUS':
            return delete_subchapter_status(action.payload, state)

        case 'EXAM_DATA':
            return exam_data(action.payload, state)
        
        case 'TEM_EXAM':
            return temp_exam(action.payload, state)

        case 'EMAI_VERIFY_REDIRECT':
            return email_verify_redirect(action.payload, state)

        case 'REGISTER_BTN_STATE':
            return register_btn_state(action.payload, state)

        case 'USER_DATAS':
            return user_datas(action.payload, state)

        case 'TEMP_USER_DATA':
            return tem_user_data(action.payload, state)

        case 'OTP_VALID':
            return otp_valid(action.payload, state)

        case 'OTP_ERR':
            return otp_error(action.payload, state)

        case 'TEMP_EMAIL':
            return temp_email(action.payload, state)

        case 'PRACTICAL_DATA':
            return practical_data(action.payload, state)

        default:
            return state
    }
}   