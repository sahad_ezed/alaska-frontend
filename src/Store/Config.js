//const BASE_MAIN = 'https://backend.alaskainstitutes.com/'
const BASE_MAIN = 'http://localhost:8080/'


export const BASE_URL = BASE_MAIN + 'api/'

export const COURSE_IMAGE_URL = BASE_MAIN + 'images/course/'
export const CHAPTER_IMAGE_URL = BASE_MAIN + 'images/chapter/'
export const SUBCHAPTER_IMAGE_URL = BASE_MAIN + 'images/subchapter/'

