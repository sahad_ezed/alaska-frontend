import React, {useEffect} from 'react'
import { Redirect } from 'react-router-dom'
import { StoreConsumer } from '../../Store/Store';

function EmailVerify(props) {

    

    // const[redirect, setRedirect] = useState(false)

    // if(redirect) return <Redirect to="/" />
    
    
    const ApiCall = ({values}) => {
        const { verifyEmail } = values
        const { emailVerifyRedirect } = values.store
        const { id } = props.match.params 
        
        useEffect(() => {
            verifyEmail(id)
        })

        if(emailVerifyRedirect) return <Redirect to='/' />

        return (
            <div>
                <h3>Email Verification page</h3>
            </div>
        )
    }

    return (
        <StoreConsumer>
            {values => {
                return (
                    <ApiCall values={values}/>
                )
            }}
        </StoreConsumer>
    )
}

export default EmailVerify
