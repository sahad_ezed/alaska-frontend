import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'
import ChapterDetailed from '../Elements/Chapter/ChapterDetailed'
import ChaptersNav from '../Elements/Chapter/ChaptersNav'
import { StoreConsumer } from '../../Store/Store'

function ChapterDetail() {

    const RenderButton = ({values}) => {
        const { chapter, course_progress } = values.store
        if(chapter) {
            const { exam } = chapter
            if(exam) {
                if(exam.length > 0) {
                    if(course_progress === 100) {
                        return (
                            <div className="index-section px-3 mt-3">
                                <p className="h5 font-weight-bold">Exam</p>
                                <div className="d-flex justify-content-center pt-3">
                                   <Link style={{ color: '#fff', textDecoration: 'none'}} to="/exam"><button className="btn btn-info px-5">Start Exam</button></Link> 
                                </div>
                            </div>
                        )
                    } else {
                        return (
                            <div className="index-section px-3 mt-3">
                                <p className="h5 font-weight-bold">Exam</p>
                                <div className="d-flex justify-content-center pt-3">
                                    <button disabled className="btn btn-info px-5">Start Exam</button>
                                </div>
                            </div>
                        )
                    }
                    
                } else return null
            } else return null
        } else return null

        
    }

    return (
        <StoreConsumer>
            {values => {
                const { course_progress } = values.store
                var width = course_progress + '%'
                
                return (
                    <Fragment>
                        <section id="chapter-section">
                            <div className="container">
                                <div className="row">
            
                                    <div className="col-md-9 col-sm-7 chapter-details-layout mb-3">
                                        <ChapterDetailed />
                                    </div>
            
            
                                    <div className="col-md-3 col-sm-4 ">
                                        
                                        <div className="mb-3 bg-white pb-2 pt-3 px-3 index-section">
                                            <p className="h5 font-weight-bold">Progress</p>
                                            <div style={{ height: 10, width: '80%' }} className="progress">
                                                <div className="progress-bar bg-warning " style={{width: width}}></div>
                                            </div>
                                            <h3 className="progress-text pt-2"><p>{course_progress}% Complete</p></h3>
                                        </div>
                                    
                                        <div className="index-section px-3">
                                            <p className="h5 font-weight-bold">Content</p>
                                            <p className="text-right pr-2">Course Home</p>
                                            <ChaptersNav />
                                        </div>

                                        <RenderButton values={values} />
                                        
                                    </div>
            
                                </div>
                            </div>
                        </section>
                    </Fragment>
                )
            }}
        </StoreConsumer>
       
    )
}

export default ChapterDetail
