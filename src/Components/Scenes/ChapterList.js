import React from 'react'
import ChapterLists from '../Elements/Chapter/ChapterLists'
import ChaptersNav from '../Elements/Chapter/ChaptersNav'
import { StoreConsumer } from '../../Store/Store'
import { Link } from 'react-router-dom'


function ChapterList() {

    
    const RenderButton = ({values}) => {
        const { chapter, course_progress } = values.store
        if(chapter) {
            const { exam } = chapter
            if(exam) {
                if(exam.length > 0) {
                    if(course_progress === 100) {
                        return (
                            <div className="index-section px-3 mt-3">
                                <p className="h5 font-weight-bold">Exam</p>
                                <div className="d-flex justify-content-center pt-3">
                                   <Link style={{ color: '#fff', textDecoration: 'none'}} to="/exam"><button className="btn btn-info px-5">Start Exam</button></Link> 
                                </div>
                            </div>
                        )
                    } else {
                        return (
                            <div className="index-section px-3 mt-3">
                                <p className="h5 font-weight-bold">Exam</p>
                                <div className="d-flex justify-content-center pt-3">
                                    <button disabled className="btn btn-info px-5">Start Exam</button>
                                </div>
                            </div>
                        )
                    }
                    
                } else return null
            } else return null
        } else return null

        
    }

    return (
        <StoreConsumer>
            {values => {
                const { course_progress } = values.store
                var width = course_progress + '%'

                const { course_name, chapter, duration } = values.store.chapter
                var chapter_count = 0
                if(chapter) {
                    chapter_count = chapter.length
                }
                return (
                    <section id="chapter-section">
                        <div className="container">
                            <div className="row">
        
                                <div className="col-md-9 col-sm-8">
                                    <h3 className="font-weight-bold" >{course_name}</h3>
                                    <p className="" style={{ color: '#a2a6a3' }}>{chapter_count} chapters | {duration} </p>
                                    <h4 className="pb-3">Chapters</h4>
                                    <ChapterLists values={values}/>
                                </div>
        
        
                                <div className="col-md-3 col-sm-4 ">
        
                                    <div className="mb-3 bg-white pb-2 pt-3 px-3 index-section">
                                        <p className="h5 font-weight-bold">Progress</p>
                                        <div style={{ height: 10, width: '80%' }} className="progress">
                                            <div className="progress-bar bg-warning " style={{width: width }}></div>
                                        </div>
                                        <h3 className="progress-text pt-2"><p>{course_progress}% Complete</p></h3>
                                    </div>
                                
                                    <div className="index-section px-3">
                                        <p className="h5 font-weight-bold">Content</p>
                                        <p className="text-right pr-2">Course Home</p>
                                        <ChaptersNav data={values.store.chapter}/>
                                    </div>

                                   {/* //////jkhdfkjdfdjf */}
                                   <RenderButton values={values}/>
                                    
                                </div>
                            </div>
                        </div>
                    </section>
                )
            }}
           
        </StoreConsumer>
            
    )
}



export default ChapterList
