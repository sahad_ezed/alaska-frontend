import React from 'react'
import { Redirect, Link } from 'react-router-dom'
import ExamPage from '../Elements/ExamPage'
import { StoreConsumer } from '../../Store/Store'
import Icofont from 'react-icofont'

function ExamScreen() {
    return (
        <StoreConsumer>
            {values => {
                if(!values.store.chapter.exam) return <Redirect to="/chapter"/>
                return (
                    <section id="chapter-section">
                        <div className="container">
                            <div className="row">
        
                                <div className="col-md-9 col-sm-8">
                                    <h3 className="font-weight-bold" >Basics of Strategic Management</h3>
                                    <p className="" style={{ color: '#a2a6a3' }}>8 chapters | 1.5 hours </p>
                                    <h4 className="pb-3">Exam</h4>
                                    
                                    <ExamPage values={values}/>
                                    
                                </div>
        
        
                                <div className="col-md-3 col-sm-4 ">

                                    <div className="index-section px-3 mt-3">
                                        <p className="h5 font-weight-bold"><Icofont icon="arrow-left" /> Back</p>
                                        {/* <p className="text-right pr-2">Course Home</p> */}
                                        <div className="d-flex justify-content-center pt-3">
                                        <Link style={{ textDecoration: 'none', color: '#000'}} to='/my-course'> 
                                            <button className="btn btn-warning px-5"> My Courses </button>
                                        </Link>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </section>
                )
            }}
           
        </StoreConsumer>
    )
}

export default ExamScreen
