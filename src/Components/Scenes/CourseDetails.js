import React, {Fragment, useState, useEffect} from 'react'
import { Redirect, Link } from 'react-router-dom'
import Icofont from 'react-icofont';
import swal from 'sweetalert';
import { StoreConsumer } from '../../Store/Store'
import { COURSE_IMAGE_URL } from '../../Store/Config'
import RelatedCourse from '../Elements/RelatedCourse';


const StarRender = (props) => {
    const { rating } = props
    var activeColor = {color: '#fa8c25', fontSize: 25}
    var disableColor = {color: 'grey', fontSize: 25}

    var totalStars = 5
    var disableCount = totalStars - rating

 
    
    return(
        <div className="row ml-1 mb-2 mt-2">

            {
                Array.from(Array(rating), (e, i) => {
                    return <Icofont key={i} style={activeColor} icon="star"></Icofont>
                }) 
            }

            {
                Array.from(Array(disableCount), (e, i) => {
                    return <Icofont key={i} style={disableColor} icon="star"></Icofont>
                }) 
            }

        </div>
       
    )
}

function CourseDetails() {

    useEffect(() => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
    })

    const [redirect, setRedirect] = useState(false)
    const [redirectCourses, setRedirectCourses] = useState(false)

    const add_cart = async (isUserLogged, add_to_cart, course_id, setLogin_to_page) => {
        if(isUserLogged) {
            await setLogin_to_page('')
            setRedirect(false)
            await add_to_cart(course_id)
        } else {
            setLogin_to_page('/course')
            setRedirect(true)
        }
    }

    const deleteCourseFx = (deleteCourse, course_id) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this course and related datas!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                deleteCourse(course_id)
                setRedirectCourses(true)
            } 
          });
    }


    const EditDeleteButton = (props) => {
        const { is_admin_access, deleteCourse, course_id } = props
        if(is_admin_access) {
            return(
                <StoreConsumer>
                    {values => {
                        const { set_admin_chapter } = values
                        return (
                            <div className="row mt-5">
                                <Link to="/admin-edit-courses"><button className="btn btn-warning mt-5 m-2">Edit</button></Link>
                                <button className="btn btn-danger mt-5 m-2"
                                    onClick={() => {
                                        deleteCourseFx(deleteCourse, course_id)
                                    }} 
                                >Delete</button>
                                <Link to="/admin-chapter" onClick={() => set_admin_chapter(course_id)} ><button className="btn btn-info mt-5 m-2">Chapters</button></Link>
                            </div>
                        )
                    }}
                </StoreConsumer>
            )
        } else return null
        
    }


   

    const RenderPracticalCourse = ({values}) => {
        const { practical_data, isUserLogged } = values.store
        const { add_to_cart, setLogin_to_page } = values
        const { course_id, course_name, price, offer_percentage, details, available_centers } = practical_data

        const [practicalStauts, setPracticalStauts] = useState(false)
        
        if(practical_data.course_id !== undefined) {
            var offer_amount = price - ((offer_percentage / 100) * price) 
                offer_amount = offer_amount.toFixed(2)

        const PracticalDetails = () => {
            if(practicalStauts) {
                return (
                    <div style={{backgroundColor: '#f5ec73', borderRadius: 8, padding: 10 }} >
                        <p className="text-justify">{details}</p>
                        <p><u>Available Center</u></p>
                        <ul style={{ fontSize: 12 }}>
                            {
                                available_centers.map((item, index) => {
                                    const { center_name, place } = item
                                    return (
                                        <li key={index}>
                                            <div className="row pl-3">
                                                <p>{center_name}</p>
                                                <p style={{ fontSize: 10, fontWeight: 'normal', marginTop: 2, marginLeft: 3 }} >({place})</p>
                                            </div>
                                            
                                        </li>
                                    )
                                    
                                })
                            }
                            
                        </ul>
                    </div>
                )
            } else return null
        }
            return (
                    <Fragment>
                        <div style={{ cursor: 'pointer' }} onMouseEnter={() => setPracticalStauts(true)} onMouseLeave={() => setPracticalStauts(false)}>
                            <p style={{ color: 'grey' }}><u>Practical Course</u></p>
                            <h5>{course_name}</h5>
                        </div>
                        

                        <PracticalDetails />

                        <div className="hlf f-lft price">
                            <p className="text-left">${offer_amount}  <s> $ {price}</s> </p>
                        </div>

                        
                       
                        <div className="hlf f-lft cart-btn">
                            <div onClick={() => add_cart(isUserLogged, add_to_cart, course_id, setLogin_to_page)} className="btn cart float-right" to="/">ADD TO CART</div>
                        </div>
                    </Fragment>
            )

        } else return null

    }
    

    if(redirect) {
        return <Redirect to='/login' />
    } else {
        if(redirectCourses) return <Redirect to='/courses' />
    return (
        <StoreConsumer>
            {values => {    
                    if(values.store.temp_product.course_id === undefined) {
                        return (
                            <Redirect to='/courses' />
                        )
                    } else {
                     
                    const { course_id, course_name, skills, about, introduction, price, course_image, rating, type, duration, award, language, level, offer_percentage, course_offered } = values.store.temp_product
                    const { isUserLogged, cart_alert, cart_alert_content, is_admin_access } = values.store
                    const { add_to_cart, resetCart_alert, setLogin_to_page, deleteCourse } = values //action functions
     
                    var offer_amount = price - ((offer_percentage / 100) * price) 
                    offer_amount = offer_amount.toFixed(2)
                    var skillsData = JSON.parse(skills)
                    var aboutData = JSON.parse(about)

                    var image = COURSE_IMAGE_URL + course_image
     
                    if(cart_alert) {
                        swal(cart_alert_content).then((status) => {
                            if(status) {
                                resetCart_alert()
                            }
                        })
                    }

                    const CourseOffered = () => {
                        if(course_offered) {
                            return (
                                <p style={{ color: '#000', fontSize: 20, marginBottom: 10, fontWeight: '700' }}><span style={{ fontSize: 18, paddingRight: 5 }}>Course Offered:</span> {course_offered} </p>
                            )
                                
                            } else return ( <p style={{ color: '#000', fontSize: 20, marginBottom: 10, fontWeight: '700' }}><span style={{ fontSize: 18, paddingRight: 5 }}>Course Offered:</span> Alaska </p>  )
                        
                    }
                   
                return (
                    <Fragment>
                        <div className="container-fluid inner">
                            <div className="container">
                                <div className="row">
                                    <div className="spc50"></div>
                                    <div className="col-md-12">
                                        <div className="m-hd">
                                            <h2>{course_name}</h2>
                                            <span></span>
                                        <h1>{introduction}</h1>
                                        </div>
                                        <div className="spc50"></div>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="c-inner">
                                            <div className="c-dlt">
                                                <CourseOffered />
                                                <StarRender rating={rating} />
                                                
                                                <h1>Skills you will learn</h1>
                                                <ul style={{ listStyleType: 'none' }}>
                                                    {
                                                    skillsData.map((item, index) => {
                                                        const { content } = item
                                                        return (
                                                                <li key={index}><Icofont icon="tick-mark"></Icofont> {content} </li>
                                                        )
                                                    
                                                    })
                                                    }
                                                
                                                </ul>
                                                <h1>About this course</h1>

                                                {
                                                    aboutData.map((item, index) => {
                                                        const { content } = item
                                                        return (
                                                            <p key={index}>{content}</p>
                                                        )
                                                    })
                                                }


                                                <EditDeleteButton is_admin_access={is_admin_access} deleteCourse={deleteCourse} course_id={course_id}/>
                                                
                                            </div>
                                        </div>
                                    </div>


                                    <div style={{ marginBottom: 150 }} className="col-md-4 side">
                                        <div className="c-box">
                                            <div className="c-img">
                                                <img src={image} alt="product" />
                                            </div>
                                            <div className="c-dlt">
                                                <div className="hlf f-lft cart-btn">
                                                    <div onClick={() => add_cart(isUserLogged, add_to_cart, course_id, setLogin_to_page)} className="btn cart" to="/">ADD TO CART</div>
                                                </div>
                                                <div className="hlf f-lft price">
                                                    <s>${price}</s>
                                                    <p>${offer_amount}</p>
                                                </div>
                                                <div className="cls"></div>
                                                <div className="line1"></div>
                                                <p><Icofont icon="computer"></Icofont> Type: 	<span>{type}</span></p>
                                                <p><Icofont icon="clock-time"></Icofont> Duration:<span>{duration} (self-paced)</span></p>
                                                <p><Icofont icon="award"></Icofont> Award:	<span>{award}</span></p>
                                                <p><Icofont icon="world"></Icofont> Language:<span>{language}</span></p>
                                                <p><Icofont icon="chart-bar-graph"></Icofont>Level: <span>{level}</span></p>
                                                <p><Icofont icon="brand-myspace"></Icofont> Access: <span>Open to everyone</span> </p>
                                                <div className="line1"></div>

                                                <RenderPracticalCourse values={values} course_id={course_id }/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div style={{ marginTop: 20 }} className="fxd-bg inner">
                            <div className="container-fluid">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="ftrs">
                                                    <img src={require('../Assets/images/icon-skill.png')} alt="product" />
                                                <h2>Learn New Skills</h2>
                                                <div className="cls"></div>
                                            </div> 
                                            <div className="ftrs">
                                                    <img src={require('../Assets/images/icon-online.png')} alt="icon"/>
                                                <h2>100% Online</h2>
                                                <div className="cls"></div>
                                            </div> 
                                            <div className="ftrs">
                                                    <img src={require('../Assets/images/icon-certificated.png')} alt="icon"/>
                                                <h2>Get Certificated</h2>
                                                <div className="cls"></div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="m-img">
                                                <img src={require('../Assets/images/certificate.png')} alt="icon"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="container-fluid blue-bg p100">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="blu-btn">
                                        <Link style={{ textDecoration: 'none'}} className="btn" to="/courses">View all Courses</Link> 
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="crs-dtls">
                                            <h1>Save {offer_percentage} % on this course now:</h1>
                                            <div className="line2"></div>
                                            <p><s>{price}</s> {offer_amount}</p>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="orng-btn">
                                        <span className="btn" onClick={() => add_cart(isUserLogged, add_to_cart, course_id, setLogin_to_page)}>ADD TO CART </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <RelatedCourse />
                        </Fragment>
                )
             }
            }}
        </StoreConsumer>
        
    )
    }
}

export default CourseDetails
