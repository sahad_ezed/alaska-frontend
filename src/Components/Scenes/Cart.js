import React, {Fragment, useState} from 'react'
import { Redirect, Link } from 'react-router-dom'
import { StoreConsumer } from '../../Store/Store'
import { COURSE_IMAGE_URL } from '../../Store/Config'
import PaypalButton from '../Elements/PaypalButton'

function Cart() {

   
    // const client = {
    //     sandbox:    'YOUR-SANDBOX-APP-ID',
    //     production: 'YOUR-PRODUCTION-APP-ID',
    // }
    const [redirect, setRedirect] = useState(false)
    if(redirect) return <Redirect to='/courses' />
    return (
        <StoreConsumer>
            {values => {
                const { cart, isUserLogged, cart_total } = values.store
                const { deleteCart, setLogin_to_page } = values
                
                if (!isUserLogged) {
                    setLogin_to_page('/cart')
                    return <Redirect to="/login" />
                } 
                
                if(cart.length === 0) {
                    return (
                        <div className="container mt-5">
                            <h2>Cart is empty</h2>
                        </div>
                    )
                } else {
                return (
                    <Fragment>
                        <div className="container-fluid cart">
                            <div className="container">
                                <div className="row">
                                    <div className="spc50"></div>
                                    <div className="col-md-12">
                                        <div className="m-hd">
                                            <h2> CHECKOUT DETAILS    </h2>
                                            <span></span> 
                                        </div>
                                        <div className="spc50"></div>
                                    </div> 
                                    <div className="col-md-12 side">
                                        <div className="cart-box">
                                            <table width="100%" style={{ cellPadding: 0, cellSpacing: 0 }} border="">
                                                <thead>
                                                    <tr>
                                                        <th className="m-img">Images</th>
                                                        <th>Course</th>
                                                        <th>Price</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>

                                                {
                                                    cart.map(item => {
                                                        const { cart_id } = item
                                                        const { course_name, course_image, price, course_id, offer_percentage} = item.course_data

                                                        var offer_amount = price - ((offer_percentage / 100) * price) 
                                                        var image = COURSE_IMAGE_URL + course_image
                                                        return (
                                                            <tr style={{ height: 75 }} key={cart_id}>
                                                                <td width="10%" height="100" className="p-3">
                                                                        <img height="60" src={image} alt="product" />
                                                                </td>
                                                                <td width="60%" className="c-dlt">									
                                                                    <h2>{course_name}</h2> 
                                                                </td>
                                                                <td width="20%" className="price">
                                                                    <h3>{offer_amount.toFixed(2)}</h3>  
                                                                </td>
                                                                <td width="10%" className="delet"> 
                                                                    <p><i onClick={() => deleteCart(course_id)} className="icofont-ui-delete"></i></p> 
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }

                                                </tbody>
                                             
                                            </table>

                                            <table width="100%" style={{ cellPadding: 0, cellSpacing: 0 }} border="" className="total"> 
                                            <tbody>
                                                <tr> 
                                                    <td width="50%"></td>
                                                    <td width="30%" className="price" align="right">									
                                                        <h3>Total</h3> 
                                                    </td>
                                                    <td width="20%" className="price" align="right">
                                                        <h3>{cart_total}</h3>  
                                                    </td>   
                                                </tr>
                                            </tbody>
                                            </table>

                                            <Link style={{ textDecoration: 'none' }} to="/courses" className="btn btn-blue mt-4"> ADD MORE COURSES</Link>
                                            <button className="btn float-md-right ml-3 mt-4"> 
                                                CHECKOUT
                                                <PaypalButton redirect={setRedirect}/>
                                            </button>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Fragment>
                )
                }
            }}
        </StoreConsumer>
        
    )
}

export default Cart
