import React from 'react'

function PageNotFound() {
    return (
        <div className="container mt-5" >
            <h1 className="my-3 font-weight-bolder" ><span className="text-danger" >404</span> Page Not Found</h1>
        </div>
    )
}

export default PageNotFound
