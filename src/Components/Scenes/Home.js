import React,{ Fragment } from 'react'
import Banner from '../Elements/Banner'
import CourseListSection from '../Elements/CourseListSection'
import Protfolio1 from '../Elements/Protfolio1'

function Home() {

    window.scrollTo({
        top: 0,
        behavior: "smooth"
      });
      
    return (

       <Fragment>
            <Banner />

            <CourseListSection />

            <div className="container-fluid carea gry-bg">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="content">
                                <h1>About Alaska</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="m-img">
                                <img src={require('../Assets/images/certificate.png')} alt="certificate" /> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid testimonial carea blue-bg">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h2>What our <br/>students have to say</h2>
                            <div className="row mx-auto my-auto">
                                <div id="myCarousel" className="carousel slide w-100" data-ride="carousel">
                                    <div className="carousel-inner w-100" role="listbox">
                                        <div className="carousel-item active">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" />
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" /> 
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" />
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" />
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" />
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <div className="col-lg-4 col-md-6">
                                                <div className="testi">
                                                    <div className="auther">
                                                        <div className="p-img">
                                                            <img src={require('../Assets/images/auther.jpg')} alt="author" />
                                                        </div>
                                                        <div className="p-dtl">
                                                            <h1>Divya Dutta</h1>
                                                            <p>MBA Student</p>
                                                        </div>
                                                        <div className="cls"></div>
                                                    </div>
                                                    <div className="comments">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <Protfolio1 />
    
       </Fragment>
    )
}

export default Home
