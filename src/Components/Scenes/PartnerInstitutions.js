import React, {Fragment, useState} from 'react'
import { Spinner } from 'react-bootstrap'
import { StoreConsumer } from '../../Store/Store'

function PartnerInstitutions() {
    return (
        <StoreConsumer>
            {values => {

                const { institutionContactForm, reg_btn_state } = values
                const { register_btn_state } = values.store

                const RenderForms = () => {
                    const [email, setEmail] = useState('')
                    const [name, setName] = useState('')
                    const [content1, setContent1] = useState('')
                    const [content2, setContent2] = useState('')

                    const sendButton = () => {

                        if(email) {
                            if(name) {
                                if(content1) {
                                    if(content2) {
                                        reg_btn_state('loading')

                                    var data = {
                                        name,
                                        email,
                                        content1,
                                        content2
                                    }
                                    institutionContactForm(data)
                                    } else {
                                        alert('Content field is empty')
                                    }
                                } else {
                                    alert('Content field is empty')
                                }
                            } else {
                                alert('Name field is empty')
                            }
                        }else {
                            alert('Email field is empty')
                        }
                    }

                    const RenderSendButton = () => {
                        if(register_btn_state === 'loading') {
                            return (
                                <div className="text-center text-md-center my-5">
                                    <button disabled className="btn btn-primary" >
                                        <Spinner animation="border" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </Spinner>
                                    </button>
                                </div>
                            )
                        } else {
                            return (
                                <div className="text-center text-md-center my-5">
                                    <button className="btn btn-primary" onClick={() => sendButton()} >Send Message</button>
                                </div>
                            )
                        }
                    }

                    return (
                            <div className="col-md-12 text-left ">
                                <form>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="md-form mb-0">
                                            <label htmlFor="email" className="">Your email</label>
                                                <input type="text" id="email" name="email" className="form-control"
                                                    value={email}
                                                    onChange={(txt) => setEmail(txt.target.value)}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="md-form mb-0">
                                            <label htmlFor="name" className="">Institution name</label>
                                                <input type="text" id="inname" name="inname" className="form-control"
                                                    value={name}
                                                    onChange={(txt) => setName(txt.target.value)}
                                                />
                                            </div>  
                                        </div>
                                    </div> 
                                    <div className="row mt-3">
                                            <div className="col-md-6">
                                                <div className="md-form">
                                                <label htmlFor="message">Which are the courses wish to partner with alaska?</label>
                                                    <textarea type="text" id="message" name="message" rows="5" className="form-control md-textarea" 
                                                        value={content1}
                                                        onChange={(txt) => setContent1(txt.target.value)}
                                                    />
                                                </div>
                                            </div>

                                            <div className="col-md-6">
                                                <div className="md-form">
                                                <label htmlFor="message">List your available courses</label>
                                                    <textarea type="text" id="message" name="message" rows="5" className="form-control md-textarea" 
                                                        value={content2}
                                                        onChange={(txt) => setContent2(txt.target.value)}
                                                    />
                                                </div>
                                            </div>
                                    </div>
                                    
                                </form>
                                
                                <RenderSendButton />

                            </div>
                    )
                }

                return (
                    <Fragment>
                        <h2 className="h1-responsive font-weight-bold text-center my-4">Partner with Alaska</h2>
                        <div className="container">
                        <div className="row ">
                            <RenderForms />
                        </div>
                        </div>
                    </Fragment>
                )
            }}
        </StoreConsumer>
        
    )
}

export default PartnerInstitutions
