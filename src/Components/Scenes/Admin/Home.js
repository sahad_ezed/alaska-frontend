import React from 'react'
import { StoreConsumer } from '../../../Store/Store'
import { Redirect } from 'react-router-dom'
import Home from '../Home'

function AdminHome() {
    return (
        <StoreConsumer>
            {values => {
                const { is_admin_access } = values.store
                if(!is_admin_access) {
                    return (
                        <Redirect to='/adminLogin' />
                    )
                }

                return (
                   <Home />
                )
            }}
        </StoreConsumer>
        
    )
}

export default AdminHome
