import React, {useState} from 'react'
import Icofont from 'react-icofont';
import { StoreConsumer } from '../../../Store/Store'


function Category() {

    const [editStatus, setEditStatus] = useState(0)
    const [categoryName, setCategoryName] = useState('')
    const [catname, setCatname] = useState('')

    const EditButton = (category_id, category_name) => {
        setCategoryName(category_name)
        setEditStatus(category_id)
    }


    const OKButton = (updateCategory) => {
        updateCategory(editStatus, categoryName)
        setEditStatus(0)
        setEditStatus('')
    }

    const RemoveButton = (deleteCategory) => {
        deleteCategory(editStatus)
        setEditStatus(0)
        setEditStatus('')
    }

    const CancelButton = () => {
        setEditStatus(0)
        setEditStatus('')
    }

    const addCategoryFx = (addCategory) => {
        addCategory(catname)
        setCatname('')
    }

    return (
        <StoreConsumer>
            {values => {
                const { category_list } = values.store
                const { addCategory, deleteCategory, updateCategory } = values
               return (
                <div className="container mt-5 mb-5">
                    <div className="row">
                        <div className="col-md-7">
                        <h4>Category List</h4>
                        <div className="cart-box">
                            <table width="100%" style={{ cellPadding: 0, cellSpacing: 0 }} border="">
                                <thead>
                                    <tr style={{ height: 50 }}>
                                        <th className="px-4">Sl No</th>
                                        <th className="px-4">Course Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        category_list.map((items, index) => {
                                            const { category_id, category_name } = items

                                            if(editStatus === category_id) {
                                                return (
                                                    <tr key={category_id} style={{ height: 80 }} >
                                                        <td className="px-4">{index + 1 }</td>
                                                        <td className="px-2">
                                                            <div className="row d-flex justify-content-center align-items-center">
                                                                <div className="col-md-8">
                                                                    <input type="text" className="form-control" value={categoryName} onChange={(text) => setCategoryName(text.target.value)} />
                                                                </div>
                                                                <div className="col-md-4">
                                                                    <div className="row d-flex justify-content-center align-items-center">
                                                                        <button className="btn btn-sm float-right btn-primary mr-1" onClick={() => OKButton(updateCategory)}>OK</button> 
                                                                        <button className="btn btn-sm float-right btn-danger mr-1" onClick={() => RemoveButton(deleteCategory)}>Remove</button> 
                                                                        <Icofont icon="close" className="btn" onClick={() => CancelButton()} />
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            } else {

                                                return (
                                                    <tr key={category_id} style={{ height: 50 }}>   
                                                        <td className="px-4">{index + 1 }</td>
                                                        <td className="px-4">{category_name}
                                                            <button className="btn btn-sm float-right" onClick={() => EditButton(category_id, category_name)}>Edit</button> 
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                           
                                        })
                                    }

                
                                </tbody>
                            </table>
                        </div>
                        </div>

                        <div className="col-md-5">
                            <h4>Add Category</h4>

                            <div className="row register-box ml-2"> 	
                                    <label>Category:</label>
                                    <input type="text" name="category" value={catname} onChange={(text) => setCatname(text.target.value)} />
                                    {/* { error ?  <p className="mt-2 text-danger">{error}</p> : <p></p> } */}

                                    <button onClick={() => addCategoryFx(addCategory)}>Add</button>
                            </div>
                        </div>
                    </div>
                </div>
               )

            }}
        </StoreConsumer>
       
    )
}

export default Category
