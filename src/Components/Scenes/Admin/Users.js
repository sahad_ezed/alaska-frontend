import React, {Fragment} from 'react'
import { StoreConsumer } from '../../../Store/Store'
import UserModal from '../../Elements/UserModal'


function Users() {


    const TableContents = ({values}) => {
    
        const { user_datas } = values.store
        const { model_chapter_open, temp_user_data } = values

        return (
            <tbody>

                {
                    user_datas.map(item => {
                        const { user_id, user_name, user_email, orders} = item

                        var statusColor = 'yellow'
                        if(orders) {
                            if(orders.length > 0) {
                                statusColor = 'green'
                            } else {
                                statusColor = 'yellow'
                            }
                        }
                        return (
                            <tr className="user-table" key={user_id} onClick={() => {temp_user_data(item); model_chapter_open()}}>
                                <td>{user_name}</td>
                                <td>{user_email}</td>
                                <td>
                                    <div style={{ height: 20, width: 20, backgroundColor: statusColor, borderRadius: 10, marginLeft: 10 }} />
                                </td>
                            </tr>
                        )
                    })
                }
                
            </tbody>
        )
    }

    return (
        <StoreConsumer>
            {values => {
                return (
                    <Fragment>
                        <div style={{ backgroundColor: '#e6e8ed' }} className="col-md-6 mt-5 mx-5 p-5 ">
                        <h4 className="my-2 ml-3">Users</h4>
                        
                            <div className="col-md-6">
                                <input type="text" className="form-control" placeholder="Search" />
                            </div>
                        
                        <div className="bg-white rounded mt-3 ml-2 p-2">
                            <table className="table border">

                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <TableContents values={values}/>

                            </table>
                        </div>
                        </div>

                        <UserModal />
                    </Fragment>
                )
            }}
        </StoreConsumer>
       
    )
}

export default Users
