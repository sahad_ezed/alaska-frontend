import React, { Fragment, useState } from 'react'
import {Link, Redirect} from 'react-router-dom'
import Icofont from 'react-icofont'
import { StoreConsumer } from '../../../../Store/Store'

function AddPractical() {

    const RenderForms = ({values}) => {
        
        const { temp_product, practical_data } = values.store
        const { add_practical_course, update_practical_course } = values
        const { category_id, course_id} = temp_product

        const { available_centers} = practical_data
        var available_temp = {
            center_name: '',
            place: ''
        }

        if(available_centers) {
            available_temp = available_centers
        }
      
        


        const [courseName, setCourseName] = useState(practical_data.course_name)
        const [details, setDetails] = useState(practical_data.details)
        const [price, setPrice] = useState(practical_data.price)
        const [offer, setOffer] = useState(practical_data.offer_percentage)
        const [centerName, setCenterName] = useState(available_temp.center_name)
        const [centerPlace, setCenterPlace] = useState(available_temp.place)
        const [redirect, setRedirect] = useState(false)

        var pr_course_id = practical_data.course_id
        
        if(category_id === undefined) return <Redirect to='/courses' />
        if(redirect) return <Redirect to='/admin-chapter' />

        const ButtonRender = () => {
            if(practical_data.course_name) {
                return (
                    <button className="btn btn-lg btn-warning float-right" onClick={() => update_fx()} >Update</button>
                )
            } else {
                return (
                    <button className="btn btn-lg btn-warning float-right" onClick={() => add_practical_fx()} >Add Practical</button>
                )
            }
        }

        const update_fx = () => {
            if(courseName) {
                if(details) {
                    if(price) {
                        if(offer) {
                            if(centerName) {
                                if(centerPlace) {
                                    var centers = {
                                        center_name: centerName,
                                        place: centerPlace
                                    }
                                    var data = {
                                        course_name: courseName,
                                        course_id: pr_course_id,
                                        price: price,
                                        offer_percentage: offer,
                                        details: details,
                                        available_centers: centers
                                    }
    
                                    update_practical_course(data)
                                    setRedirect(true)
                                } else {
                                    alert('Center Place field is empty')
                                }
                            }else {
                                alert('Center Name field is empty')
                            }
                        } else {
                            alert('Offer Percentage field is empty')
                        }
                    } else {
                        alert('Price field is empty')
                    }
                } else {
                    alert('Details field is empty')
                }
            } else {
                alert('Course Name field is empty')
            }
        }

        const add_practical_fx = () => {
            if(courseName) {
                if(details) {
                    if(price) {
                        if(offer) {
                            if(centerName) {
                                if(centerPlace) {
                                    var centers = {
                                        center_name: centerName,
                                        place: centerPlace
                                    }
                                    var data = {
                                        course_name: courseName,
                                        category_id,
                                        course_id,
                                        price: price,
                                        offer_percentage: offer,
                                        details: details,
                                        available_centers: centers
                                    }
    
                                    add_practical_course(data)
                                    setRedirect(true)
                                } else {
                                    alert('Center Place field is empty')
                                }
                            }else {
                                alert('Center Name field is empty')
                            }
                        } else {
                            alert('Offer Percentage field is empty')
                        }
                    } else {
                        alert('Price field is empty')
                    }
                } else {
                    alert('Details field is empty')
                }
            } else {
                alert('Course Name field is empty')
            }
        }

       

        return (
            <Fragment>
            <label className="form-check-label mt-4 pl-2 font-weight-bold">Course Name</label>
            <input className="form-control" placeholder='Course Name' type="text"
                defaultValue={courseName}
                onChange={(txt) => setCourseName(txt.target.value)}
            />

            <label className="form-check-label mt-4 pl-2 font-weight-bold">Details</label>
            <textarea className="form-control" placeholder='Details' type="text"
                defaultValue={details}
                onChange={(txt) => setDetails(txt.target.value)}
            />

            <div className="row">
            
                <div className="col-md-4">
                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Center Name</label>
                    <input className="form-control" placeholder='Center Name' type="text"
                        defaultValue={centerName}
                        onChange={(txt) => setCenterName(txt.target.value)}
                    />

                </div>

                <div className="col-md-4">
                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Place</label>
                    <input className="form-control" placeholder='Place of center' type="text"
                        defaultValue={centerPlace}
                        onChange={(txt) => setCenterPlace(txt.target.value)}
                    />

                </div>
               <div className="col-md-4" />
                <div className="col-md-4">
                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Price</label>
                    <input className="form-control" placeholder='Price' type="number"
                        defaultValue={price}
                        onChange={(txt) => setPrice(txt.target.value)}
                    />

                </div>

                <div className="col-md-4">
                    
                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Offer Percentage</label>
                    <input className="form-control" placeholder='Offer perccentage' type="number"
                        defaultValue={offer}
                        onChange={(txt) => setOffer(txt.target.value)}
                    />

                </div>

            </div>
            <ButtonRender />
            </Fragment>
        )
    }

    return (
        <StoreConsumer>
            {values => {
                return (
                    <section id="chapter-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-9">
                                <h3 className="font-weight-bold" >Practical</h3>
                                <h4 className="pb-3">Course name</h4>

                                    <RenderForms values={values}/>
                                    
                                    
                                    
                            </div>

                            <div className="col-md-3">
                                <div className="index-section px-3">
                                    <Link style={{ textDecoration: 'none', color: '#000'}} to="/admin-chapter">
                                        <p className="h5 font-weight-bold"> <Icofont icon="arrow-left" /> Back</p>
                                    </Link>
                                    <p className="text-right pr-2">Details</p>

                                    
                                </div>
                            </div>    
                        </div>            
                    </div>
                    </section>
                )
            }}
        </StoreConsumer>
        
    )
}

export default AddPractical
