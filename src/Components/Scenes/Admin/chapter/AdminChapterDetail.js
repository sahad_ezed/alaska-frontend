import React, {Fragment} from 'react'
import { StoreConsumer } from '../../../../Store/Store'
import { Link, Redirect } from 'react-router-dom'
import {CHAPTER_IMAGE_URL} from '../../../../Store/Config'
import Icofont from 'react-icofont'

function AdminChapterDetail() {

    window.scrollTo({
        top: 0,
        behavior: "smooth"
      });

    return (
        <StoreConsumer>
            {values => {
                // const { temp_chapter } = values.store
                const TypeA = (props) => {
                    const { content, image } = props.data
                    var realImg = CHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            <div className="col-md-3 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                                <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
                        </div>
                    )
                }
                
                const TypeB = (props) => {
                    const { content, image } = props.data
                    var realImg = CHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            
                            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
                
                            <div className="col-md-3 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                
                        </div>
                    )
                }
                
                const TypeC = (props) => {
                    const { content } = props.data
                    return (
                        <div className="row py-3">
                            
                            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-12"></div>
                
                        </div>
                    )
                }
                
                const TypeD = (props) => {
                    const { image } = props.data
                    var realImg = CHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            
                            <div className="col-md-12 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                        </div>
                    )
                }


                const MainDataLayout = ({values}) => {
    
                    const { temp_chapter } = values.store
                    const { chapter_content } = temp_chapter
                    // data={values.store.temp_chapter} store={values.store} readStatus={set_read_chapter}
                    if(chapter_content) {
                        return (
                            <Fragment>
                            {
                                chapter_content.map((item, index) => {
                                    const { type} = item
                                    switch(type) {
                                        case 1 : return <TypeA key={index} data={item} />
                                        case 2 : return <TypeB key={index} data={item} />
                                        case 3 : return <TypeC key={index} data={item} />
                                        case 4 : return <TypeD key={index} data={item} />
                                        default: return null
                                    }
                                })
                            }
                
                    
                            </Fragment>
                        )
                    } else return null
                }

                const { chapter_title, chapter_no } = values.store.temp_chapter
                const { modify_temp_data, delete_chapter, status_edit_chapter, status_add_subchapter } = values
                const { temp_chapter, delete_chapter_status } = values.store
                const { chapter_content, chapter_id } = temp_chapter

                
                const RenderSubchapterNav = () => {
                    return (
                        <StoreConsumer>
                            {values => {
                                const { subchapter, chapter_id } = values.store.temp_chapter
                                const { set_temp_subchapter } = values
                                if(chapter_id === undefined) return <Redirect to="/admin-chapter" />
                                if(subchapter) {
                                    return (
                                        <div style={{ backgroundColor: '#ebeae8' }} className="index-section px-3 mt-3">
                                            <p className="h5 font-weight-bold">Subchapters</p>
                                            
                                            {
                                                subchapter.map(item => {
                                                    const { sub_chapter_title, sub_chapter_id } = item
                                                    return (
                                                        <Link key={sub_chapter_id} to="/admin-detailed-subchapter" style={{ textDecoration: 'none', color: '#000'}} onClick={() => set_temp_subchapter(item)}>
                                                            <div  className="mt-3 chapter-nav-box d-flex align-items-center px-2">
                                                                <div className="row mt-3">
                                                                    <Icofont style={{ color: 'green'}} className="ml-3 mr-3" icon="check-circled" /> <p>{sub_chapter_title}</p>
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    )
                                                })
                                            }

                                            
    
                                            
                                            
                                        </div>
                                    )
                                } else return null
                                
                            }}
                        </StoreConsumer>
                        
                    )
                }

                

                if(delete_chapter_status) return <Redirect to='/admin-chapter' />

                return (
                    
                        <div className="row m-5">
                            <div className="col-md-9 add-chapter-layout">
                                <div className="px-3 py-3 mt-3">
                                    <h4 className="font-weight-bold text-center" >{chapter_title}</h4>
                                    <p className="mb-4 text-center">(Chapter No: {chapter_no})</p>
                                    <MainDataLayout values={values}/>
                                </div>
                            </div>

                            <div className="col-md-3 col-sm-4">

                                <div style={{ backgroundColor: '#ebeae8' }} className="index-section px-3">
                                    <Link style={{ textDecoration: 'none', color: '#000'}} to="/admin-chapter">
                                        <p className="h5 font-weight-bold"><Icofont icon="arrow-left" /> Back</p>
                                    </Link>
                                    <Link style={{ textDecoration: 'none', color: '#fff'}} to="/admin-edit-chapter">
                                        <button className="btn btn-info ml-2 mt-2" onClick={() => {modify_temp_data(chapter_content); status_edit_chapter(false)}}><Icofont icon="plus-circle" className="mr-3"/>Edit Chapter</button>
                                        </Link>
                                    <Link style={{ textDecoration: 'none', color: '#fff'}} to="/admin-add-subchapter">
                                        <button className="btn btn-warning ml-2 mt-2" onClick={() => {modify_temp_data([]); status_add_subchapter(false)}}><Icofont icon="plus-circle" className="mr-3"/>Add Sub Chapter</button>
                                    </Link>
                                    
                                    <button className="btn btn-danger ml-2 mt-2" onClick={() => {delete_chapter(chapter_id);}}><Icofont icon="ui-delete" className="mr-3"/>Delete</button>
                                   
                                    {/* <div className="mt-3 d-flex justify-content-center align-items-center" style={{ padding: 5, backgroundColor: '#fff', borderRadius: 5, cursor: 'alias'}}>
                                        <h5 className="mt-2">Chapter Title</h5>
                                    </div> */}
                                    
                                </div>

                               <RenderSubchapterNav />

                            </div>


                            
                        </div>
                        
                )
            }}
        </StoreConsumer>
        
    )
}

export default AdminChapterDetail
