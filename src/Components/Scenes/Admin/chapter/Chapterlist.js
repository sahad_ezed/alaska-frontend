import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import Icofont from 'react-icofont'
import { StoreConsumer } from '../../../../Store/Store'

function Chapterlist() {

    window.scrollTo({
        top: 0,
        behavior: "smooth"
      });

    const ChaptersList = ({values}) => {
        const { chapter } = values.store
        const { set_temp_chapter, delete_chapter_status, delete_subchapter_status } = values

        if(chapter) {
            if(chapter.length > 0) {
                return (
                    chapter.map((item) => {
                        const {chapter_id, chapter_no, chapter_title, is_subchapters, subchapter } = item
                        return (
                            <Link key={chapter_id} style={{ textDecoration: 'none'}} onClick={() => {set_temp_chapter(item); delete_chapter_status(); delete_subchapter_status();}} to="/admin-detailed-chapter">
                                <div style={{ background: '#e8e8e8' }} className="chapter-box-content">
                                <p className="box-text mt-2"><Icofont style={{ color: 'green', paddingRight: 10 }} icon='check-circled' /> {chapter_no}. {chapter_title}</p>
                                    <Topics is_subchapters={is_subchapters} subchapters={subchapter}/>
                                </div>
                            </Link>
                        )
                    })
                )
            } else return <h5>No chapters here</h5>
        } else return <h5>No chapters here</h5>
        
    }

    const Topics = (props) => {
        const {is_subchapters, subchapters} = props
        if(is_subchapters) {
                
            var count = subchapters.length
            return (
                <p className="px-5 sub-text">{count} Topics</p>
            )
        } else return null
    }


    return (
        <StoreConsumer>
            {values => {
                 console.log(values.store)
                if(values.store.temp_product.course_id === undefined) {
                    return (
                        <Redirect to='/courses' />
                    )
                }
                const { course_name, course_id } = values.store.temp_product
                const { modify_temp_data, status_add_chapter, exam_data } = values
                return (
                    <section id="chapter-section">
                    <div className="container">
                        <div className="row">
    
                            <div className="col-md-9 col-sm-8">
                                <h3 className="font-weight-bold" >{course_name}</h3>
                                <h4 className="pb-3">Chapters</h4>

                                <ChaptersList values={values}/>
                                
                            </div>
    
    
                            <div className="col-md-3 col-sm-4 ">
    
                                {/* <div className="mb-3 bg-white pb-2 pt-3 px-3 index-section d-flex justify-content-center">
                                    <Link to='/admin-add-chapter' onClick={() => modify_temp_data([])} style={{ textDecoration: 'none', color: '#fff'}}><button className="btn btn-info px-3">Add Chapter</button></Link>
                                    <button className="btn btn-warning px-3">Exam</button>
                                </div> */}

                                <div className="index-section px-3">
                                    <Link style={{ textDecoration: 'none', color: '#000'}} to="/course">
                                        <p className="h5 font-weight-bold"> <Icofont icon="arrow-left" /> Back</p>
                                    </Link>
                                    <p className="text-right pr-2">Details</p>

                                    <Link to='/admin-add-chapter' onClick={() => {modify_temp_data([]); status_add_chapter()}} style={{ textDecoration: 'none', color: '#fff'}}>
                                        <button className="btn btn-info px-3 ml-2 mt-2">Add Chapter</button>
                                    </Link>
                                    
                                    <Link to='/admin-exam' style={{ textDecoration: 'none', color: '#000'}} onClick={() => exam_data(course_id)}>
                                        <button className="btn btn-warning px-3 ml-2 mt-2">Exam</button>
                                    </Link>

                                    <Link to='/admin-practical' style={{ textDecoration: 'none', color: '#000'}}>
                                        <button className="btn btn-primary px-3 ml-2 mt-2">Practical</button>
                                    </Link>
                                </div>
                            
                               

                            </div>
                        </div>
                    </div>
                </section>
                )
            }}
        </StoreConsumer>
        
    )
}


export default Chapterlist
