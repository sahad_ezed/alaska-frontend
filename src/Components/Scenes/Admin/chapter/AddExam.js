import React, { Fragment } from 'react'
import {Link, Redirect} from 'react-router-dom'
import Icofont from 'react-icofont'
import QuestionModal from '../../../Elements/Chapter/QuestionAddModal'
import { StoreConsumer } from '../../../../Store/Store'

function AddExam() {
    return (
        <StoreConsumer>
            {values => {
                const { model_chapter_open, temp_exam, delete_exam } = values
                const { exam_data, temp_product } = values.store

                const RenderAlert = () => {
                    if(exam_data.length > 0) {
                        return null
                    } else {
                        return (
                            <h5>No questions here..!</h5>
                        )
                    }
                    
                }

                if(temp_product.course_id === undefined) return <Redirect to="/course" />

                return (
                    <Fragment>
                        <div className="container">
                            <section id="chapter-section">
                                <div className="container">
                                    <div className="row">

                                        <div className="col-md-9 col-sm-8">
                                            <h3 className="font-weight-bold" >Exam</h3>
                                            {/* <h4 className="pb-3">Chapters</h4> */}
                                            <div className="my-2">
                                                <button className="btn btn-info" onClick={() => {model_chapter_open(); temp_exam({})}} >Add Question</button>
                                            </div>

                                            {
                                                exam_data.map((item, index) => {
                                                    const { question_id, question_title, options } =item
                                                    return (
                                                        <div key={question_id} className="col-md-11 exam-layout p-3 my-4">
                                                            <h4>#Question {index + 1}</h4>
                                                            
                                                            <h5 className="px-4 my-3 text-justify">{question_title}</h5>
                                                            {
                                                                options.map((itemz, index) => {
                                                                    const { is_true, option_title } = itemz
                                                                    const RenderTrue = () => {
                                                                        if(is_true) {
                                                                            return (
                                                                                <span style={{ color: 'green'}} className="ml-3" >( True )</span>
                                                                            )
                                                                        } else return null
                                                                        
                                                                    }

                                                                    return (
                                                                        <p key={index} className="px-4 my-3">{option_title} <RenderTrue /> </p>
                                                                    )
                                                                })
                                                            }

                                                            <button className="btn btn-warning ml-3" onClick={() => {model_chapter_open(); temp_exam(item)}} >Edit</button>
                                                            <button className="btn btn-danger ml-2" onClick={() => delete_exam(question_id)}>Delete</button>
                                                        </div>
                                                    )
                                                })
                                            }

                                            <RenderAlert />
                                        
                                            
                                        </div>


                                        <div className="col-md-3 col-sm-4 ">
                                            <div className="index-section px-3">
                                                <Link style={{ textDecoration: 'none', color: '#000'}} to="/admin-chapter">
                                                    <p className="h5 font-weight-bold"> <Icofont icon="arrow-left" /> Back</p>
                                                </Link>
                                                <p className="text-right pr-2">chapter</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>

                        <QuestionModal />

                    </Fragment>
                )
            }}
        </StoreConsumer>
        
    )
}

export default AddExam
