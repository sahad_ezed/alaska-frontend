import React, {Fragment} from 'react'
import { StoreConsumer } from '../../../../Store/Store'
import { Link, Redirect } from 'react-router-dom'
import {SUBCHAPTER_IMAGE_URL} from '../../../../Store/Config'
import Icofont from 'react-icofont'

function AdminSubChapterDetail() {

    window.scrollTo({
        top: 0,
        behavior: "smooth"
      });
   
    return (
        <StoreConsumer>
            {values => {
                // const { temp_chapter } = values.store
                const TypeA = (props) => {
                    const { content, image } = props.data
                    var realImg = SUBCHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            <div className="col-md-3 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                                <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
                        </div>
                    )
                }
                
                const TypeB = (props) => {
                    const { content, image } = props.data
                    var realImg = SUBCHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            
                            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
                
                            <div className="col-md-3 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                
                        </div>
                    )
                }
                
                const TypeC = (props) => {
                    const { content } = props.data
                    return (
                        <div className="row py-3">
                            
                            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-12"></div>
                
                        </div>
                    )
                }
                
                const TypeD = (props) => {
                    const { image } = props.data
                    var realImg = SUBCHAPTER_IMAGE_URL + image
                    return (
                        <div className="row py-3">
                            
                            <div className="col-md-12 d-flex justify-content-center align-items-center">
                                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                            </div>
                        </div>
                    )
                }


                const MainDataLayout = ({values}) => {
                    const { temp_subchapter } = values.store
                    const { sub_chapter_contents } = temp_subchapter
                    if(sub_chapter_contents) {
                        return (
                            <Fragment>
                            {
                                sub_chapter_contents.map((item, index) => {
                                    const { type} = item
                                    switch(type) {
                                        case 1 : return <TypeA key={index} data={item} />
                                        case 2 : return <TypeB key={index} data={item} />
                                        case 3 : return <TypeC key={index} data={item} />
                                        case 4 : return <TypeD key={index} data={item} />
                                        default: return null
                                    }
                                })
                            }
                
                    
                            </Fragment>
                        )
                    } else return null
                }

                const { sub_chapter_title, sub_chapter_contents, sub_chapter_id, chapter_id } = values.store.temp_subchapter
                const { chapter_title } = values.store.temp_chapter
                const { modify_temp_data, set_temp_subchapter, delete_subchapter, status_edit_subchapter } = values
                const { delete_subchapter_status } = values.store

                if(delete_subchapter_status) return <Redirect to='/admin-chapter' />
                
                if(sub_chapter_id === undefined) return <Redirect to="/admin-chapter" />

                const RenderSubchapterNav = () => {
                    return (
                        <StoreConsumer>
                            {values => {
                                const { subchapter } = values.store.temp_chapter
                                if(subchapter) {
                                    return (
                                        <div style={{ backgroundColor: '#ebeae8' }} className="index-section px-3 mt-3">
                                            <p className="h5 font-weight-bold">Subchapters</p>
                                            {
                                                subchapter.map(item => {
                                                    const { sub_chapter_title, sub_chapter_id } = item
                                                    return (
                                                        <Link key={sub_chapter_id} to="/admin-detailed-subchapter" style={{ textDecoration: 'none', color: '#000'}} onClick={() => set_temp_subchapter(item)}>
                                                            <div className="mt-3 chapter-nav-box d-flex align-items-center px-2">
                                                                <div className="row mt-3">
                                                                    <Icofont style={{ color: 'green'}} className="ml-3 mr-3" icon="check-circled" /> <p>{sub_chapter_title}</p>
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    )
                                                })
                                            }

                                            
    
                                            
                                            
                                        </div>
                                    )
                                } else return null
                                
                            }}
                        </StoreConsumer>
                        
                    )
                }

                return (
                    
                        <div className="row m-5">
                            <div className="col-md-9 add-chapter-layout">
                                <div className="px-3 py-3 mt-3">
                                    <button className="btn btn-danger float-right ml-2"
                                        onClick={() => {delete_subchapter(sub_chapter_id, chapter_id);}}
                                    ><Icofont icon="ui-delete" className="mr-3"/>Delete</button>
                                   

                                    <Link to="/admin-edit-subchapter">
                                        <button className="btn btn-warning float-right ml-2" onClick={() => {modify_temp_data(sub_chapter_contents); status_edit_subchapter(false)}}><Icofont icon="ui-edit" className="mr-3"/>Edit</button>
                                    </Link>
                                    
                                    <h4 className="font-weight-bold text-center" >{sub_chapter_title}</h4>
                                    <MainDataLayout values={values}/>
                                </div>
                            </div>

                            <div className="col-md-3 col-sm-4">

                                <div style={{ backgroundColor: '#ebeae8' }} className="index-section px-3">
                                    <p className="h5 font-weight-bold"> <Icofont icon="arrow-left" />  Back - Chapter</p>
                                    <Link to='/admin-detailed-chapter' style={{ textDecoration: 'none', color: '#000'}}>
                                        <div className="my-3 d-flex justify-content-center align-items-center" style={{ padding: 5, backgroundColor: '#fff', borderRadius: 5 }}>
                                            <h5 className="mt-2">{chapter_title}</h5>
                                        </div>
                                    </Link>
                                    
                                </div>

                               <RenderSubchapterNav />

                            </div>


                            
                        </div>
                        
                )
            }}
        </StoreConsumer>
        
    )
}

export default AdminSubChapterDetail
