import React, { Fragment, useState, useEffect } from 'react'
import {Redirect, Link} from 'react-router-dom'
import Icofont from 'react-icofont'
import { StoreConsumer } from '../../../../Store/Store'
import AddChapterContents from '../../../Elements/Chapter/AddChapterContents'


function AddSubChapter() {

    useEffect(() => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
    })

    return (
        <StoreConsumer>
            
            {values => {

                const { modify_temp_data } = values
                const { temp_chapter_data, add_subchapter_redirect } = values.store
                if(values.store.temp_product.course_id === undefined) {
                    return (
                        <Redirect to='/courses' />
                    )
                }

                if(add_subchapter_redirect) return <Redirect to='/admin-chapter' />

           
            const ChapterDataController = () => {

                const add_typeA = () => {
                    var temp_content = {
                        "type": 1,
                        "content": "<p style='text-align: justify'><strong>Welcome to the Basics of Strategic Management!</strong>Strategic Management is all about identification and description of the strategies that managers can carry to achieve better performance and a competitive advantage for their organization.</p>",
                        "image": "temporary!!00.png"
                        }
                    let temporary_data = temp_chapter_data
                    temporary_data.push(temp_content)
                    modify_temp_data(temporary_data)
                    
                }

                const add_typeB = () => {
                    var temp_content = {
                        "type": 2,
                        "content": "<p style='text-align: justify'><strong>Welcome to the Basics of Strategic Management!</strong>Strategic Management is all about identification and description of the strategies that managers can carry to achieve better performance and a competitive advantage for their organization.</p>",
                        "image": "temporary!!00.png"
                        }
                    let temporary_data = temp_chapter_data
                    temporary_data.push(temp_content)
                    modify_temp_data(temporary_data)
                    
                }

                const add_typeC = () => {
                    var temp_content = {
                        "type": 3,
                        "content": "<p style='text-align: justify'><strong>Welcome to the Basics of Strategic Management!</strong>Strategic Management is all about identification and description of the strategies that managers can carry to achieve better performance and a competitive advantage for their organization.</p>",
                        "image": ""
                        }
                    let temporary_data = temp_chapter_data
                    temporary_data.push(temp_content)
                    modify_temp_data(temporary_data)
                    
                }

                const add_typeD= () => {
                    var temp_content = {
                        "type": 4,
                        "content": "",
                        "image": "temporary!!00.png"
                        }
                    let temporary_data = temp_chapter_data
                    temporary_data.push(temp_content)
                    modify_temp_data(temporary_data)
                    
                }


                return (
                    <Fragment>
                        <AddChapterContents values={values} temp={temp_chapter_data}/>

                        <div className="row d-flex justify-content-center mt-5">
                            <div className="add-chapter-controller">
                                <button className="btn btn-info m-2" onClick={() => add_typeA()}>Image + Text</button>
                                <button className="btn btn-warning m-2" onClick={() => add_typeB()}>Text + Image</button>
                                <button className="btn btn-info m-2" onClick={() => add_typeC()}>Text</button>
                                <button className="btn btn-warning m-2" onClick={() => add_typeD()}>Image</button>
                            </div>
                        </div>
                    </Fragment>
                    
                )
            }
            
            const FromRender = ({values}) => {
                const [title, setTitle] = useState('')
                

                function add_subchapter_fx () {
                    const { temp_chapter_data } = values.store
                    const { add_subchapter } = values


                    if(title) {

                            //processign data
                            if(temp_chapter_data.length > 0) {

                                var image_arr = []
                                var updated_content_arr = []

                                temp_chapter_data.forEach(item => {
                                    const { type, image, ext, file } = item

                                    if(image !== 'temporary!!00.png') {

                                        if(type === 3) {
                                            updated_content_arr.push(item)
                                        } else {
                                            var timestamp = new Date().getUTCMilliseconds();
                                            var imageData = {
                                                name: 'image_' +  Math.floor(Math.random()*timestamp) + '.' + ext,
                                                image: file
                                            }
                                            let updated_data = item
                                            updated_data.image = imageData.name
                                            delete updated_data.ext
                                            delete updated_data.file
                                            image_arr.push(imageData)
                                            updated_content_arr.push(updated_data)


                                        }

                                    } else {
                                        alert('some of Image of a content is not updated')
                                    }
                                    
                                });

                                if(temp_chapter_data.length === updated_content_arr.length) {
                                    add_subchapter(updated_content_arr, image_arr, title)
                                }

                            } else {
                                alert('Chapter Contents are empty')
                            }
                            
                    } else {
                        alert('Chapter Title field is empty')
                    }
                }

                return (
                    <Fragment>
                        <div className="col-md-4">
                            <label>Chapter Title <span style={{ color: 'red' }}>*</span> </label>
                            <input type="text" className="form-control" placeholder="Chapter title"
                                value={title} 
                                onChange={(text) => setTitle(text.target.value)} 
                            />
                        </div>

                        <div className="col-md-4">
                            
                        </div>

                        <div className="col-md-4 d-flex justify-content-center align-items-center">
                            <button className="btn btn-success btn-lg" onClick={() => add_subchapter_fx()}>Add Sub Chapter</button>
                        </div>
                    </Fragment>
                )
            }
            
                return (
                    <div className="container mt-5">
                        <Link style={{ textDecoration: 'none', color: '#000'}} to="/admin-detailed-chapter">
                            <p className="h5 font-weight-bold text-right mb-2"> <Icofont icon="arrow-left" />  Back</p>
                        </Link>
                        <div className="col-md-12 add-chapter-layout py-5 mb-5">
                            <div className="row mb-3 mb-5">
                                <StoreConsumer>
                                    {values => {
                                        return (
                                            <FromRender values={values}/>
                                        )
                                    }}
                                </StoreConsumer>
                                
                            </div>
                            <hr />


                            <ChapterDataController />

                        </div>
                    </div>
                )
            }}
        </StoreConsumer>
       
    )
}

export default AddSubChapter
