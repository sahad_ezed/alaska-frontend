import React, {useState, useEffect} from 'react'
import Icofont from 'react-icofont';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom'
import { StoreConsumer } from '../../../../Store/Store'
import { COURSE_IMAGE_URL } from '../../../../Store/Config'

function EditCourse(props) {


    const data = {
        course_name: '', 
        category_id: '', 
        price: '',
        offer_percentage: '',
        introduction: '', 
        skills: [], 
        about: '',
        type: '', 
        duration: '',  
        award: '',  
        language: '',  
        level: ''
    }

    const [redirect, setRedirect] = useState(false)

    const [postData, setPostData] = useState(data)

    const [skill, setSkill] = useState([])
    const [tempSkill, setTempSkill] = useState('')

    const [about, setAbout] = useState([])
    const [tempAbout, setTempAbout] = useState('')

    const [image, setImage] = useState('')
    const [tempImg, setTempImg] = useState('')
   

    const { course_id,  course_name, introduction, course_offered, category_id, type, duration, award, language, level, price, offer_percentage } = postData

    const CourseImage = () => { 

        if(image || tempImg) {
            return <img style={{ width: '100%' }} src={tempImg} alt="product"/> 
        } else {
            return (null)
        }
    }

    const validateDatas = () => {
        if(course_id) {
        if(course_name) {
            if(category_id) {
                if(introduction) {
                    if(skill.length > 0) {
                        if(type) {
                            if(duration) {
                                if(award) {
                                    if(language) {
                                        if(level) {
                                                if(price) {
                                                    if(offer_percentage) {
                                                        if(about.length > 0) {
                                                            return null
                                                        } else {
                                                            return 'About Field is Empty'
                                                        }
                                                    } else {
                                                        return 'Offer Percentage Field is Empty'
                                                    }
                                                } else {
                                                    return 'Price Field is Empty'
                                                }
                                            } else {
                                                return 'Level Field is Empty'
                                            }
                                        } else {
                                            return 'Language Field is Empty'
                                        }
                                    } else {
                                        return 'Award Field is Empty'
                                    }
                                } else {
                                    return 'Duration Field is Empty'
                                }
                            } else {
                                return 'Type Field is Empty'
                            }
                        } else {
                            return 'Skill Field is Empty'
                        }
                    } else {
                        return 'Introduction Field is Empty'
                    }
                } else {
                    return 'Category Field is Empty'
                }
            } else {
                return 'Title Field is Empty'
            }
        } else {
            return 'Course ID is Empty'
        }
    }


    const UpdateCourse = async (update_fx, update_img_fx) => {

            //check any changes affected
           var previous_data = props.data
           var tem_skill = previous_data.skills
           var tem_abt = previous_data.about
           var tem_course_image = previous_data.course_image
           delete previous_data['course_image']
           delete previous_data['rating']
           delete previous_data['skills']
           delete previous_data['about']
           previous_data.skills = tem_skill
           previous_data.about = tem_abt

            var updated_data = postData
            var skillConvertion = JSON.stringify(skill)
            var aboutConversion = JSON.stringify(about)
            
            updated_data.skills = skillConvertion
            updated_data.about = aboutConversion
           

           var tst_prv = JSON.stringify(previous_data)
           var tst_upd = JSON.stringify(updated_data)

            if(tst_prv === tst_upd) {
                
                if(!image) {
                    
                    swal("Error!", 'No changes here', "error");
                } else {
                    update_img_fx(image, tem_course_image)
                    setRedirect(true)
                }

            } else {

                var errors = validateDatas()
                if(errors === null) {
                    update_fx(updated_data)
                    if(image) {
                        update_img_fx(image, tem_course_image)
                    }
                    setRedirect(true)

                } else {
                    swal("Error!", errors, "error");
                }
                
            }

           

            
    }

    useEffect(() => {
        const { course_id, course_name, category_id, price, offer_percentage, course_image, introduction, course_offered, skills, about, type, duration, award, language, level} = props.data
        const Course_data = {
            course_id: course_id,
            course_name: course_name, 
            category_id: category_id, 
            price: price,
            offer_percentage: offer_percentage,
            introduction: introduction, 
            type: type, 
            duration: duration,  
            award: award,  
            language: language,  
            level: level,
            course_offered: course_offered
        }

        //skill
        var skill_data = JSON.parse(skills)
        setSkill(skill_data)

        //about
        var about_data = JSON.parse(about)
        setAbout(about_data)

        //image
        var imageUrl = COURSE_IMAGE_URL + course_image
        setTempImg(imageUrl)
        setPostData(Course_data)

    }, [props.data])

    if(redirect) return <Redirect to='/courses' />

    return (
        <StoreConsumer>
            {values => {
                const { category_list } = values.store
                const { updateCourse, update_course_image } = values
    
                return (
                    <div style={{ background: '#f0eee9'}} className="container mt-5 mb-5 py-3">
                        <h3 className="text-secondary font-weight-bold">Edit Course</h3>
                        <div className="row">
                            <div className="col-md-6">

                                <input className="form-control mt-3" placeholder="Title"
                                    value={course_name}
                                    onChange={(text) => {
                                        var updateDate = {...postData, course_name: text.target.value}
                                        setPostData(updateDate)
                                }} />

                                <select className="form-control mt-3" 
                                    value={category_id} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, category_id: parseInt(text.target.value)}
                                        setPostData(updateDate)
                                    }}
                                >
                                    {
                                        category_list.map((item) => {
                                            const { category_id, category_name} = item
                                            return (
                                                <option key={category_id} value={category_id}>{category_name}</option>
                                            )
                                        })
                                    }
                                </select>

                                <textarea className="form-control-plaintext bg-white mt-3 pl-3" placeholder="Introduction" 
                                    value={introduction} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, introduction: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                />

                                <input className="form-control bg-white mt-3 pl-3" type="text" placeholder="Course Offered" 
                                    value={course_offered} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, course_offered: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                />

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Skills for course</h5>
                                        <Icofont style={{ float: 'right', paddingBottom: 30, paddingRight: 30 }} icon='trash' 
                                            onClick={() => setSkill([])}
                                            className="btn"
                                        />
                                        {
                                            skill.map((item, i) => {
                                                const { content } = item
                                                return (
                                                <input key={i} className="form-control mt-3" defaultValue={content} placeholder="Skill" 
                                                onChange={(text) => {
                                                    var obj = {content: text.target.value}
                                                    var temp = skill
                                                    temp[i] = obj
                                                    setSkill(temp)
                                                }}
                                                />      
                                                )
                                            })
                                        }
                                    
                                        <input className="form-control mt-3" placeholder="Skill"
                                            value={tempSkill}
                                            onChange={(text) => setTempSkill(text.target.value)}
                                        />

                                    
                                    <div className="d-flex justify-content-center mt-3">
                                        <Icofont icon="plus-circle" className="r-0" style={{fontSize: 25}} 
                                            onClick={() => {
                                                // var temp = data_skill
                                                if(tempSkill !== "") {
                                                    var obj = { content: tempSkill}
                                                    setSkill(data => [...data, obj])
                                                    setTempSkill('')
                                                }
                                                
                                            }}
                                        />
                                    </div>

                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Other Details</h5>
                                
                                    <select className="form-control mt-3" 
                                        value={type}
                                        onChange={(text) => {
                                            var updateDate = {...postData, type: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    >
                                        <option>Type</option>
                                        <option>Online Course</option>
                                    </select>

                                    <input type="text" className="form-control mt-3" placeholder="Duration" 
                                        value={duration}
                                        onChange={(text) => {
                                            var updateDate = {...postData, duration: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    />

                                    <select className="form-control mt-3"
                                        value={award}
                                        onChange={(text) => {
                                            var updateDate = {...postData, award: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    >
                                        <option>Award</option>
                                        <option>Certificate</option>
                                    </select>

                                    <select className="form-control mt-3"
                                        value={language}
                                        onChange={(text) => {
                                            var updateDate = {...postData, language: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    >
                                        <option>Language</option>
                                        <option>English</option>
                                    </select>

                                    <input type="text" className="form-control mt-3" placeholder="Level" 
                                    value={level}
                                    onChange={(text) => {
                                        var updateDate = {...postData, level: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                    />
                                    
                                </div>

                            </div>

                            <div className="col-md-6">

                                <div className="bg-white mt-3 px-3 py-3">
                                    <label className="col-form-label">Course Image</label>
                                    <input className="form-control-file " type="file" 
                                    onChange={(image) => {
                                        const objectUrl = URL.createObjectURL(image.target.files[0])
                                        setTempImg(objectUrl)
                                        setImage(image.target.files[0])
                                    }}/>
                                    <CourseImage />
                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Price</h5>
                                    <input type="number" className="form-control mt-3" placeholder="Price"
                                        value={price}
                                        onChange={(text) => {
                                            var updateDate = {...postData, price: parseInt(text.target.value)}
                                            setPostData(updateDate)
                                        }}
                                    />
                                    <input type="number" className="form-control mt-3" placeholder="Offer percentage"
                                        value={offer_percentage}
                                        onChange={(text) => {
                                            var updateDate = {...postData, offer_percentage: parseInt(text.target.value)}
                                            setPostData(updateDate)
                                        }}
                                    />
                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>About Course</h5>

                                    <Icofont style={{ float: 'right', paddingBottom: 30, paddingRight: 30 }} icon='trash' 
                                            onClick={() => setAbout([])}
                                            className="btn"
                                        />
                                        {
                                            about.map((item, i) => {
                                                const { content } = item
                                                return (
                                                <input key={i} className="form-control mt-3" defaultValue={content} placeholder="Skill" 
                                                onChange={(text) => {
                                                    var obj = {content: text.target.value}
                                                    var temp = about
                                                    temp[i] = obj
                                                    setAbout(temp)
                                                }}
                                                />      
                                                )
                                            })
                                        }
                                    
                                        <input className="form-control mt-3" placeholder="About"
                                            value={tempAbout}
                                            onChange={(text) => setTempAbout(text.target.value)}
                                        />

                                    
                                    <div className="d-flex justify-content-center mt-3">
                                        <Icofont icon="plus-circle" className="r-0" style={{fontSize: 25}} 
                                            onClick={() => {
                                                if(tempAbout !== "") {
                                                    var obj = { content: tempAbout}
                                                    setAbout(data => [...data, obj])
                                                    setTempAbout('')
                                                }
                                            }}
                                        />
                                    </div>

                                </div>

                                <div className="bg-white mt-3 px-3 py-3" >
                                    <button style={{height: 100 }} onClick={() => {UpdateCourse(updateCourse, update_course_image)}} className="btn btn-warning btn-lg w-100">Update Course</button>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                )

            }}
        </StoreConsumer>
            
        )
        
}

export default EditCourse
