import React, {useState} from 'react'
import Icofont from 'react-icofont';
import { Redirect } from 'react-router-dom'
import swal from 'sweetalert';
import { StoreConsumer } from '../../../../Store/Store'

function AddCourse() {

    const data = {
        course_name: '', 
        category_id: '', 
        price: '',
        offer_percentage: '',
        introduction: '', 
        skills: [], 
        about: '',
        type: '', 
        duration: '',  
        award: '',  
        language: '',  
        level: ''
    }

    const [redirect, setRedirect] = useState(false)

    const [postData, setPostData] = useState(data)

    const [skill, setSkill] = useState([])
    const [tempSkill, setTempSkill] = useState('')

    const [about, setAbout] = useState([])
    const [tempAbout, setTempAbout] = useState('')

    const [image, setImage] = useState('')
    const [tempImg, setTempImg] = useState('')
   

    const { course_name, introduction, course_offered, category_id, type, duration, award, language, level, price, offer_percentage } = postData

    const CourseImage = () => { 
        if(image) {
            return <img style={{ width: '100%' }} src={tempImg} alt="product"/> 
        } else {
            return (null)
        }
    }

    const validateDatas = () => {
        if(course_name) {
            if(category_id) {
                if(introduction) {
                    if(skill.length > 0) {
                        if(type) {
                            if(duration) {
                                if(award) {
                                    if(language) {
                                        if(level) {
                                            if(image) {
                                                if(price) {
                                                    if(offer_percentage) {
                                                        if(about.length > 0) {
                                                            return null
                                                        } else {
                                                            return 'About Field is Empty'
                                                        }
                                                    } else {
                                                        return 'Offer Percentage Field is Empty'
                                                    }
                                                } else {
                                                    return 'Price Field is Empty'
                                                }
                                            } else {
                                                return 'Image Field is Empty'
                                            }
                                        } else {
                                            return 'Level Field is Empty'
                                        }
                                    } else {
                                        return 'Language Field is Empty'
                                    }
                                } else {
                                    return 'Award Field is Empty'
                                }
                            } else {
                                return 'Duration Field is Empty'
                            }
                        } else {
                            return 'Type Field is Empty'
                        }
                    } else {
                        return 'Skill Field is Empty'
                    }
                } else {
                    return 'Introduction Field is Empty'
                }
            } else {
                return 'Category Field is Empty'
            }
        } else {
            return 'Title Field is Empty'
        }
    }


    const Upload_Course = async (upload_fx, category_id_value) => {
            
            //set default category
            if(!category_id) {
                var updateDate = {...postData, category_id: category_id_value}
                await setPostData(updateDate)
            }

            var errors = validateDatas()
            if(errors === null) {

                    var skillConvertion = JSON.stringify(skill)
                    var aboutConversion = JSON.stringify(about)
                    var postDta = postData
                    postDta.skills = skillConvertion
                    postDta.about = aboutConversion
                    upload_fx(postDta, image)
                    setRedirect(true)

                } else {
                    swal("Error!", errors, "error");
                }
    }
    if(redirect) return <Redirect to='/courses' />
    return (
        <StoreConsumer>
            {values => {
                const { category_list, is_admin_access } = values.store
                const { addCourse } = values

                if(!is_admin_access) return <Redirect to="/admin" />

                return (
                    <div style={{ background: '#f0eee9'}} className="container mt-5 mb-5 py-3">
                        <h3 className="text-secondary font-weight-bold">Add Course</h3>
                        <div className="row">
                            <div className="col-md-6">

                                <input className="form-control mt-3" placeholder="Title"
                                    value={course_name}
                                    onChange={(text) => {
                                        var updateDate = {...postData, course_name: text.target.value}
                                        setPostData(updateDate)
                                }} />

                                <select className="form-control mt-3" 
                                    value={category_id} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, category_id: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                >
                                    {
                                        category_list.map((item) => {
                                            const { category_id, category_name} = item
                                            return (
                                                <option key={category_id} value={category_id}>{category_name}</option>
                                            )
                                        })
                                    }
                                </select>
                                
                                <textarea className="form-control-plaintext bg-white mt-3 pl-3" placeholder="Introduction" 
                                    value={introduction} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, introduction: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                />

                                <input className="form-control bg-white mt-3 pl-3" type="text" placeholder="Course Offered" 
                                    value={course_offered} 
                                    onChange={(text) => {
                                        var updateDate = {...postData, course_offered: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                />

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Skills for course</h5>
                                        <Icofont style={{ float: 'right', paddingBottom: 30, paddingRight: 30 }} icon='trash' 
                                            onClick={() => setSkill([])}
                                            className="btn"
                                        />
                                        {
                                            skill.map((item, i) => {
                                                const { content } = item
                                                return (
                                                   <input key={i} className="form-control mt-3" defaultValue={content} placeholder="Skill" 
                                                   onChange={(text) => {
                                                       var obj = {content: text.target.value}
                                                       var temp = skill
                                                       temp[i] = obj
                                                       setSkill(temp)
                                                   }}
                                                   />      
                                                )
                                            })
                                        }
                                    
                                        <input className="form-control mt-3" placeholder="Skill"
                                            value={tempSkill}
                                            onChange={(text) => setTempSkill(text.target.value)}
                                        />

                                    
                                    <div className="d-flex justify-content-center mt-3">
                                        <Icofont icon="plus-circle" className="r-0" style={{fontSize: 25}} 
                                            onClick={() => {
                                                // var temp = data_skill
                                                var obj = { content: tempSkill}
                                                setSkill(data => [...data, obj])
                                                setTempSkill('')
                                            }}
                                        />
                                    </div>

                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Other Details</h5>
                                
                                    <select className="form-control mt-3" 
                                        value={type}
                                        onChange={(text) => {
                                            var updateDate = {...postData, type: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    >
                                        <option>Type</option>
                                        <option>Online Course</option>
                                    </select>

                                    <input type="text" className="form-control mt-3" placeholder="Duration" 
                                        value={duration}
                                        onChange={(text) => {
                                            var updateDate = {...postData, duration: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    />

                                    <select className="form-control mt-3"
                                        value={award}
                                        onChange={(text) => {
                                            var updateDate = {...postData, award: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                     >
                                        <option>Award</option>
                                        <option>Certificate</option>
                                    </select>

                                    <select className="form-control mt-3"
                                        value={language}
                                        onChange={(text) => {
                                            var updateDate = {...postData, language: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    >
                                        <option>Language</option>
                                        <option>English</option>
                                    </select>

                                    <input type="text" className="form-control mt-3" placeholder="Level" 
                                    value={level}
                                    onChange={(text) => {
                                        var updateDate = {...postData, level: text.target.value}
                                        setPostData(updateDate)
                                    }}
                                    />
                                    
                                </div>

                            </div>

                            <div className="col-md-6">

                                <div className="bg-white mt-3 px-3 py-3">
                                    <label className="col-form-label">Course Image</label>
                                    <input className="form-control-file " type="file" 
                                    onChange={(image) => {
                                        const objectUrl = URL.createObjectURL(image.target.files[0])
                                        setTempImg(objectUrl)
                                        setImage(image.target.files[0])
                                    }}/>
                                    <CourseImage />
                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>Price</h5>
                                    <input type="text" className="form-control mt-3" placeholder="Price"
                                        value={price}
                                        onChange={(text) => {
                                            var updateDate = {...postData, price: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                     />
                                    <input type="text" className="form-control mt-3" placeholder="Offer percentage"
                                        value={offer_percentage}
                                        onChange={(text) => {
                                            var updateDate = {...postData, offer_percentage: text.target.value}
                                            setPostData(updateDate)
                                        }}
                                    />
                                </div>

                                <div className="bg-white mt-3 px-3 py-3">
                                    <h5>About Course</h5>

                                    <Icofont style={{ float: 'right', paddingBottom: 30, paddingRight: 30 }} icon='trash' 
                                            onClick={() => setAbout([])}
                                            className="btn"
                                        />
                                        {
                                            about.map((item, i) => {
                                                const { content } = item
                                                return (
                                                   <input key={i} className="form-control mt-3" defaultValue={content} placeholder="Skill" 
                                                   onChange={(text) => {
                                                       var obj = {content: text.target.value}
                                                       var temp = about
                                                       temp[i] = obj
                                                       setAbout(temp)
                                                   }}
                                                   />      
                                                )
                                            })
                                        }
                                    
                                        <input className="form-control mt-3" placeholder="About"
                                            value={tempAbout}
                                            onChange={(text) => setTempAbout(text.target.value)}
                                        />

                                    
                                    <div className="d-flex justify-content-center mt-3">
                                        <Icofont icon="plus-circle" className="r-0" style={{fontSize: 25}} 
                                            onClick={() => {
                                                // var temp = data_skill
                                                var obj = { content: tempAbout}
                                                setAbout(data => [...data, obj])
                                                setTempAbout('')
                                            }}
                                        />
                                    </div>

                                </div>

                                <div className="bg-white mt-3 px-3 py-3" >
                                    <button style={{height: 100 }} onClick={() => {Upload_Course(addCourse, category_list[0].category_id)}} className="btn btn-warning btn-lg w-100">Add Course</button>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                )
            }}
        </StoreConsumer>
            
            )
        
}

export default AddCourse
