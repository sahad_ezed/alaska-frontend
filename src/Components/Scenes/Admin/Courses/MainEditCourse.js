import React from 'react'
import { Redirect } from 'react-router-dom'
import { StoreConsumer } from '../../../../Store/Store'
import EditCourse from './EditCourse'

function TestEdit() {
    return (
        <StoreConsumer>
            {values => {
                 const { temp_product } = values.store
        
                 if(temp_product.course_id === undefined) {
                     return (
                         <Redirect to='/courses' />
                     )
                 } else {
                    return (
                        <EditCourse data={temp_product}/>
                    )
                 }
            }}
        </StoreConsumer>
       
    )
}

export default TestEdit
