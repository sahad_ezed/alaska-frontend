import React, {Fragment, useState} from 'react'
import { Redirect } from 'react-router-dom' 
import { StoreConsumer } from '../../../Store/Store'


function AdminLogin() {
            const [email, setEmail] = useState('')
            const [password, setPassword] = useState('')
            const [error, setError] = useState('')

            const onSubmit = (login_fx) => {
                if(email === '' || password === '') {
                    setError('Email and password required')
                    setTimeout(() => {
                        setError('')
                    }, 3000);
                } else {
                    login_fx(email, password)
                }
                
            }
    return (
        <StoreConsumer>
            {values => {
                 const {admin_login } = values
                 const { login_alert, is_admin_access } = values.store
                 if(is_admin_access) {
                         return <Redirect to="/admin" />
                 }
                return (
                    <Fragment>
                        <div className="container-fluid cart">
                            <div className="container">
                                <div className="row">
                                    <div className="spc50"></div> 
                                    <div className="spc50"></div> 
                                </div>

                                <div className="row register-box mb-5"> 	
                                    <div className="col-md-5">
                                        <h1>Admin Login</h1>
                                        <label>Email:</label>
                                        <input type="email" name="email" onChange={(e) => setEmail(e.target.value)} value={email} />

                                        <label>Password:</label>
                                        <input type="password" name="password" onChange={(e) => setPassword(e.target.value)} value={password}/> 

                                        <span>
                                            <input type="checkbox" name="" />Remember me 
                                        </span>

                                        { error ?  <p className="mt-2 text-danger">{error}</p> : <p></p> }
                                        { login_alert ?  <p className="mt-2 text-danger">{login_alert}</p> : <p></p> }

                                        <button onClick={() => onSubmit(admin_login)}>Login</button>

                                        <span>
                                            Lost your password?
                                        </span>
                                    </div>		

                                </div>
                                </div>
                            </div>

                    </Fragment>
                )
            }}
        </StoreConsumer>
        
    )
}

export default AdminLogin
