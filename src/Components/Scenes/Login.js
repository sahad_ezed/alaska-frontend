import React, {Fragment, useState} from 'react'
import { Redirect } from 'react-router-dom' 
import {Spinner } from 'react-bootstrap'
import { StoreConsumer } from '../../Store/Store'
import EmailVerifyModal from '../Elements/EmailVerifyModal'
import ForgetPasswordModal from '../Elements/ForgetPasswordModal'

function Login() {

    window.scrollTo({
        top: 0,
        behavior: "smooth"
      });
      

            const [email, setEmail] = useState('')
            const [password, setPassword] = useState('')
            const [error, setError] = useState('')

            const[rname, setRname] = useState('')
            const[remail, setRemail] = useState('')
            const[rpassword, setRpassword] = useState('')
            const[rcpassword, setRcpassword] = useState('')
            const[rerror, setRerror] = useState('')

            const onSubmit = (login_fx) => {
                if(email === '' || password === '') {
                    setError('Email and password required')
                    setTimeout(() => {
                        setError('')
                    }, 3000);
                } else {
                    login_fx(email, password)
                }
                
            }

            const onRegister = (register_fx, btn_state) => {
                //validate datas
                if(rname && remail && rpassword && rcpassword) {
                    if(rpassword === rcpassword) {
                        btn_state('loading')
                        register_fx(rname, remail, rpassword)
                    } else {
                        setRerror('Password dosent match')
                        setTimeout(() => {
                            setRerror('')
                        }, 3000);
                    }
                } else {
                    setRerror('All fields are mandatory')
                    setTimeout(() => {
                        setRerror('')
                    }, 3000);
                }
            }

    return (
        <StoreConsumer>
            {values => {
                 const {login, register, reg_btn_state, model_chapter_open, modal_password_open } = values
                 const { login_alert, isUserLogged, login_to_page, register_alert, register_btn_state } = values.store
                 if(isUserLogged) {
                     if(login_to_page) {
                         return <Redirect to={login_to_page} />
                     } else {
                         return <Redirect to="/my-course" />
                     }
                 }

                 const RegButton = () => {
                     if(register_btn_state === 'loading') {
                         return (
                            <button disabled={true} onClick={() => onRegister(register)}>
                                <Spinner animation="border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                            </button>
                            
                         )
                     }
                     return (
                         <button onClick={() => onRegister(register, reg_btn_state)}>
                                Register
                        </button>
                     )
                 }
             
                return (
                    <Fragment>
                        <div className="container-fluid cart">
                            <div className="container">
                                <div className="row">
                                    <div className="spc50"></div> 
                                    <div className="spc50"></div> 
                                </div>

                                <div className="row register-box"> 	
                                    <div className="col-md-5">
                                        <h1>Login</h1>
                                        <label>Email:</label>
                                        <input type="email" name="email" onChange={(e) => setEmail(e.target.value)} value={email} />

                                        <label>Password:</label>
                                        <input type="password" name="password" onChange={(e) => setPassword(e.target.value)} value={password}/> 

                                        {/* <span>
                                            <input type="checkbox" name="" />Remember me 
                                        </span> */}

                                        { error ?  <p className="mt-2 text-danger">{error}</p> : <p></p> }
                                        { login_alert ?  <p className="mt-2 text-danger">{login_alert}</p> : <p></p> }

                                        <button onClick={() => onSubmit(login)}>Login</button>

                                        <span className="btn" onClick={() => modal_password_open()}>
                                            Lost your password?
                                        </span>

                                        <span className="btn" onClick={() => model_chapter_open()}>
                                            Verify Email!
                                        </span>

                                    </div>		

                                    <div className="col-md-2">
                                        <div className="line4">
                                            
                                        </div>
                                    </div>

                                    <div className="col-md-5 mb-4">
                                        <h1>Register</h1>
                                        <label>Name:</label>
                                        <input type="text" name="name" onChange={(text) => setRname(text.target.value)} value={rname}/>

                                        <label>Email:</label>
                                        <input type="email" name="email" onChange={(text) => setRemail(text.target.value)} value={remail} /> 

                                        <label>Password:</label>
                                        <input type="password" name="password" onChange={(text) => setRpassword(text.target.value)} value={rpassword} /> 

                                        <label>Confirm Password:</label>
                                        <input type="password" name="cpassword" onChange={(text) => setRcpassword(text.target.value)} value={rcpassword} />  

                                        {/* <span>
                                            <p>Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our privacy policy.</p>
                                        </span> */}
                                        { rerror ?  <p className="mt-2 text-danger">{rerror}</p> : <p></p> }
                                        { register_alert ?  <p className="mt-2 text-danger">{register_alert}</p> : <p></p> }

                                        <RegButton />
                                        {/* <button disabled={} onClick={() => onRegister(register)}>
                                            Register
                                        </button> */}
                                        
                                        
                                    </div>	

                                </div>
                                </div>
                            </div>

                            <EmailVerifyModal />
                            <ForgetPasswordModal />
                    </Fragment>
                )
            }}
        </StoreConsumer>
        
    )
}

export default Login
