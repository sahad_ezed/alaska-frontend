import React, {useState, Fragment, useEffect} from 'react'
import { Link, Redirect } from 'react-router-dom'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { StoreConsumer } from '../../Store/Store'
import {COURSE_IMAGE_URL} from '../../Store/Config'

function MyAccounts() {

    const[currentPage, setCurrentPage] = useState(0)

    useEffect(() => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }, [])

    const MyCourseRender = () => {
        return (
            <StoreConsumer>
                {values => {
                    const { my_course } = values.store
                    const { set_chapter } = values

                    const MyCourseAlertRender = () => {
                        if(my_course) {
                            if(my_course.length > 0) {
                                return null
                            } else {
                                return (
                                    <div className="container p-4 d-flex flex-column align-items-center">
                                        <p className="text-justify my-3">no course purchased content :   To add a new course, simply select it from our course list.
                                             You will have instant course access and can complete it at your own pace. You will receive a 
                                            certificate at the end of each course. If you complete all courses of a program, you will get 
                                            an additional program certificate.</p>

                                        <Link to="/courses" style={{ textDecoration: 'none', color: '#fff'}}> <button className="btn btn-info btn-lg">Browse Courses</button> </Link>

                                    </div>
                                )
                            }
                        }
                    }

                    return (
                        <Fragment>
                            <h5 className="my-2">My Courses</h5>
                            <div className="row"> 

                                <MyCourseAlertRender />

                                {my_course.map(item => {
                                    
                                    const { order_id, data, certificate_status } = item
                                    const { course_name, course_image } = data
                                    var image = COURSE_IMAGE_URL + course_image

                                    var style = {}
                                    var status = ''
                                    if(certificate_status) {
                                        status = 'Completed'
                                        style = { fontSize: 12, color: '#fff', paddingTop: 6, position: 'absolute', backgroundColor: 'green', padding: 5, borderRadius: 5 }
                                    } else {
                                        status = 'Pending'
                                        style = { fontSize: 12, color: '#000', paddingTop: 6, position: 'absolute', backgroundColor: 'yellow', padding: 5, borderRadius: 5 }

                                    }

                                    return (
                                            
                                            <div className="col-md-4 my-4" key={order_id}>
                                            <Link style={{ textDecoration: 'none' }} onClick={() => set_chapter(order_id)} to="/chapter">
                                                <div className="c-box">
                                                    <p style={style}>{status}</p>
                                                    <div className="c-img">
                                                        <img style={{ height: 200}} src={image} alt="product"/>
                                                    </div>
                                                    <div className="c-dlt">
                                                        <p>{course_name}</p>
                                                    
                                                    </div>
                                                </div>
                                            </Link>
                                            </div>
                                    )  
                                }) }
                            </div>
                        </Fragment>
                    )
                }}
            </StoreConsumer>
            
        )
    }

    const MyCerificateRender = () => {
        return (
            <StoreConsumer>
                {values => {
                    const { my_course } = values.store

                    const Nocertificate = () => {
                        var tst = my_course.find(item => item.certificate_status === 1)
                        if(tst) {
                            return(
                                <h5 className="my-2">Your Certificates </h5>
                            )
                        } else {
                            return(
                                <h5 className="my-2">You have no Certificates </h5>
                            )
                        }
                        
                    }

                    return (
                        <Fragment>
                            <Nocertificate />
                            <div className="row">     
                                {my_course.map(item => {
                                    
                                    const { order_id, data, certificate_status } = item
                                    const { course_name, course_image } = data
                                    var image = COURSE_IMAGE_URL + course_image

                                    if(certificate_status) {
                                        return (
                                            
                                            <div className="col-md-4 my-4" key={order_id}>
                                           
                                                <div className="c-box">
                                                    <div className="c-img">
                                                        <img style={{ height: 200}} src={image} alt="product"/>
                                                    </div>
                                                    <div className="c-dlt">
                                                        <p>{course_name}</p>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    } else return null

                                
                                }) }
                            </div>
                        </Fragment>
                    )
                }}
            </StoreConsumer>
        )
    }

    



    const SettingsRender = ({values}) => {

        var profile_data = {
            first_name: '',
            last_name: '',
            mobile: '',
            gender: 'Male',
            dob: '',
            current_status: 'Student',
            academic_qualification: 'High School'
        }
        
        const [profile, setProfile] = useState(profile_data)

        const{ first_name, last_name, mobile, gender, dob, current_status, academic_qualification} = profile

        const update_profile = () => {
            const { profile_update } = values
            // check validations
            if(first_name) {
                if(last_name) {
                    if(mobile) {
                        if(dob) {
                            // profile_fx(profile)
                            profile_update(profile)
                        } else {
                            alert('DOB field is empty')
                        }
                    } else {
                        alert('Mobile number field is empty')
                    }
                } else {
                    alert('Last Name field is empty')
                }
            } else {
                alert('First Name field is empty')
            }
        }

        const[oldPassword, setOldPassword] = useState('')
        const[newPassword, setNewPasswor] = useState('')
        const[cPassword, setCpassword] = useState('')

        const update_password = () => {
            const { update_password } = values
            if(oldPassword) {
                if(newPassword) {
                    if(cPassword) {
                        
                        if(newPassword === cPassword) {

                            //action
                            update_password(oldPassword, newPassword)

                        } else {
                            alert('Password not matcing')
                        }

                    } else {
                        alert('Confirm password field is empty')
                    }
                } else {
                    alert('New password field is empty')
                }
            } else {
                alert('Current password field is empty')
            }
        }

        const { store } = values
        return (
            <Fragment>
                <h4 className="my-2">Profile </h4>
                <div className="row mt-3">
                        <div className="col-md-6">

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Username</label>
                            <input className="form-control" type="text" value={store.profile.user_name} readOnly/>

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">First Name</label>
                            <input className="form-control" placeholder='First Name' type="text"
                                defaultValue={first_name || store.profile.first_name}
                                onChange={(text) => {
                                    var updateDate = {...profile, first_name: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            />

                
                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Mobile No</label>
                            {/* <input className="form-control" type="number" placeholder='mobile' 
                                defaultValue={mobile || store.profile.mobile}
                                onChange={(text) => {
                                    var updateDate = {...profile, mobile: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            /> */}

                            <PhoneInput
                                country={'us'}
                                value={mobile || store.profile.mobile}
                                onChange={(text) => {
                                    var updateDate = {...profile, mobile: text}
                                    setProfile(updateDate)
                                }} 
                            />

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">DOB</label>
                            <input className="form-control" type="date" placeholder='dob' 
                                defaultValue={dob || store.profile.dob}
                                onChange={(text) => {
                                    var updateDate = {...profile, dob: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            />

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Academic Qualification</label>
                            <select className="form-control"
                                defaultValue={academic_qualification || store.profile.academic_qualification}
                                onChange={(text) => {
                                    var updateDate = {...profile, academic_qualification: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            >
                                <option value="High School">High School</option>
                                <option value="Graduate">Graduate</option>
                                <option value="Post Graduate">Post Graduate</option>
                                <option value="Other">Other</option>
                            </select>


                        </div>

                        <div className="col-md-6">
                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Email</label>
                            <input className="form-control" type="email" value={store.profile.user_email} readOnly/>

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Last Name</label>
                            <input className="form-control" type="text" placeholder='Last Name' 
                                defaultValue={last_name || store.profile.last_name}
                                onChange={(text) => {
                                    var updateDate = {...profile, last_name: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            />

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Gender</label>
                            <select className="form-control"
                                defaultValue={gender || store.profile.gender}
                                onChange={(text) => {
                                    var updateDate = {...profile, gender: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            >
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select>

                            <label className="form-check-label mt-4 pl-2 font-weight-bold">Current Status</label>
                            <select className="form-control"
                                defaultValue={current_status || store.profile.current_status}
                                onChange={(text) => {
                                    var updateDate = {...profile, current_status: text.target.value}
                                    setProfile(updateDate)
                                }} 
                            >
                                <option value="Student">Student</option>
                                <option value="Employee">Employee</option>
                                <option value="Entrepreneur">Entrepreneur</option>
                                <option value="Other">Other</option>
                            </select>

                            <button className="btn btn-warning btn-lg mt-4 float-right mr-4" onClick={() => update_profile(profile)}>Update</button>
                        </div>

                </div>

                <div className="row pl-2 mt-2 flex-column">
                    <h4 className="pl-2 mt-5">Change Password</h4>

                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Current Password</label>
                    <input className="form-control ml-1" type="password" placeholder='Current Password'
                        value={oldPassword}
                        onChange={(text) => setOldPassword(text.target.value)}
                    />
                    
                    <label className="form-check-label mt-4 pl-2 font-weight-bold">New Password</label>
                    <input className="form-control ml-1" type="password" placeholder='New Password'
                        value={newPassword}
                        onChange={(text) => setNewPasswor(text.target.value)}
                    />

                    <label className="form-check-label mt-4 pl-2 font-weight-bold">Confirm Password</label>
                    <input className="form-control ml-1" type="password" placeholder='Confirm Password'
                        value={cPassword}
                        onChange={(text) => setCpassword(text.target.value)}
                    />

                    <button className="btn btn-info btn-lg mt-5 w-25" onClick={() => update_password()}>Update</button>
                    
                </div>
            </Fragment>
                    
            
        )
    }

    const PageManager = () => {
        return (
            <StoreConsumer>
                {values => {
                    switch(currentPage) {
                        case 0: return <MyCourseRender />
                        case 1: return <MyCerificateRender />
                        case 2: return <SettingsRender values={values}/>
                        default: return <MyCourseRender />
                    }
                }}
            </StoreConsumer>
        )
    }

    const NavMyCourse = () => {
        var style = 'account-nav-item'
        if(currentPage === 0) {
            style = 'account-nav-item account-nav-selected'
        }
        return (
            <div className={style} onClick={() => setCurrentPage(0)}>
                <h3 className="account-nav-text">  My Courses </h3>
            </div>
        )
    }

    const NavMyCertificates = () => {
        var style = 'account-nav-item'
        if(currentPage === 1) {
            style = 'account-nav-item account-nav-selected'
        }
        return (
            <div className={style} onClick={() => setCurrentPage(1)}>
                <h3 className="account-nav-text"> My Certificates </h3>
            </div>
        )
    }

    const NavSettings = () => {
        var style = 'account-nav-item'
        if(currentPage === 2) {
            style = 'account-nav-item account-nav-selected'
        }
        return (
            <div className={style} onClick={() => setCurrentPage(2)}>
                <h4 className="account-nav-text"> Settings </h4>
            </div>
        )
    }

    const NavLogout = () => {
        var style = 'account-nav-item'
        return (
            <StoreConsumer>
                {values => {
                    const { logout } = values
                    const { isUserLogged } = values.store
                    if(!isUserLogged) return <Redirect to='/login' />
                    return (
                        <div className={style} onClick={() => logout()}>
                            <h3 className="account-nav-text"> Logout </h3>
                        </div>
                    )
                }}
            </StoreConsumer>
            
        )
    }

    return (
       
            <div className="container mb-5">
                <div className="row">
                    <div className="col-md-3 account-navigation">
                        <NavMyCourse />
                        <NavMyCertificates />
                        <NavSettings />
                        <NavLogout />
                    </div>

                    <div className="col-md-8 ml-4">
                        

                    <PageManager />


                    </div>
                </div>
            </div>
                
        
    )
}

export default MyAccounts
