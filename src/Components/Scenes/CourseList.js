import React, {Fragment, useEffect} from 'react'
import { Link } from 'react-router-dom'
import Protfolio1 from '../Elements/Protfolio1'
import CategoryCourseList from '../Elements/course/CategoryCourseList'
import { StoreConsumer } from '../../Store/Store'

function CourseList() {

    useEffect(() => {
        // window.location.reload();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
    })

    const AddButton = (props) => {
        const { is_admin_access } = props
        if(is_admin_access) {
            return(
                <Link style={{ textDecoration: 'none' }} to="/admin-add-courses">
                    <button className="btn btn-lg btn-warning float-right mr-5">Add Course</button>
                </Link>
            )
        } else return null
        
    }



    return (
            <StoreConsumer>
                {values => {
                    const { category_list, is_admin_access } = values.store
                    return (
                        <Fragment>
                            <div className="container-fluid boxes">
                                
                            <AddButton is_admin_access={is_admin_access} />
                                {
                                    category_list.map(item => {
                                        const { category_id } = item
                                        return (
                                            <CategoryCourseList key={category_id} data={item} />
                                        )
                                    })
                                }  
                            </div>
                            <Protfolio1 />
                        </Fragment>
                    )
                }}
            </StoreConsumer>
            
    )
}

export default CourseList
