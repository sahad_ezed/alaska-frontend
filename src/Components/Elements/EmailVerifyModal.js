import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import Icofont from 'react-icofont';
import { StoreConsumer } from '../../Store/Store'

const ModalContainer = styled.div`
    position: fixed;
    overflow: hidden;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: rgba(0,0,0,0.3);
    display: flex;
    align-items: center;
    justify-content: center;

    #modal {
        background: #fff;
        z-index: 10000000;
        width: 600;
        overflow: hidden;
    }

`;

function EmailVerifyModal() {
    return (
        <StoreConsumer>
            {values => {
                const { modal_chapter } = values.store
                const { model_chapter_close, VerifyEmailRetry } = values

                if(modal_chapter) {

                const VerifyForm = () => {

                    const [email, setEmail] = useState('')
                    var btn = true

                    if(email) {
                        btn = false
                    }

                    const verifyButton = () => {
                        var data = {
                            email: email
                        }
                        VerifyEmailRetry(data)
                        model_chapter_close()
                    }

                    return (
                        <Fragment>
                            <div className="row mt-5">
                                <input type="email" placeholder="Your Email" className="form-control"
                                    value={email}
                                    onChange={(txt) => setEmail(txt.target.value)}
                                />
                            </div>
                            <button disabled={btn} className="btn btn-primary mt-3" onClick={() => verifyButton()}>Send Verification</button> 
                        </Fragment>
                    )
                }


                return (
                    <Fragment>
                    <ModalContainer>
                       <div className="container">
                           <div className="row">
                               <div id="modal" className="mx-auto text-center text-capitalize p-5 rounded">
                               <button className="btn float-right" onClick={() => model_chapter_close()} ><Icofont style={{ fontSize: 30 }} icon="close"/> </button>
                                   <h5 className="text-muted mt-5"><u>Verify Your Email</u></h5>
                                  
       
                                   <div style={{ width: 500 }} className="mt-2">
                                      
                                       <VerifyForm />
       
                                       {/* <h5 className="text-left my-3"><u>Purchased Course</u></h5>
                                       <div className="row">
       

                                       </div> */}
                                   </div>
                               </div>
                           </div>
                       </div>
                   </ModalContainer>
               </Fragment>
                )
            } else return null
            }}
        </StoreConsumer>
       
       
    )
}

export default EmailVerifyModal
