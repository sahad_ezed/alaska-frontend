import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'
import SingleCourse from './SingleCourse'

function MyCourses() {

    var data = { course_name: 'course abc', course_image: 'image_3.jpeg', rating: 2, level: 'IT' }

    return (
        <Fragment>
        <h3>My Courses</h3>
        <div className="row">
            <Link to="/chapter">
                <SingleCourse data={data}/>
            </Link>
            <SingleCourse data={data}/>
            <SingleCourse data={data}/>

            <SingleCourse data={data}/>
            <SingleCourse data={data}/>
            <SingleCourse data={data}/>
        </div>
        </Fragment>
    )
}

export default MyCourses
