import React from 'react'
import { Link } from 'react-router-dom'
import Icofont from 'react-icofont';
import { COURSE_IMAGE_URL } from '../../../Store/Config'
import { StoreConsumer } from '../../../Store/Store'

const StarRender = (props) => {
    const { rating } = props
    if(rating) {
        var activeColor = {color: '#fa8c25'}
        var disableColor = {color: 'grey'}

        var totalStars = 5
        var disableCount = totalStars - rating
        
        return(
            <div className="row ml-1 mb-2 mt-2">

                {
                    Array.from(Array(rating), (e, i) => {
                        return <Icofont key={i} style={activeColor} icon="star"></Icofont>
                    }) 
                }

                {
                    Array.from(Array(disableCount), (e, i) => {
                        return <Icofont key={i} style={disableColor} icon="star"></Icofont>
                    }) 
                }

            </div>
        
        )
    } else {
        return null
    }
    
}

function SingleCourse(props) {
    const { course_id, course_name, course_image, rating, level, course_offered } = props.data
    var image = COURSE_IMAGE_URL + course_image

    return (
        <StoreConsumer>
            {values => { 
                const { set_temp_product, practical_course } = values
                const CourseOffered = () => {
                    if(course_offered) {
                        return (
                            <p style={{ color: 'grey', fontSize: 16, marginBottom: 2 }}><span style={{ fontSize: 12, paddingRight: 5 }}>Course Offered:</span> {course_offered} </p>
                        )
                    } else return (  <p style={{ color: 'grey', fontSize: 16, marginBottom: 2 }}><span style={{ fontSize: 12 }}>Course Offered:</span> Alaska </p>                    )
                    
                }
                return (
                    <div className="col-md-3 my-4">
                        <Link style={{ textDecoration: 'none' }} onClick={() => {set_temp_product(props.data); practical_course(course_id)}} to="/course">
                            <div className="c-box">
                                <div className="c-img">
                                    <img style={{ height: 260}} src={image} alt="product"/>
                                </div>
                                <div className="c-dlt">
                                    <h1>{course_name}</h1>
                                    <StarRender rating={rating} />
                                    <CourseOffered />
                                    <p><i className="icofont-certificate mr-2"></i> Get certificate</p>
                                    <p><i className="icofont-briefcase mr-2"></i> 100% Placement</p>
                                    <p><i className="icofont-engineer mr-2"></i>{level}</p>
                                </div>
                            </div>
                        </Link>
                    </div>
                )
            }}
        </StoreConsumer>
    )
}

export default SingleCourse
