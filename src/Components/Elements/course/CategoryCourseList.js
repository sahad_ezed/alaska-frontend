import React from 'react'
import SingleCourse from './SingleCourse'

function CategoryCourseList(props) {
    const { category_name, courses} = props.data
    return (
        <div className="container">
            <div className="row">
                <div className="spc30"></div>
                <div className="col-md-12">
                    
                        <div className="row">
                            <div className="m-hd">
                                <h2>{category_name}</h2>
                                
                                <span></span>
                            </div>
                        </div>
                        
                        
                </div>

                {
                    courses.map(item => {
                        const { course_id } = item
                        return (
                            <SingleCourse key={course_id} data={item} />
                        )
                    })
                }

            </div>
        </div>
    )
}

export default CategoryCourseList
