import React from 'react'
import {Link} from 'react-router-dom'
import { StoreConsumer } from '../../Store/Store'

function TopNav() {

    return (
        <StoreConsumer>
            {values => {
                const { isUserLogged } = values.store
                const { logout } = values
                if(!isUserLogged) {
                    return (
                        <div className="container-fluid top">
                            <div className="row pr-4">
                                <div className="col-md-12">
                                <Link style={{ textDecoration: 'none' }} to="/login"><p> Register Now | Login</p></Link>
                                </div>
                            </div>
                        </div>
                    )
                } else {
                    return (
                        <div className="container-fluid top">
                            <div className="row pr-4">
                                <div className="col-md-12">
                                    <p onClick={() => logout()}> Logout </p>
                                </div>
                            </div>
                        </div>
                    )
                }
                
            }}
        </StoreConsumer>
        
    )
}

export default TopNav
