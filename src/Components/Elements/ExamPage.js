import React, { useState, Fragment } from 'react'
import swal from 'sweetalert'
import { Redirect } from 'react-router-dom'

function ExamPage({values}) {

    const { exam, course_id } = values.store.chapter
    const { complete_course_status } = values
    const { order_list } = values.store

    const[tempQuestion, setTempQuestion] = useState(exam[0])
    const[temp_answer, setAnswer] = useState(false)
    const[mark, setMark] = useState(0)
    const[redirect, setRedirect]=useState(false)

    const next_question = () => {
        var Index = exam.indexOf(tempQuestion)
        //check mark
        if(temp_answer === 1) {
            setMark(mark + 1)
        } 
        if(Index + 1 === exam.length) {
            if(mark > 4) {

                var order_data = order_list.find(item => item.course_id === course_id)
                const { order_id, certificate_status} = order_data
                if(certificate_status === 0) {
                    complete_course_status(order_id)
                }

                swal("Congratulations", "You are eligible for certificate", "success").then(() => {
                    setRedirect(true)
                })
            } else {
                swal("Sorry", "You failed the exam. Should qualify exam for certificate", "error").then(() => {
                    setRedirect(true)
                })
            }
            
        } else {
            setTempQuestion(exam[Index + 1])
        }
        
    }


    var iIndex = exam.indexOf(tempQuestion)

    if(redirect) return <Redirect to='/my-course' />

    return (
        
            <div className="exam-layout">
                <h4 className="px-3 pt-3">Question No: {iIndex + 1}</h4>

                <h6 className="px-5 pt-3 pb-2">{tempQuestion.question_title}</h6>
                <div className="px-5">
                    {
                        tempQuestion.options.map((item) => {
                            const{ option_id, option_title, is_true} = item
                            return (
                                <Fragment key={option_id}>
                                    <input type="radio" name="option" onChange={() => setAnswer(is_true)}/>
                                    <label className="pl-4">{option_title}</label>
                                    <br />
                                </Fragment>
                            )
                        })
                    }
                </div>
                
                

                <div className="pb-5 pr-5">
                    <button className="btn btn-info float-right" onClick={() => next_question()}>Next</button>
                </div>
            
                
            </div>
                
        
    )
}

export default ExamPage
