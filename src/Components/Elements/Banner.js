import React from 'react'
import { Link } from 'react-router-dom'
import { Carousel } from 'react-bootstrap'

function Banner() {
    return (
        <Carousel>
        <Carousel.Item>
         
            <div className="banner">
                <div className="banner-box">
                    <h1>Lets Get </h1>  
                    <h2>Certificate in</h2> 
                    <h3>Mangement Studies</h3>
                    <Link style={{ textDecoration: "none"}} to="/courses">Browse course</Link>
                </div>
                <img
                    className="d-block w-100"
                    src={require('../Assets/images/slide1.jpg')}
                    alt="First slide"
                />
                {/* <img src={require('../Assets/images/slide1.jpg')} alt="slide1"/> */}
            </div>

          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={require('../Assets/images/slide2.jpg')}
            alt="Third slide"
          />
      
          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={require('../Assets/images/slide3.jpg')}
            alt="Third slide"
          />
      
          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    )
}

export default Banner
