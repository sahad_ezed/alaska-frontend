import React from 'react'
import { StoreConsumer } from '../../Store/Store'
import { PayPalButton } from "react-paypal-button-v2";


function PaypalButton(props) {
    const { redirect } = props
   
    const success_payment = (makeOrder, cart, deleteCart) => {
        return (
            cart.map(item => {
                const { course_id } = item
                makeOrder(course_id)
                deleteCart(course_id)
                redirect(true)
                return null
            })
        )
    }

    return (
        <StoreConsumer>
            {values => {
                const { cart, cart_total } = values.store
                const { makeOrder, deleteCart } = values
                return (
                    //<PaypalExpressBtn env={env} client={client} currency={currency} total={cart_total} onError={onError} onSuccess={() => success_payment(makeOrder, cart, deleteCart)} onCancel={onCancel} />
                    // <button onClick={() => success_payment(makeOrder, cart, deleteCart, cart_total)}>Checkout</button>

                    <PayPalButton
                        amount={cart_total}
                        // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                        onSuccess={(details, data) => {
                            alert("Transaction completed by " + details.payer.name.given_name);
                            success_payment(makeOrder, cart, deleteCart)
                        }}

                        options={{
                            clientId: "AdEXOKR4KLb0kMzGi_c5RoPJEyOLNhwWroFkAtnbO4aDMPhe09pOjdIS3Of31SfFWc1K7AhZQsD7wgcq"
                          }}
                    />
                )
            }}
        </StoreConsumer>
    )
}

export default PaypalButton
