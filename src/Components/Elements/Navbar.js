import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'
import { Navbar, Nav, NavItem, NavDropdown } from 'react-bootstrap'
import { StoreConsumer} from '../../Store/Store'






function NavbarContainer() {

    const AdminNavItems = (props) => {
        const { is_admin_access, cart_list, cart, values } = props
        var NavitemStyle = { color: '#0e0042', fontWeight: '500', fontSize: 16, textDecoration: 'none' }
        const { setLogin_to_page, userDetails } = values
        if(is_admin_access) {
            return (
                <Fragment>

                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/admin-users" onClick={() => userDetails()}>Users</Nav.Link>
                    <NavDropdown title="Courses" id="nav-dropdown">
                        <NavDropdown.Item href="/admin-category">Category</NavDropdown.Item>
                        <NavDropdown.Item href="/admin-courses">Courses</NavDropdown.Item>
                    </NavDropdown>

                </Fragment>
            )
        } else {
            return (
                <Fragment>
                    <NavItem className="px-4"> <Link style={NavitemStyle} to="/" onClick={() => setLogin_to_page('/login')}>Home </Link></NavItem> 
                    <NavItem className="px-4"> <Link style={NavitemStyle} to="/about-us" onClick={() => setLogin_to_page('/login')}>ABOUT ALASKA </Link></NavItem> 
                    <NavItem className="px-4"> <Link style={NavitemStyle} to="/courses">COURSES </Link> </NavItem> 
                    <NavItem className="px-4"> <Link style={NavitemStyle} to="/partner-institutions">PARTNER INSTITUTIONS </Link> </NavItem> 

                    <MyAccountNavItem values={values}/>
                    
                    <NavItem>
                        <Link to="/cart" onClick={() => cart_list()} style={{ textDecoration: 'none' }}>
                            <div style={{ color: '#fff', fontSize: 18, fontWeight: '700', background: '#0e0042', borderWidth: 0 }} className="btn btn-primary px-md-4">
                                <div style={{ width: 30, height: 30, borderRadius: 15, display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'absolute', background: '#c6c8cc', color: '#0e0042', marginTop: -15, marginLeft: 50 }}>
                                        {cart.length}
                                </div>
                                    Cart
                            </div>
                        </Link>
                    </NavItem>
                </Fragment>
            )
        }
    }

    const MyAccountNavItem = ({values}) => {
        const { isUserLogged } = values.store
        var NavitemStyle = { color: '#0e0042', fontWeight: '500', fontSize: 16, textDecoration: 'none' }
        if(isUserLogged) {
            return (
                <NavItem className="px-4"> <Link style={NavitemStyle} to="/my-course">My Account</Link> </NavItem> 
            )
        } else return null
    }
    
    return (
        <StoreConsumer>
            {values => {
                const { cart_list } = values
                const { cart, is_admin_access } = values.store
                if(is_admin_access) {
                    var routing = '/admin'
                } else {
                     routing = '/'
                }
                return (
                    <div className="px-5">
                        <Navbar style={{ background: '#fff' }} fixed collapseOnSelect expand="lg">
                        <Navbar.Brand href={routing}>
                            <img src={require('../Assets/images/logo.png')} alt="logo" /> </Navbar.Brand>
                        <Navbar.Toggle style={{ marginLeft: '90%'}} aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto"></Nav>
                            <Nav>
                                <AdminNavItems is_admin_access={is_admin_access} cart_list={cart_list} cart={cart} values={values} />
                            </Nav>
                        </Navbar.Collapse>
                        </Navbar>
                    </div>
                )
            }}
        </StoreConsumer>
    )
}

export default NavbarContainer
