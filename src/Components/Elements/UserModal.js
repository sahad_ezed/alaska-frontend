import React, { Fragment } from 'react'
import styled from 'styled-components'
import Icofont from 'react-icofont';
import { StoreConsumer } from '../../Store/Store'

const ModalContainer = styled.div`
    position: fixed;
    overflow: hidden;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: rgba(0,0,0,0.3);
    display: flex;
    align-items: center;
    justify-content: center;

    #modal {
        background: #fff;
        z-index: 10000000;
        width: 600;
        overflow: hidden;
    }

`;

function UserModalRender() {
    return (
        <StoreConsumer>
            {values => {
                const { modal_chapter, temp_user_data } = values.store
                const { model_chapter_close } = values
                const { user_name, user_email, first_name,last_name, mobile, gender, dob, current_status, academic_qualification, orders } = temp_user_data
                console.log(temp_user_data)

                const RenderCourse = () => {
                    if(orders.length > 0) {
                        return (
                            <Fragment>
                                {
                                    orders.map(item => {
                                        const { course_name } = item
                                        return (
                                            <div className="col-md-3 mt-3">
                                                <div style={{ height: 100, width: 100, borderRadius: 5, backgroundColor: '#d4d4d4' }} className="d-flex justify-content-center align-items-center" >
                                                    <p style={{ fontSize: 12 }} >{course_name}</p>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </Fragment>
                            
                        )
                    } else return (<p className="ml-4">No items</p>)
                   
                }


                var statusColor = 'yellow'
                if(orders) {
                    if(orders.length > 0) {
                        statusColor = 'green'
                    } else {
                        statusColor = 'yellow'
                    }
                }
                

                if(modal_chapter) {
                return (
                    <Fragment>
                    <ModalContainer>
                       <div className="container">
                           <div className="row">
                               <div id="modal" className="mx-auto text-center text-capitalize p-5 rounded">
                               <button className="btn float-right" onClick={() => model_chapter_close()} ><Icofont style={{ fontSize: 30 }} icon="close"/> </button>
                                   <h5 className="text-muted mt-5"><u>User Details</u></h5>
                                   <div className="row">
                                       <p className="text-left">Status </p>
                                       <div style={{ height: 20, width: 20, backgroundColor: statusColor, borderRadius: 10, marginLeft: 10 }} />
                                   </div>
       
                                   <div style={{ width: 500 }} className="mt-2">
                                       <h4>{user_name}</h4>
                                       <p>{user_email}</p>
                                       <div className="row">
       
                                           <div className="col-md-6 d-flex flex-column align-items-start">
                                               <p>First Name :</p>
                                               <p>Last Name :</p>
                                               <p>Mobile :</p>
                                               <p>Gender :</p>
                                               <p>DOB :</p>
                                               <p>Academic Qualification :</p>
                                               <p>Current Status :</p>
                                           </div>
       
                                           <div className="col-md-6 d-flex flex-column align-items-start">
                                               <p><b>{first_name}</b></p>
                                               <p><b>{last_name}</b></p>
                                               <p><b>{mobile}</b></p>
                                               <p><b>{gender}</b></p>
                                               <p><b>{dob}</b></p>
                                               <p><b>{current_status}</b></p>
                                               <p><b>{academic_qualification}</b></p>
                                           </div>
                                       </div>
       
                                       <h5 className="text-left my-3"><u>Purchased Course</u></h5>
                                       <div className="row">
       
                                           <RenderCourse />

                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </ModalContainer>
               </Fragment>
                )
            } else return null
            }}
        </StoreConsumer>
       
       
    )
}

export default UserModalRender
