import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import Icofont from 'react-icofont';
import { StoreConsumer } from '../../Store/Store'

const ModalContainer = styled.div`
    position: fixed;
    overflow: hidden;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: rgba(0,0,0,0.3);
    display: flex;
    align-items: center;
    justify-content: center;

    #modal {
        background: #fff;
        z-index: 10000000;
        width: 600;
        overflow: hidden;
    }

`;

function ForgetPasswordModal() {
    return (
        <StoreConsumer>
            {values => {
                const { modal_password, otp_valid, otp_err, temp_email } = values.store
                const { modal_password_close, generate_otp, verify_otp, temp_email_fx, reset_password, OtpError } = values
                if(modal_password) {

                const VerifyForm = () => {

                    const [email, setEmail] = useState('')
                    const [otp, setOtp] = useState('')
                    const [password, setPassword] = useState('')
                    const [cpassword, setCpassword] = useState('')

                    var btn = true
                    var otpBtn = true
                    var reset_btn = true

                    if(email) {
                        btn = false
                    }

                    if(otp) {
                        otpBtn = false
                    }

                    if(cpassword && setCpassword) {
                        reset_btn = false
                    }


                    const generate_otp_btn = async () => {
                        var data = {
                            email: email
                        }
                        temp_email_fx(email)
                        generate_otp(data)
                    }

                    const verify_otp_btn = () => {
                        var data = {
                            email: temp_email,
                            otp
                        }

                        verify_otp(data)
                    }

                    const reset_password_btn = () => {
                       
                        if(password === cpassword) {
                            var data = {
                                new_password: password,
                                email: temp_email
                            }
                            reset_password(data)
                        } else {
                            OtpError('Password not match')
                        }
                            
                    
                    }

                    if(otp_valid) {

                        return (
                            <Fragment>
                                 <p style={{ color: 'red', marginTop: 15 }}>{otp_err}</p>
                                 <div className="row mt-5 d-flex flex-column align-items-center">
                                    <div className="col-md-8">
                                        <input type="password" placeholder="New Password" className="form-control"
                                            value={password}
                                            onChange={(txt) => setPassword(txt.target.value)}
                                        />

                                        <input type="password" placeholder="Confirm Password" className="form-control mt-3"
                                            value={cpassword}
                                            onChange={(txt) => setCpassword(txt.target.value)}
                                        />
                                        <button disabled={reset_btn} className="btn btn-primary mt-3" onClick={() => reset_password_btn()}>Reset Password</button> 
                                    </div>                        
                                </div>
                            </Fragment>
                        )

                    } else {

                            return (
                                <Fragment>
                                    <p style={{ color: 'red', marginTop: 15 }}>{otp_err}</p>
                                    <div className="row mt-5">
                                        <div className="col-md-8">
                                            <input type="email" placeholder="Your Email" className="form-control"
                                                value={email}
                                                onChange={(txt) => setEmail(txt.target.value)}
                                            />
                                            
                                        </div>

                                        <div className="col-md-4 d-flex justify-content-start">
                                            <button disabled={btn} className="btn btn-primary" onClick={() => generate_otp_btn()}>Send Verification</button> 
                                        </div>
                                        
                                    </div>
                                    

                                    <div className="row mt-3">
                                        <div className="col-md-8">
                                            <input type="email" placeholder="OTP" className="form-control"
                                                value={otp}
                                                onChange={(txt) => setOtp(txt.target.value)}
                                            />
                                        </div>

                                        <div className="col-md-4 d-flex justify-content-start">
                                            <button disabled={otpBtn} className="btn btn-primary" onClick={() => verify_otp_btn()}>Verify</button> 
                                        </div>
                                    </div>
                                
                                </Fragment>
                            )
                        }
                }


                return (
                    <Fragment>
                    <ModalContainer>
                       <div className="container">
                           <div className="row">
                               <div id="modal" className="mx-auto text-center text-capitalize p-5 rounded">
                               <button className="btn float-right" onClick={() => modal_password_close()} ><Icofont style={{ fontSize: 30 }} icon="close"/> </button>
                                   <h5 className="text-muted mt-5"><u>Forget Password</u></h5>
                                  
       
                                   <div style={{ width: 700 }} className="mt-2">
                                      
                                       <VerifyForm />
       
                                       {/* <h5 className="text-left my-3"><u>Purchased Course</u></h5>
                                       <div className="row">
       

                                       </div> */}
                                   </div>
                               </div>
                           </div>
                       </div>
                   </ModalContainer>
               </Fragment>
                )
            } else return null
            }}
        </StoreConsumer>
       
       
    )
}

export default ForgetPasswordModal
