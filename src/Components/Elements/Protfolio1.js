import React, {Fragment} from 'react'

function Protfolio1() {
    return (
        <Fragment>
            <div className="fxd-bg">
                <div className="container-fluid">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="ftrs">
                                    <img src={require('../Assets/images/icon-skill.png')} alt="icon" />
                                    <h2>Learn New Skills</h2>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="ftrs">
                                    <img src={require('../Assets/images/icon-online.png')} alt="icon" />
                                    <h2>100% Online</h2>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="ftrs">
                                    <img src={require('../Assets/images/icon-certificated.png')} alt="icon" />
                                    <h2>Get Certificated</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Protfolio1
