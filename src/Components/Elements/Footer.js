import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'
import { StoreConsumer } from '../../Store/Store'


function Footer() {
    return (
        <StoreConsumer>
        {values => {
            const { is_admin_access } = values.store
            if(!is_admin_access) {
                return (
                    <Fragment>
                        <div className="container-fluid footer">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="f1">
                                            <h1 >About Alaska</h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="f1">
                                            <h1>Institute</h1>
                                            <ul>  
                                                <li><Link style={{ textDecoration: 'none' }} to="/"><img src={require('../Assets/images/arrow.png')} alt="icon"/>Home </Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/about-us"><img src={require('../Assets/images/arrow.png')} alt="icon"/>About Us</Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/courses"><img src={require('../Assets/images/arrow.png')} alt="icon"/>Courses</Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/faq"><img src={require('../Assets/images/arrow.png')} alt="icon"/>FAQ</Link></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="f1">
                                            <h1>My Account</h1>
                                            <ul>
                                                <li><Link style={{ textDecoration: 'none' }} to="/login"><img src={require('../Assets/images/arrow.png')} alt="icon"/>Register</Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/login"><img src={require('../Assets/images/arrow.png')} alt="icon" />Login</Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/my-course"><img src={require('../Assets/images/arrow.png')} alt="icon" />My courses</Link></li>
                                                <li><Link style={{ textDecoration: 'none' }} to="/"><img src={require('../Assets/images/arrow.png')} alt="icon" />Contact Us</Link></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div className="container-fluid footer1">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-10">
                                        <p>Terms and Conditions  |   privacy Policy  |  Legal Discloser</p>
                                    </div>
                                    <div className="col-md-2" style={{ textAlign: "right" }}>
                                        <img src={require('../Assets/images/paypal.png')} alt="paypal-logo" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
            
                        <div className="container-fluid footer2">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">
                                        <p>Developed By Ezed Tech</p>
                                    </div>
                                    <div className="col-md-6" style={{ textAlign: "right" }}>
                                        <p>© 2019 All rights reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </Fragment>
                )
            } else return null
        }}

    </StoreConsumer>

    )
}

export default Footer
