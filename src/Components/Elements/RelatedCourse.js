import React, {Fragment } from 'react'
import {Link} from 'react-router-dom'
import { StoreConsumer } from '../../Store/Store'
import SingleCourse from './course/SingleCourse'

const ProductsRender = (props) => {
    const { products } = props
    if(products.length > 0) {
        if(products.length > 4) {
            return(
                <Fragment>
                    <SingleCourse data={products[0]} />
                    <SingleCourse data={products[1]} />
                    <SingleCourse data={products[2]} />
                    <SingleCourse data={products[3]} />
                </Fragment>
            )
        } else {
            return (
                products.map((item, index)=> {
                    return (
                        <SingleCourse key={index} data={item} />
                    )
                })
            )
            
        }
    } else {
        return (
            <Fragment></Fragment>
        )
    }

   
    
}

function RelatedCourse() {
    return (
        <StoreConsumer>
            {values => {
              
              const { category_list, temp_product } = values.store
              var category_id = temp_product.category_id
              var related_items = category_list.find(item => item.category_id === category_id)
              var products_array = []
              if(related_items.category_id !== undefined) {
                  const { courses } = related_items
                  courses.map(item2 => {
                      return (
                          products_array.push(item2)
                      )
                  })
              } else {
                  category_list.map(item => {
                      const { courses } = item
                      return (
                          courses.map(item2 => {
                              return (
                                  products_array.push(item2)
                              )
                          })
                      ) 
                  })
              }
                
                return (
                    <Fragment>
                        <div className="container-fluid boxes">
                            <div className="container">
                                <div className="row">
                                    <div className="spc50"></div>
                                    <div className="col-md-12">
                                        <div className="m-hd">
                                            <h2>Related Courses</h2>
                                            <span></span>
                                            {/* <h1>Choose from 100+ courses</h1> */}
                                        </div>
                                    </div>

                                    <ProductsRender products={products_array} />
                                    
                                    <div className="col-md-12">
                                        <div className="read-more">
                                            <Link style={{ textDecoration: 'none' }} to="/courses" >Get more Courses</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Fragment>
                )
            }}
        </StoreConsumer>
       
    )
}

export default RelatedCourse
