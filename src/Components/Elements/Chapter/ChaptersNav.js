import React from 'react'
import Icofont from 'react-icofont';
import swal from 'sweetalert';
import {Link} from 'react-router-dom'
import { StoreConsumer } from '../../../Store/Store'
import {JumpPageUp} from './SubchapterDetailed'

function ChaptersNav() {

    return (
        <StoreConsumer>
            {values => {
               
                const { chapter } = values.store.chapter
                const { set_current_chapter_id, set_current_subchapter_id, set_current_chapter_index, set_temp_chapter, set_temp_subchapter } = values
                const { current_chapter_id, current_subchapter_id, current_chapter_index } = values.store


                const SubChapters = (props) => {

                    const { is_subchapters, subchapters } = props.data
                    const { index } = props
                    if( is_subchapters && current_chapter_index === index) {

                        return (
                            <div className="px-2 pb-2">
                                <div className="subchapter-nav p-3">
            
                                    {
                                        subchapters.map((item) => {
                                            const { sub_chapter_id, sub_chapter_title, status } = item

                                            var bgStyle = { borderRadius: 5, }
                                            if(sub_chapter_id === current_subchapter_id) {
                                                bgStyle.background = '#dbd8d0'
                                            }
                                            var color = ''
                                            var icon = ''
                                            if(status) {
                                                color='green'
                                                icon='check-circled'
                
                                            } else {
                                                color='red'
                                                icon='close-circled'
                                            }

                                            const subChapterClick = () => {
                                                var tempSub_index = subchapters.indexOf(item)
                                                if(tempSub_index > 0) {
                                                    var subStatus = subchapters[tempSub_index - 1].status
                                                    if(subStatus) {
                                                        set_current_subchapter_id(sub_chapter_id)
                                                        set_temp_subchapter(item)
                                                        JumpPageUp()
                                                    } else {
                                                        swal("Pending", 'Previous chapter is pending.', "error");
                                                    }
                                                } else {
                                                    set_current_subchapter_id(sub_chapter_id)
                                                    set_temp_subchapter(item)
                                                    JumpPageUp()
                                                }
                                                
                                            }

                                            return (
                                                <div style={bgStyle} key={sub_chapter_id} onClick={() => subChapterClick()} className='my-2 pr-1 pt-2 subchapter-div'>
                                                    <Link style={{ textDecoration: 'none', color: '#000'}} to='/subchapter-detailed'>
                                                        <div className='row d-flex justify-content-center'>
                                                            <div className='col-md-2'>
                                                                <Icofont style={{ color: color, fontSize: 15, paddingLeft: 3 }} icon={icon}/>
                                                            </div>
                                                            <div className='col-md-10'>
                                                                <p className="subchapter-nav-txt">{sub_chapter_title}</p>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                </div>
                                                
                                            )
                                        })
                                    }
            
                                   
                                </div>
                            </div>
                        )
                    } else return null
                    
                }


                if(chapter) {
                    return (
                        chapter.map((item,index) => {
                            const { chapter_id, chapter_no, chapter_title, status } = item
                            var color = ''
                            var icon = ''
                            var bgStyle = { }
                            var divClass = ''

                            if(current_chapter_id === chapter_id) {
                                bgStyle.border = '1px solid grey'
                            } else {
                                if(current_chapter_id === 0 && index === 0) {
                                    bgStyle.border = '1px solid grey'
                                }
                            }

                            if(status) {
                                color='green'
                                icon='check-circled'
                                divClass = 'chapter-nav-box2 mt-1'

                            } else {
                                color='red'
                                icon='close-circled'
                                divClass = 'chapter-nav-box mt-1'
                            }

                            const chapter_titleClick = () => {

                                var temp_index = chapter.indexOf(item)
                                if(temp_index > 0) {
                                    var prevStatus = chapter[temp_index - 1].status
                                    if(prevStatus) {
                                        set_current_chapter_index(index)
                                        set_temp_chapter(item)
                                        set_current_chapter_id(chapter_id)
                                    } else {
                                        swal("Pending", 'Previous chapter is pending.', "error");
                                    }
                                } else {
                                    set_current_chapter_index(index)
                                    set_temp_chapter(item)
                                    set_current_chapter_id(chapter_id)
                                }


                                
                            }
                            
                            return (
                                <div key={index} onClick={() => chapter_titleClick()} style={bgStyle} className={divClass}>

                                    <Link style={{ textDecoration: 'none', color: '#000'}} to="/chapter-detailed">
                                        <p className="p-2  chapter-nav-text"><Icofont style={{ color: color, paddingRight: 10 }} icon={icon}/>{chapter_no}. {chapter_title}</p>
                                    </Link>
                                    <SubChapters index={index} data={item} values={values}/>

                                </div>
                            )
                        })
                    ) 

                }
                       
            }}

        </StoreConsumer>
    )
}

export default ChaptersNav


