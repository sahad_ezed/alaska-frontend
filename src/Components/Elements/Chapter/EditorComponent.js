import React, { useState, Fragment } from 'react'
import styled from 'styled-components'
import {Editor} from 'primereact/editor';
import { StoreConsumer } from '../../../Store/Store';

const ModalContainer = styled.div`
    position: fixed;
    overflow: hidden;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: rgba(0,0,0,0.3);
    display: flex;
    align-items: center;
    justify-content: center;

    #modal {
        background: #fff;
        z-index: 10000000;
        width: 600;
        overflow: hidden;
    }

`;

function EditorComponent() {
    
    return (
        <Fragment>
             
            <StoreConsumer>
                {values => {
                    
                    const { modal_chapter, temp_chapter_content } = values.store
                    const { model_chapter_close, modify_individual_content } = values
                    const { content, index} = temp_chapter_content
                    const EditorComponent = () => {
                        const [state, setstate] = useState(content)

                        const update_button = () => {
                            modify_individual_content(state, index)
                            model_chapter_close()
                        }

                        return (
                            <Fragment>
                                <Editor 
                                    style={{height:'320px'}} 
                                    value={state} 
                                    onTextChange={(e) => setstate(e.htmlValue)} 
                                />
                                <button className="btn btn-warning mt-3" onClick={() => model_chapter_close()}>Close</button>
                                <button className="btn btn-success mt-3 ml-2" onClick={() => update_button()} >Update</button>
                            </Fragment>
                            
                        )
                    }

                    

                    if(!modal_chapter) {
                        return null
                    } else {
                        return (
                            <ModalContainer>
                                <div className="container">
                                    <div className="row">
                                        <div id="modal" className="mx-auto text-center text-capitalize p-5 rounded">
                                            <h5 className="text-muted">Edit content paragraph here.!</h5>

                                            <EditorComponent />

                                            
                                            {/* <Link to="/">
                                                <ButtonContainer onClick={() => closeModal()}>
                                                    Continue Shopping
                                                </ButtonContainer>
                                            </Link>
                                            <Link to="/cart">
                                                <ButtonContainer cart onClick={() => closeModal()}>
                                                    Go to Cart
                                                </ButtonContainer>
                                            </Link> */}
                                        </div>
                                    </div>
                                </div>
                            </ModalContainer>
                        )
                    }
            }}
            
        </StoreConsumer>
        </Fragment>
       
    )
}

export default EditorComponent
