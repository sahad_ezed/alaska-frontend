import React, {Fragment} from 'react'
import { Link, Redirect } from 'react-router-dom'
import Icofont from 'react-icofont';
import { StoreConsumer } from '../../../Store/Store';

function ChapterLists(props) {
    // const { chapter } = props.data

    const { set_temp_chapter, set_current_chapter_id, set_current_chapter_index } = props.values
    const { chapter } = props.values.store.chapter


    if(!chapter) return <Redirect to='/my-course' />

    const Topics = (props) => {
        const {is_subchapters, subchapters} = props
        if(is_subchapters) {
            var count = subchapters.length
            return (
                 <p className="px-5 sub-text">{count} Topics</p>
            )
        } else return null
    }

    return (
        <StoreConsumer>
            {value => {
                // const { set_current_chapter_id, set_current_chapter_index } = value
                const { chapter } = value.store.chapter
                return (
                    <Fragment>
                        {
                            chapter.map(item => {
                                const { chapter_id, chapter_no, chapter_title, is_subchapters, status, subchapters } = item
                                var color = ''
                                var icon = ''
                                var bg_color = ''
                                if(status) {
                                    color='green'
                                    icon='check-circled'
                                    bg_color='#b7edb4'
                                } else {
                                    color='red'
                                    icon='close-circled'
                                    bg_color='#e8e8e8'
                                }


                                const chapterClick = () => {
                                    var Index = chapter.indexOf(item)

                                    set_temp_chapter(item)
                                    set_current_chapter_index(Index)
                                    set_current_chapter_id(chapter_id)
                                }

                                return (
                                    <Link key={chapter_id} style={{ textDecoration: 'none'}} onClick={() => chapterClick()} to="/chapter-detailed">
                                        <div style={{ background: bg_color }} className="chapter-box-content">
                                        <p className="box-text mt-2"><Icofont style={{ color: color, paddingRight: 10 }} icon={icon}/> {chapter_no}. {chapter_title}</p>
                                            <Topics is_subchapters={is_subchapters} subchapters={subchapters}/>
                                        </div>
                                    </Link>
                                )
                            })
                        }
                    </Fragment>
                )
            }}
        </StoreConsumer>
        
    )
}

export default ChapterLists
