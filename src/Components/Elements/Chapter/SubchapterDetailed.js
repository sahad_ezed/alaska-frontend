import React, { Fragment } from 'react'
import Icofont from 'react-icofont';
import { Redirect, Link } from 'react-router-dom'
import { StoreConsumer } from '../../../Store/Store'
import {SUBCHAPTER_IMAGE_URL} from '../../../Store/Config'

const TypeA = (props) => {
    const { content, image } = props.data
    var realImg = SUBCHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            <div className="col-md-3 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>
                <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
        </div>
    )
}

const TypeB = (props) => {
    const { content, image } = props.data
    var realImg = SUBCHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            
            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>

            <div className="col-md-3 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>

        </div>
    )
}

const TypeC = (props) => {
    const { content } = props.data
    return (
        <div className="row py-3">
            
            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-12"></div>

        </div>
    )
}

const TypeD = (props) => {
    const { image } = props.data
    var realImg = SUBCHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            
            <div className="col-md-12 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>
        </div>
    )
}

const Buttons = () => {

        return (
            <StoreConsumer>
                {values => {

            
                   const { set_temp_subchapter, set_current_chapter_id, set_current_subchapter_id,  
                    set_current_chapter_index, set_temp_chapter, set_read_subchapter, set_read_chapter, set_temp_subchapter_2 } = values

                    const { temp_chapter, temp_subchapter, order_list } = values.store

                    var course_id = temp_chapter.course_id
                    var order = order_list.find(item => item.course_id === course_id)
                    var order_id = order.order_id
                    const { sub_chapter_id } = temp_subchapter

                    const NextButton = () => {
                        const { temp_chapter, temp_subchapter} = values.store
                        const { subchapters } = temp_chapter
                        const { chapter } = values.store.chapter
                        if(subchapters) {
                            var Index = subchapters.indexOf(temp_subchapter)

                        function nextClick() {
                            set_temp_subchapter_2(temp_subchapter)
                            if(subchapters.length === Index + 1) {
                                //next chapter function
                            } else {
                                const { status } = temp_subchapter
                                if(!status) {
                                    set_read_subchapter({ order_id, sub_chapter_id})
                                    // check_for_chapter_read()
                                }
                                
                                set_temp_subchapter(subchapters[Index + 1])
                                set_current_chapter_id(subchapters[Index + 1].chapter_id)
                                set_current_subchapter_id(subchapters[Index + 1].sub_chapter_id)
                                JumpPageUp()

                            }
                        }


                        if(subchapters.length === Index + 1) {
                            var ChapterIndex = chapter.indexOf(temp_chapter)
                            if(chapter.length === ChapterIndex + 1) {

                                const ExamPage = () => {
                                    set_temp_subchapter_2(temp_subchapter)
                                    const { status } = temp_subchapter
                                    if(!status) {
                                        set_read_subchapter({ order_id, sub_chapter_id})
                                        var chapter_status = temp_chapter.status
                                        if(!chapter_status) {
                                            set_read_chapter({ order_id, chapter_id: temp_subchapter.chapter_id})
                                        }
                                    }
                                }

                                return (
                                    <button style={{ background: '#fabb64', fontSize: 16, width: 180}} onClick={() => ExamPage()}  className="btn btn-blue mt-4 float-right btn-lg px-4">
                                        <Link style={{ textDecoration: 'none', color: '#000' }} to='/exam'> Exam <Icofont icon="arrow-right"/> </Link>
                                    </button>
                                )
                            } else {
                                const nextChapterClick = () => {
                                    set_temp_subchapter_2(temp_subchapter)
                                    const { status } = temp_subchapter
                                    if(!status) {
                                        set_read_subchapter({ order_id, sub_chapter_id})
                                        var chapter_status = temp_chapter.status
                                        if(!chapter_status) {
                                            set_read_chapter({ order_id, chapter_id: temp_subchapter.chapter_id})
                                        }
                                    }
                                    
                                    set_temp_chapter(chapter[ChapterIndex + 1])
                                    set_current_chapter_index(ChapterIndex + 1)
                                    set_current_chapter_id(chapter[ChapterIndex + 1].chapter_id)
                                    set_current_subchapter_id(0)

                                }
                                return (
                                    <button style={{ background: '#fabb64', fontSize: 16, width: 180}} onClick={() => nextChapterClick()}  className="btn btn-blue mt-4 float-right btn-lg px-4"> 
                                        <Link style={{ textDecoration: 'none', color: '#000' }} to='/chapter-detailed'> Next <Icofont icon="arrow-right"/> </Link>
                                    </button>
                                )
                            }

                        } else {
                            return (
                                <button style={{ background: '#fabb64', fontSize: 16, width: 180}} onClick={() => nextClick()}  className="btn btn-blue mt-4 float-right btn-lg px-4"> Next <Icofont icon="arrow-right"/> </button>
                            )
                        }
                    } else return null
                        
                    }


                    const PreviousButton = () => {
                        const { temp_chapter, temp_subchapter } = values.store
                        const { chapter } = values.store.chapter
                        const { subchapters } = temp_chapter
                        if(subchapters) {
                            
                        var Index = subchapters.indexOf(temp_subchapter)
                        
                        function prevClick() {
                            if(Index > 0) {
                                
                                set_temp_subchapter(subchapters[Index - 1])
                                set_current_chapter_id(subchapters[Index - 1].chapter_id)
                                set_current_subchapter_id(subchapters[Index - 1].sub_chapter_id)
                                JumpPageUp()
                            } 
                        }

                        if(Index > 0) {
                            
                            return (
                                <button style={{ background: '#fabb64', fontSize: 16, width: 180 }} onClick={() => prevClick()} className="btn mt-4 btn-lg px-4"> <Icofont icon="arrow-left"/> Previous</button>
                            )

                        } else {
                            var ChapterIndex = chapter.indexOf(temp_chapter)

                            if(ChapterIndex > 0) {

                                const prevChapter = () => {

                                    set_temp_chapter(chapter[ChapterIndex - 1])
                                    set_current_chapter_index(ChapterIndex -1)
                                    set_current_chapter_id(chapter[ChapterIndex - 1].chapter_id)
                                    set_current_subchapter_id(0)



                                }

                                return (
                                    <button style={{ background: '#fabb64', fontSize: 16, width: 180 }} onClick={() => prevChapter()} className="btn mt-4 btn-lg px-4"> 
                                        <Link style={{ textDecoration: 'none', color: '#000' }} to='/chapter-detailed'><Icofont icon="arrow-left"/> Previous </Link>
                                    </button>
                                )
                            } else return null
                        }

                    } else return null
                        
                    } 

                   



                    return (
                        <Fragment>
                            <PreviousButton />
                            <NextButton />
                        </Fragment>
                    )
                }}
                
            </StoreConsumer>
        )    
            
   
}


const MainDataLayout = ({data}) => {
    const { sub_chapter_contents } = data
    return (
        <Fragment>
        {
            sub_chapter_contents.map((item, index) => {
                const { type} = item
                switch(type) {
                    case 1 : return <TypeA key={index} data={item} />
                    case 2 : return <TypeB key={index} data={item} />
                    case 3 : return <TypeC key={index} data={item} />
                    case 4 : return <TypeD key={index} data={item} />
                    default: return null
                }
            })
        }

        <Buttons />

        </Fragment>
    )
}

function SubchapterDetail() {
    return (
        <StoreConsumer>
        {values => {
            const { sub_chapter_id, sub_chapter_title } = values.store.temp_subchapter
            if(!sub_chapter_id) return <Redirect to='/chapter' />
            
            return (
                <div className="px-3">
                    <h4 className="font-weight-bold text-center py-3" >{sub_chapter_title}</h4>
                    <MainDataLayout data={values.store.temp_subchapter} store={values.store}/>
                    {/* <h3>sadsd</h3> */}
                   
                </div>
            )
        }}
        </StoreConsumer>
    )
}

export default SubchapterDetail


export const JumpPageUp = () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}