import React, { Fragment, useState } from 'react'
import Icofont from 'react-icofont'
import {CHAPTER_IMAGE_URL} from '../../../Store/Config'
import { StoreConsumer } from '../../../Store/Store'
import EditorComponent from './EditorComponent'

function AddChapterContents({live, temp, modify}) {

    const[current, setCurrent] = useState({})

    // useEffect(() => {
    //     modify(live)
    // }, [live])

    
    
    const TypeAA = ({data, index}) => {
        const { content, image } = data
        //check image is local
        var img_check = image.split('blob:')
        var realImg
        if(img_check[1]) {
            realImg = image
        } else {
            realImg = CHAPTER_IMAGE_URL + image
        }

        return (
            <StoreConsumer>
                {values => {
                    const { delete_temp_data, modify_individual_image, set_temp_chapter_content, model_chapter_open } = values
                    function updateImage(fx, img, ext, image_file) {
                        if(img) {
                            if(ext === 'jpeg' || ext === 'jpg' || ext === 'png') {
                                var index = temp.indexOf(current)
                                fx(img, ext, index, image_file)
                            } else {
                                alert('File should be an image (jpeg, jpg, png) ')
                            }
                        } else {
                            alert('Choose an image for content')
                        }
                        
                    }
                    return (
                        <div style={{ borderRadius: 10 }} className="my-4 bg-white py-1 px-3 circle-5">
                            <button className="btn mb-4 btn-sm float-right" onClick={() => delete_temp_data(index)}><Icofont icon='close' /></button>
                            <div className="row py-3">
                                <div className="col-md-3 d-flex justify-content-center align-items-center">
                                    <label htmlFor="file-input" style={{ cursor: 'pointer' }} onClick={() => setCurrent(data)}>
                                        <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                                    </label>
                                        <input id="file-input" style={{ display: 'none' }} type="file"
                                            onChange={(image) => {
                                                var img_name = image.target.files[0].name
                                                var image_file = image.target.files[0]
                                                var extension_data = img_name.split('.')
                                                var img_extension = extension_data[extension_data.length - 1]
                                                var image_blob = URL.createObjectURL(image.target.files[0])
                                                updateImage(modify_individual_image, image_blob, img_extension, image_file)
                                            }}
                                        />
                                </div>
                                    <div dangerouslySetInnerHTML={{ __html: content }} onClick={() => content_updation(content, index, set_temp_chapter_content, model_chapter_open)} className="col-md-9"></div>
                            </div>
                        </div>
                    )
                }}
            </StoreConsumer>
            
        )
    }

    
    const TypeBB = ({data, index}) => {
        const { content, image } = data
        //check image is local
        var img_check = image.split('blob:')
        var realImg
        if(img_check[1]) {
            realImg = image
        } else {
            realImg = CHAPTER_IMAGE_URL + image
        }
        return (
            <StoreConsumer>
                {values => {
                    const { delete_temp_data, modify_individual_image, set_temp_chapter_content, model_chapter_open } = values
                    function updateImage(fx, img, ext, image_file) {
                        if(img) {
                            if(ext === 'jpeg' || ext === 'jpg' || ext === 'png') {
                                var index = temp.indexOf(current)
                                fx(img, ext, index, image_file)
                            } else {
                                alert('File should be an image (jpeg, jpg, png) ')
                            }
                        } else {
                            alert('Choose an image for content')
                        }
                    }
                    return (
                        <div style={{ borderRadius: 10 }} className="my-4 bg-white py-1 px-3 circle-5">
                            <button className="btn mb-4 btn-sm float-right" onClick={() => delete_temp_data(index)}><Icofont icon='close' /></button>
                            <div className="row py-3">
                                <div dangerouslySetInnerHTML={{ __html: content }} onClick={() => content_updation(content, index, set_temp_chapter_content, model_chapter_open)} className="col-md-9"></div>
                                <div className="col-md-3 d-flex justify-content-center align-items-center">
                                    <label htmlFor="file-input" style={{ cursor: 'pointer' }} onClick={() => setCurrent(data)}>
                                        <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                                    </label>
                                    <input id="file-input" style={{ display: 'none' }} type="file" multiple={false}
                                        onChange={(image) => {
                                            var img_name = image.target.files[0].name
                                            var image_file = image.target.files[0]
                                            var extension_data = img_name.split('.')
                                            var img_extension = extension_data[extension_data.length - 1]
                                            var image_blob = URL.createObjectURL(image.target.files[0])
                                            updateImage(modify_individual_image, image_blob, img_extension, image_file)
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    )
                }}
            </StoreConsumer>
            
        )
    }

    const content_updation = (content, index, set_temp_chapter_content, model_chapter_open) => {
        var data = {
            content,
            index
        }
        set_temp_chapter_content(data)
        model_chapter_open()
    }
    
    const TypeCC = ({data, index}) => {
        const { content } = data
        return (
            <StoreConsumer>
                {values => {
                const { delete_temp_data, set_temp_chapter_content, model_chapter_open } = values
                    return (
                        <div style={{ borderRadius: 10 }} className="my-4 bg-white py-1 px-3 circle-5">
                            <button className="btn mb-4 btn-sm float-right" onClick={() => delete_temp_data(index)}><Icofont icon='close' /></button>
                            <div className="row py-3" onClick={() => content_updation(content, index, set_temp_chapter_content, model_chapter_open)}>
                                <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-12"></div>
                            </div>
                        </div>
                    )
                }}
            </StoreConsumer>
            
        )
    }
    
    const TypeDD = ({data, index}) => {
        const { image } = data
        //check image is local
        var img_check = image.split('blob:')
        var realImg
        if(img_check[1]) {
            realImg = image
        } else {
            realImg = CHAPTER_IMAGE_URL + image
        }
        
        return (
            <StoreConsumer>
                {values => {
                    const { delete_temp_data, modify_individual_image } = values
                    function updateImage(fx, img, ext, image_file) {
                        if(img) {
                            if(ext === 'jpeg' || ext === 'jpg' || ext === 'png') {
                                var index = temp.indexOf(current)
                                fx(img, ext, index, image_file)
                            } else {
                                alert('File should be an image (jpeg, jpg, png) ')
                            }
                        } else {
                            alert('Choose an image for content')
                        }
                        
                    }
                    return (
                        <div style={{ borderRadius: 10 }} className="my-4 bg-white py-1 px-3 circle-5">
                            <button className="btn mb-4 btn-sm float-right" onClick={() => delete_temp_data(index)}><Icofont icon='close' /></button>
                            <div className="row py-3">
                                <div className="col-md-12 d-flex justify-content-center align-items-center">
                                    <label htmlFor="file-input" style={{ cursor: 'pointer' }} onClick={() => setCurrent(data)}>
                                        <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
                                    </label>
                                    <input id="file-input" style={{ display: 'none' }} type="file"
                                        onChange={(image) => {
                                            var img_name = image.target.files[0].name
                                            var image_file = image.target.files[0]
                                            var extension_data = img_name.split('.')
                                            var img_extension = extension_data[extension_data.length - 1]
                                            var image_blob = URL.createObjectURL(image.target.files[0])
                                            updateImage(modify_individual_image, image_blob, img_extension, image_file)
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    )
                }}
            </StoreConsumer>
            
        )
    }

   
    return (
        <Fragment>

            
            {
                //Temporary contents
                temp.map((item, index) => {
                    const { type } = item
                    switch(type) {
                        case 1: return <TypeAA key={index} data={item} index={index}/>
                        case 2: return <TypeBB key={index} data={item} index={index} />
                        case 3: return <TypeCC key={index} data={item} index={index} />
                        case 4: return <TypeDD key={index} data={item} index={index} />
                        default: return null
                    }
                })
            }



            <EditorComponent />

        </Fragment>
    )
    
    
}

export default AddChapterContents
