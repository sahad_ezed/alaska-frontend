import React, { Fragment, useEffect } from 'react'
import Icofont from 'react-icofont';
import { Link, Redirect } from 'react-router-dom'
import { StoreConsumer } from '../../../Store/Store'
import { JumpPageUp } from './SubchapterDetailed'
import {CHAPTER_IMAGE_URL} from '../../../Store/Config'

const TypeA = (props) => {
    const { content, image } = props.data
    var realImg = CHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            <div className="col-md-3 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>
                <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>
        </div>
    )
}

const TypeB = (props) => {
    const { content, image } = props.data
    var realImg = CHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            
            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-9"></div>

            <div className="col-md-3 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>

        </div>
    )
}

const TypeC = (props) => {
    const { content } = props.data
    return (
        <div className="row py-3">
            
            <div dangerouslySetInnerHTML={{ __html: content }} className="col-md-12"></div>

        </div>
    )
}

const TypeD = (props) => {
    const { image } = props.data
    var realImg = CHAPTER_IMAGE_URL + image
    return (
        <div className="row py-3">
            
            <div className="col-md-12 d-flex justify-content-center align-items-center">
                <img style={{ width: 170, borderRadius: 5}} src={realImg} alt="product"/>
            </div>
        </div>
    )
}

const LessonContent = (props) => {

    const { is_subchapters, subchapters } = props.data

    if(is_subchapters) {
        return (
            <div className="card my-4">
                <div className="card-header">
                    <h5>Lesson Content</h5>
                </div>
    
                <div className="card-body">

                    {
                        subchapters.map(item => {

                            const { sub_chapter_id, chapter_id, sub_chapter_title, status } = item
                            if(status) {
                                var icon = 'check-circled'
                                var color = 'green'
                            } else {
                                icon = 'close-circled'
                                color = 'red'
                            }

                            return (
                                <Fragment key={sub_chapter_id}>
                                    <StoreConsumer>
                                        {value => {
                                            const { set_temp_subchapter, set_current_subchapter_id, set_current_chapter_id, set_current_chapter_index } = value
                                            const { chapter } = value.store.chapter
                                            const { temp_chapter } = value.store

                                            var Index = chapter.indexOf(temp_chapter)

                                            const clickSubchapter = () => {
                                                set_current_subchapter_id(sub_chapter_id)
                                                set_temp_subchapter(item)
                                                set_current_chapter_id(chapter_id)
                                                set_current_chapter_index(Index)
                                                JumpPageUp()
                                            }
                                            return (
                                                <div  onClick={() => clickSubchapter()}>
                                                    <Link style={{ textDecoration: 'none' }} to='/subchapter-detailed'>
                                                        <p className="px-4 text-orange"><Icofont icon={icon} style={{ color: color, paddingRight: 10 }}/> {sub_chapter_title}</p>
                                                    </Link>
                                                    <hr/>
                                                </div>
                                            )
                                        }}
                                    </StoreConsumer>
                                </Fragment>
                            )
                        })
                    }
    
                </div>
            </div>
        )
    } else return null
    
}

const Buttons = ({values}) => {
    // data={temp_chapter} store={store} readStatus={readStatus}
    const {temp_chapter, order_list} = values.store
    const { is_subchapters } = temp_chapter
    const { chapter } = values.store.chapter
    const { set_read_chapter, set_current_chapter_index, set_current_chapter_id } = values

    if(!is_subchapters) {
       
        const index = chapter.indexOf(temp_chapter)
        var course_id = temp_chapter.course_id
        var order = order_list.find(item => item.course_id === course_id)
        var order_id = order.order_id
        var chapter_id = temp_chapter.chapter_id

        // previous button
        const PrevButton = ({storeFunctions}) => {
            const { set_temp_chapter } = storeFunctions

            function prevClick() {
                var prev_chapter = chapter[index - 1]
                if(prev_chapter) {
                    set_temp_chapter(prev_chapter)
                    window.scrollTo({
                        top: 0,
                        behavior: "smooth"
                    });
                }
            }

            if(index > 0) {
                return (
                    <button style={{ background: '#fabb64', fontSize: 16, width: 180 }} onClick={() => prevClick()}  className="btn mt-4 btn-lg px-4"> <Icofont icon="arrow-left"/> Previous</button>
                )
            } else return null
            
        }

        //next button
        const NextButton = ({storeFunctions}) => {
            const { set_temp_chapter } = storeFunctions

            // set_current_chapter_index, set_current_chapter_id
           
            function nextClick() {
                var next_chapter = chapter[index + 1]
                if(next_chapter) {
                    const { status } = temp_chapter

                    
                    
                    set_temp_chapter(next_chapter)
                    set_current_chapter_index(index + 1)
                    set_current_chapter_id(chapter[index + 1].chapter_id)

                    if( !status ) {
                        set_read_chapter({chapter_id, order_id})
                    }

                    window.scrollTo({
                        top: 0,
                        behavior: "smooth"
                    });
                }
            }

            if(chapter.length === index+1) {

                const ExamPage = () => {
                        var chapter_status = temp_chapter.status
                        if(!chapter_status) {
                            set_read_chapter({ order_id, chapter_id: temp_chapter.chapter_id})
                        }
                }

                return (
                    <button style={{ background: '#fabb64', fontSize: 16, width: 180}} onClick={() => ExamPage()}  className="btn btn-blue mt-4 float-right btn-lg px-4">
                        <Link style={{ textDecoration: 'none', color: '#000' }} to='/exam'> Exam <Icofont icon="arrow-right"/> </Link>
                    </button>
                )
            } else {
                return (
                    <button style={{ background: '#fabb64', fontSize: 16, width: 180}} onClick={() => nextClick()}  className="btn btn-blue mt-4 float-right btn-lg px-4"> Next <Icofont icon="arrow-right"/> </button>
                )
            }
            
        }

        return (
            <StoreConsumer>
                {values => {
                    return (
                        <Fragment>
                            <PrevButton storeFunctions={values}/>
                            <NextButton storeFunctions={values}/>
                        </Fragment>
                    )
                }}
                
            </StoreConsumer>
        )    
            
    } else return null
}





const MainDataLayout = ({data, store, readStatus, values}) => {
    
    const { temp_chapter } = values.store
    const { chapter_content } = temp_chapter
    // data={values.store.temp_chapter} store={values.store} readStatus={set_read_chapter}
    if(chapter_content) {
        return (
            <Fragment>
            {
                chapter_content.map((item, index) => {
                    const { type} = item
                    switch(type) {
                        case 1 : return <TypeA key={index} data={item} />
                        case 2 : return <TypeB key={index} data={item} />
                        case 3 : return <TypeC key={index} data={item} />
                        case 4 : return <TypeD key={index} data={item} />
                        default: return null
                    }
                })
            }
    
            <LessonContent data={temp_chapter}/>
    
            <Buttons values={values}/>
    
            </Fragment>
        )
    } else return null
}

function ChapterDetailed() {

    useEffect(() => {
        // window.location.reload();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
    })

    return (
        <StoreConsumer>
            
        {values => {
            
            if(!values.store.temp_chapter.chapter_id) return <Redirect to='/chapter' />
            const { chapter_title } = values.store.temp_chapter
            const { set_read_chapter } = values
            return (
                <div className="px-3">
                    <h4 className="font-weight-bold text-center" >{chapter_title}</h4>
                    <MainDataLayout data={values.store.temp_chapter} store={values.store} readStatus={set_read_chapter} values={values}/>
                </div>
            )
        }}
        </StoreConsumer>
    )
}

export default ChapterDetailed

