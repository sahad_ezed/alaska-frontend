import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import { StoreConsumer } from '../../../Store/Store';
import Icofont from 'react-icofont';

const ModalContainer = styled.div`
    position: fixed;
    overflow: hidden;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: rgba(0,0,0,0.3);
    display: flex;
    align-items: center;
    justify-content: center;

    #modal {
        background: #fff;
        z-index: 10000000;
        width: 600;
        overflow: hidden;
    }

`;

function QuestionModal() {
    
    return (
        <Fragment>
             
            <StoreConsumer>
                {values => {
                    
                    const { modal_chapter, temp_exam } = values.store
                    const { model_chapter_close, add_exam_question, edit_exam_question } = values
                    const { question_title, options, question_id } = temp_exam
                    var Initial_title = ''
                    var InitialOptons = ['']
                    if(question_title) {
                        Initial_title = question_title
                    }
                    if(options){
                        InitialOptons = options
                    }
            
                    if(!modal_chapter) {
                        return null
                    } else {

                        function RenderOptions() {
                            
                            const [options, setOption] = useState(InitialOptons)
                            const [title, setTitle] = useState(Initial_title)

                            function onTrue(index) {
                                var new_arr = []
                                options.forEach(element => {
                                    var item = element
                                    item.is_true = 0
                                    new_arr.push(item)
                                });

                                if(new_arr.length === options.length) {
                                    new_arr[index].is_true = 1
                                    setOption(new_arr)
                                }
                            }

                            function add_question_click () {

                                var data_ckeck = options.find(item => item.is_true === 1)

                                if(title) {
                                    if(options.length >= 2) {
                                        if(data_ckeck) {
                                            if(temp_exam.question_id) {
                                                var question = {
                                                    question_title: title,
                                                    question_id: question_id
                                                }
                                                edit_exam_question(question, options)
                                                model_chapter_close()
                                            } else {
                                                add_exam_question(title, options)
                                            }

                                            
                                        } else {
                                            alert('Select correct answer')
                                        }
                                    } else {
                                        alert('Minimum 2 options are mandatory')
                                    }
                                } else {
                                    alert('question title is mandatory')
                                }

                                
                            }
                          
                            const RenderAddButton = () => {
                                if(temp_exam.length >= 2) {
                                    return null
                                } else {
                                    return (
                                        <button className="btn btn-info btn-sm mt-2" onClick={() => setOption(dataa => [...dataa, {option: '', status: false}])}>
                                            <Icofont icon="plus-circle" /> add
                                        </button>
                                    )
                                }
                                
                            }

                            return (
                                <Fragment>

                                            <textarea className="form-control" type="text" placeholder="Question" value={title}
                                                onChange={(text) => setTitle(text.target.value)}
                                            />
                                            <p className="mt-2">Options</p>

                                            <div className="my-2"> 

                                            {
                                                options.map((item, index) => {
                                                    const { option_title, is_true, option_id} = item
                                                    // const { optionz, status } = item
                                                    console.log(item)
                                                    var color
                                                    if(is_true) {
                                                        color = 'btn btn-sm btn-success'
                                                    } else {
                                                        color = 'btn btn-sm btn-warning'
                                                    }
                                                    return (
                                                        <Fragment key={index}>
                                                            <div className="row">
                                                                <div className="col-md-10">
                                                                    <input className="form-control" type="text" placeholder="Question" 
                                                                        defaultValue={option_title}
                                                                        onChange={(text) => {
                                                                            var obj
                                                                            var temp
                                                                            if(option_id) {
                                                                                obj = item
                                                                                obj.option_title = text.target.value
                                                                                temp = options
                                                                                temp[index]= obj
                                                                                setOption(temp)
                                                                            } else {
                                                                                obj = { option_title: text.target.value }
                                                                                temp = options
                                                                                temp[index]= obj
                                                                                setOption(temp)
                                                                            }
                                                                           
                                                                        }}
                                                                    />
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <button onClick={() => onTrue(index)} className={color}>True</button>
                                                                </div>
                                                            </div>
                                                        </Fragment>
                                                    )
                                                })
                                            }

                                        </div>

                                    <RenderAddButton />

                                    <div>
                                        <button className="btn btn-warning mt-5" onClick={() => model_chapter_close()}>Close</button>
                                        <button className="btn btn-success mt-5 ml-2" onClick={() => {add_question_click()}} >Update</button>
                                    </div>
                                    
                                    
                                </Fragment>
                            )
                        }

                        return (
                            <ModalContainer>
                                <div className="container">
                                    <div className="row">
                                        <div id="modal" className="mx-auto text-center text-capitalize p-5 rounded">
                                            <h5 className="text-muted">Edit question</h5>
                                            
                                          
                                                <RenderOptions />
                                            

                                        </div>
                                    </div>
                                </div>
                            </ModalContainer>
                        )
                    }
            }}
            
        </StoreConsumer>
        </Fragment>
       
    )
}

export default QuestionModal
